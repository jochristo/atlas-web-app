package org.atlas.core.helpers;

import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;

//Hibernate Naming Convension Unproxifier.
public class Unproxifier {

	@SuppressWarnings("unchecked")
	public static <T> T initializeAndUnproxy(T entity) {
	    if (entity == null) {
	        throw new  NullPointerException("Entity passed for initialization is null");//TODO
	    }

	    Hibernate.initialize(entity);
	    if (entity instanceof HibernateProxy) {
	        entity = (T) ((HibernateProxy) entity).getHibernateLazyInitializer().getImplementation();
	    }
	    return entity;
	}
	
}
