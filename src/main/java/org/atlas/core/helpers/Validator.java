package org.atlas.core.helpers;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Core Services ( Helpers )
 * @since 2016-07-22
 **/
public class Validator {	
	
	/**
	 * Checking if a String is integer.
	 * @param str	The string to check.
	 * @return	true/false
	 */
	public static boolean isInteger(String str) {
	    try {
	        Integer.parseInt(str);
	        return true;
	    } catch (NumberFormatException nfe) { return false;}
	}	
	
}
