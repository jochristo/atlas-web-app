package org.atlas.core.helpers;

import java.util.Random;
import java.util.UUID;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Core ( Generators )
 * @since 2016-07-27
 **/

public class Generator {
	
	/**
	 * Generate a random 6 digits verification code.
	 * @return	String
	 */
	public static String generateVerificationCode(){
		Random rand = new Random();
		int randomInt = rand.nextInt(999999) + 100000;
		return String.valueOf(randomInt);
	}
	
	/**
	 * Generate a UUID for the Verification
	 * @return	String
	 */
	public static String generateHash(){
		return UUID.randomUUID().toString();
	}
	

}
