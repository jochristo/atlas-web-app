package org.atlas.core.jsonapi;

import java.util.ArrayList;
import java.util.Set;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class JSONApiDocument {
	
	private Object data;
	private ArrayList<JSONApiErrors> errors;
	private Object meta;
	private JSONApiLink links;
	private Object included;
	private JSONApi jsonapi = new JSONApi();
        
        private Object relationships;
	
	public JSONApiDocument(){}
	
	public JSONApiDocument(Object obj){
		
		if( obj == null ) {
			this.data = new JSONApiData();
			return;
		}
		
		if( (obj instanceof ArrayList) ){
			ArrayList<JSONApiData> list = new ArrayList<JSONApiData>();
			int arraySize = ((ArrayList) obj).size();			
			for( int i=0; i<arraySize; i++ ){
				Object curObj = ((ArrayList) obj).get(i);                                
				list.add(new JSONApiData().convertToJSONApiData(curObj));
			}			
			this.data = list;
		}else{			
			this.data = new JSONApiData();
			((JSONApiData) this.data).convertToJSONApiData(obj);
		}
	}
	
	//This will be used only for override single object type
	public JSONApiDocument(Object obj, String type){
		if( obj == null ) {
			this.data = new JSONApiData(type);
			return;
		}
		
		if( (obj instanceof ArrayList) ){
			ArrayList<JSONApiData> list = new ArrayList<JSONApiData>();
			int arraySize = ((ArrayList) obj).size();			
			for( int i=0; i<arraySize; i++ ){
				Object curObj = ((ArrayList) obj).get(i);
				list.add(new JSONApiData(type).convertToJSONApiData(curObj));
			}			
			this.data = list;
		}else{			
			this.data = new JSONApiData(type);
			((JSONApiData) this.data).convertToJSONApiData(obj);
		}
	}

	public Object getData() {
		
		if( (this.data instanceof java.util.LinkedHashMap) ){
			ObjectMapper mapper = new ObjectMapper();
			this.data = mapper.convertValue(this.data, JSONApiData.class);
		}
		
		return this.data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	
	public ArrayList<JSONApiErrors> getErrors() {
		return errors;
	}

	public void setErrors(ArrayList<JSONApiErrors> errors) {
		this.errors = errors;
	}
	
	public Object getMeta() {
		return meta;
	}
	public void setMeta(Object meta) {
		this.meta = meta;
	}
	public JSONApi getJsonapi() {
		return jsonapi;
	}
	public void setJsonapi(JSONApi jsonapi) {
		this.jsonapi = jsonapi;
	}
	public JSONApiLink getLinks() {
		return links;
	}
	public void setLinks(JSONApiLink links) {
		this.links = links;
	}
	
	public Object getIncluded() {
		ObjectMapper mapper = new ObjectMapper();
		if( (this.included instanceof java.util.LinkedHashMap) ){
			this.included = mapper.convertValue(this.included, JSONApiData.class);
		}
		
		if( (this.included instanceof java.util.ArrayList) ){
			ArrayList<JSONApiData> data = new ArrayList<JSONApiData>();
			for( Object obj: (ArrayList<Object>) this.included ){
				data.add(mapper.convertValue(obj, JSONApiData.class));
			}
			this.included = data;
		}
		
		return this.included;
	}

	public void setIncluded(Object included) {
		this.included = included;
	}
	
	public void includeData(Object... included){
		ArrayList<JSONApiData> list = new ArrayList<JSONApiData>();
		for(Object include : included){
			list.add(new JSONApiData().convertToJSONApiData(include));
	    }
		this.included = list;
	}
	
	public void includeDataAsArrayList(ArrayList<?> included){
		ArrayList<JSONApiData> list = new ArrayList<JSONApiData>();
		for(Object include : included){
			list.add(new JSONApiData().convertToJSONApiData(include));
	    }
		this.included = list;
	}
	
	public void includeDataAsSet(Set<?> included){
		ArrayList<JSONApiData> list = new ArrayList<JSONApiData>();
		for(Object include : included){
			list.add(new JSONApiData().convertToJSONApiData(include));
	    }
		this.included = list;
	}

        
        /**
         * Appends items in the given data collection to this instance included property.
         * @param included - The collection to include in the included property
         */
	public void appendIncludedDataCollection(Collection<?> included)
        {		
            Object object = this.getIncluded();
            ArrayList<JSONApiData> list = new ArrayList<>();
            if ((null != object && object instanceof java.util.LinkedHashMap)) {
                Set set = (HashSet<?>) object;                
                set.forEach((include) -> {
                    list.add(new JSONApiData().convertToJSONApiData(include));
                });
                included.forEach((include) -> {
                    set.add(new JSONApiData().convertToJSONApiData(include));
                });                
                this.included = list;
            }
            else if(null != object && object instanceof java.util.ArrayList){
                List tempList = (List<?>) object;                
                tempList.forEach((include) -> {
                    list.add(new JSONApiData().convertToJSONApiData(include));
                });
                included.forEach((include) -> {
                    tempList.add(new JSONApiData().convertToJSONApiData(include));
                });                
                this.included = tempList;                
            }
            else if(object  == null){
                included.forEach((include) -> {
                    list.add(new JSONApiData().convertToJSONApiData(include));
                });                
                this.included = list;
            }
	}         
        
	public void appendIncludedData(Object... included)
        {		
            Object object = this.getIncluded();
            ArrayList<JSONApiData> list = new ArrayList<>();
            if ((null != object && object instanceof java.util.LinkedHashMap)) {
                Set set = (HashSet<?>) object;
		for(Object include : included){
                    set.add(new JSONApiData().convertToJSONApiData(include));
                } 
                this.included = set;
            } else if (null != object && object instanceof java.util.ArrayList) {
                List tempList = (List<?>) object;                
		for(Object include : included){
                    tempList.add(new JSONApiData().convertToJSONApiData(include));
                }                
                this.included = tempList;
            } else if (object == null) {
                for (Object include : included) {
                    list.add(new JSONApiData().convertToJSONApiData(include));
                    this.included = list;
                }
            }            
	}          
        
        // relationships added
        
        public Object getRelationships() {
		ObjectMapper mapper = new ObjectMapper();
		if( (this.relationships instanceof java.util.LinkedHashMap) ){
			this.relationships = mapper.convertValue(this.relationships, JSONApiData.class);
		}
		
		if( (this.relationships instanceof java.util.ArrayList) ){
			ArrayList<JSONApiData> data = new ArrayList<JSONApiData>();
			for( Object obj: (ArrayList<Object>) this.relationships ){
				data.add(mapper.convertValue(obj, JSONApiData.class));
			}
			this.relationships = data;
		}
		
		return this.relationships;
        }

        public void setRelationships(Object relationships) {
            this.relationships = relationships;
        }
        
	public void attachRelationshipsData(Object... included){
		ArrayList<JSONApiData> list = new ArrayList<JSONApiData>();
		for(Object include : included){
			list.add(new JSONApiData().convertToJSONApiData(include));
	    }
		this.relationships = list;
	}
	
	public void attachRelationshipsDataAsArrayList(ArrayList<?> included){
		ArrayList<JSONApiData> list = new ArrayList<JSONApiData>();
		for(Object include : included){
			list.add(new JSONApiData().convertToJSONApiData(include));
	    }
		this.relationships = list;
	}
	
	public void attachRelationshipsDataAsSet(Set<?> included){
		ArrayList<JSONApiData> list = new ArrayList<JSONApiData>();
		for(Object include : included){
			list.add(new JSONApiData().convertToJSONApiData(include));
	    }
		this.relationships = list;
	}            

}
