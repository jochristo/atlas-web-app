package org.atlas.core.jsonapi;

/**
 * JSON API Implementation - Links
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System core ( JSON API Schema Implementation )
 * @since 2016-08-05
 * @see http://jsonapi.org/format/#document-links
 **/

import org.atlas.core.jsonapi.helpers.RelatedLink;

public class JSONApiLink {
	
	private String self;
	private RelatedLink related;
	
	public String getSelf() {
		return self;
	}
	public void setSelf(String self) {
		this.self = self;
	}
	public RelatedLink getRelated() {
		return related;
	}
	public void setRelated(RelatedLink related) {
		this.related = related;
	}

}
