package org.atlas.core.jsonapi;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.LinkedHashMap;

import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;
import org.atlas.core.jsonapi.annotations.JAPILink;
import org.atlas.core.jsonapi.annotations.JAPIRelationship;
import org.atlas.core.jsonapi.annotations.JAPIType;
import org.atlas.core.jsonapi.helpers.RelatedLink;
import org.atlas.core.jsonapi.serializers.RelationshipSerializer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class JSONApiData{
	
	private final ObjectMapper mapper = new ObjectMapper();
	
	@JsonInclude(Include.ALWAYS)
	private String id = java.util.UUID.randomUUID().toString();
	
	@JsonInclude(Include.ALWAYS)
	private String type;
	
	@JsonInclude(Include.ALWAYS)
	private Object attributes;
	
	@JsonSerialize(using = RelationshipSerializer.class)
	private JSONApiRelationship relationships;
	private JSONApiLink links;
	
	@JsonIgnore
	private boolean isResponse = false;
	
	@JsonIgnore
	private String classpath = "";
	
	@JsonIgnore
	private String overridedDataType;
	
	public JSONApiData(){
		this.overridedDataType = null;
	}
	
	public JSONApiData(String overridedDataType){
		this.overridedDataType = overridedDataType;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Object getAttributes() {
		
		if( this.isResponse ){
			if(this.attributes != null){
				try {
					return this.mapper.convertValue(this.attributes, Class.forName(this.classpath));
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}				
		}		
		return this.attributes;
	}
	public void setAttributes(Object attributes) {
		this.attributes = attributes;
	}
	public JSONApiRelationship getRelationships() {
		return this.relationships;
	}
	public void setRelationships(JSONApiRelationship relationships) {
		this.relationships = relationships;
	}
	
	/**
	 * 
	 * @param a - Is the current Class
	 * @param b - Is the Superclass;
	 * @return
	 */
	private Field[] mergeClassFields(Field[] a, Field[] b){
		
		int aLen = a.length;
		int bLen = b.length;
		
		if( bLen == 0 ) return a;
		
		Field[] c= new Field[aLen+bLen];
		System.arraycopy(a, 0, c, 0, aLen);
		System.arraycopy(b, 0, c, aLen, bLen);
		return c;
		
	}	

	public JSONApiData convertToJSONApiData(Object obj){
		
		LinkedHashMap lhm = new LinkedHashMap();//Attributes
		JSONApiRelationship relationship = null;
		
		getTypeByClass(obj);
		
		Field[] allFields = this.mergeClassFields(obj.getClass().getDeclaredFields(), obj.getClass().getSuperclass().getDeclaredFields()); 
		
		for (Field annotation : allFields) {//obj.getClass().getDeclaredFields()
			
			if(annotation.isAnnotationPresent(JAPIIdentifier.class)){
				annotation.setAccessible(true);
				Object value;
				try {
					value = annotation.get(obj);
					this.id = value.toString();
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(annotation.isAnnotationPresent(JAPIAttribute.class)){
				annotation.setAccessible(true);
				Object value;
				try {
					value = annotation.get(obj);				
					lhm.put(annotation.getName(), value);
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
			if(annotation.isAnnotationPresent(JAPIRelationship.class)){
				
				JAPIRelationship japran = (JAPIRelationship) annotation.getAnnotation(JAPIRelationship.class);
				relationship = new JSONApiRelationship();
				JSONApiLink link = new JSONApiLink();
				annotation.setAccessible(true);
				Object value;				
				try {
					value = annotation.get(obj);
					link.setSelf(japran.selfLink().replaceFirst("\\{.+\\}", String.valueOf(value)));
					relationship.setRelationtype(japran.relationship().getSimpleName());
					relationship.setLinks(link);
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
    	}
		
		this.isResponse = true;		
		this.attributes = lhm;
		
		if( relationship != null ){
			//RelationshipType type = new RelationshipType();
			//type.setRelationship();
			this.relationships = relationship; 
		}
		
		
		configureDocumentLinks(obj);
		
		return this;
	}
	
	private void configureDocumentLinks(Object obj) {
		JSONApiLink links = new JSONApiLink();
		boolean linksAnnotationFund = false;
		for (Annotation annotation : obj.getClass().getDeclaredAnnotations()) {
			if( (annotation instanceof JAPILink) ){
				linksAnnotationFund = true;
				JAPILink japlan = (JAPILink) annotation;
				links.setSelf(japlan.self().replaceFirst("\\{.+\\}", this.id));
				links.setRelated(new RelatedLink(japlan.related().replaceFirst("\\{.+\\}", this.id)));
			}
		}		
		if( linksAnnotationFund ) this.setLinks(links);
	}
	
	private void getTypeByClass(Object object){
		
		boolean typeAnnotationFound = false;
		for (Annotation annotation : object.getClass().getDeclaredAnnotations()) {
			if( (annotation instanceof JAPIType) ){
				typeAnnotationFound = true;
				JAPIType japtype = (JAPIType) annotation;
				this.type = japtype.type();
			}
		}
		
		String classname = object.getClass().toString();
		
		this.classpath = "";
		String[] temp = classname.split(" ");
		temp = temp[temp.length-1].split("\\.");
		
		int tempLength = temp.length;
		for( int i=0; i <tempLength; i++){
			if( i == tempLength-1 ){
				if( !typeAnnotationFound ){
					this.type = temp[i].toLowerCase();
				}
				this.classpath += temp[i].substring(0, 1).toUpperCase() + temp[i].substring(1);
			}else{
				this.classpath += temp[i].toLowerCase()+".";
			}
		}
		
		if( this.overridedDataType != null ){
			this.type = this.overridedDataType;
		}
	}

	public Object convertTo(Class<?> classModel) {
		return this.mapper.convertValue(this.attributes, classModel);
	}

	public JSONApiLink getLinks() {
		return links;
	}

	public void setLinks(JSONApiLink links) {
		this.links = links;
	}

}
