package org.atlas.core.jsonapi;

/**
 * JSON API Implementation - Relationships
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System core ( JSON API Schema Implementation )
 * @since 2016-08-05
 * @see http://jsonapi.org/format/#document-resource-object-relationships
 **/

import com.fasterxml.jackson.annotation.JsonIgnore;

public class JSONApiRelationship {
	
	@JsonIgnore
	private String relationtype;
	
	private JSONApiLink links;
	private Object data;
	private Object meta;
	
	public JSONApiRelationship(){}
	
	public JSONApiLink getLinks() {
		return links;
	}
	public void setLinks(JSONApiLink links) {
		this.links = links;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public Object getMeta() {
		return meta;
	}
	public void setMeta(Object meta) {
		this.meta = meta;
	}

	public String getRelationtype() {
		return relationtype;
	}
	public void setRelationtype(String relationtype) {
		this.relationtype = relationtype;
	}
}
