package org.atlas.core.jsonapi.serializers;

import java.io.IOException;

import org.atlas.core.jsonapi.JSONApiRelationship;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class RelationshipSerializer extends JsonSerializer<Object>{

	@Override
	public void serialize(Object value, JsonGenerator gen, SerializerProvider serializers) throws IOException,JsonProcessingException {
		
		JSONApiRelationship relationship = (JSONApiRelationship) value;
		
		gen.writeStartObject();
		gen.writeObjectField(relationship.getRelationtype().toLowerCase(), value);
		gen.writeEndObject();
		
	}

}
