package org.atlas.core.jsonapi.helpers;

public class ErrorSource {

	private String pointer;
	private String parameter;
	
	public ErrorSource(){};
	
	public ErrorSource(String pointer){
		this.pointer = pointer;
	}
	
	public String getPointer() {
		return "/data/attributes/"+pointer;
	}
	public void setPointer(String pointer) {
		this.pointer = pointer;
	}
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	
	
	
}
