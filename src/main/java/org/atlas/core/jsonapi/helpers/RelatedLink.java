package org.atlas.core.jsonapi.helpers;

public class RelatedLink {
	
	public RelatedLink(){}
	public RelatedLink(String href){
		this.href = href;
	}

	private String href = "";

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}
	
}
