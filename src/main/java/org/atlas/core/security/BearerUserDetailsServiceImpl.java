package org.atlas.core.security;

import org.atlas.modules.users.domain.User;
import org.atlas.modules.users.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class BearerUserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	private UserRepository customerRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		//We do not have username is Customers Table.
		User customer = customerRepository.findByEmail(username);
		
		if (customer == null) {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        } else {
            return null;//BearerCustomerFactory.create(customer);
        }
		
	}

}
