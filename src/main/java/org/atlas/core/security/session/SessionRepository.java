package org.atlas.core.security.session;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SessionRepository  extends JpaRepository<Session, Long> {
	
	/**
	 * Retrieve a session by the given token and status(Usually Opened)
	 * @param token		Token(UUID) to check
	 * @param status	Status to tcheck
	 * @return			Session
	 */
	Session findByTokenAndStatus(String token, SessionStatus status);
	
	/**
	 * Clean up session
     * @param closed status closed
     * @param interval interval
     * @param open status open
     * @param mydate the date
	 */
	@Transactional
	@Modifying
	@Query(value = "UPDATE sessions SET status=:closed, updated_at=:mydate WHERE (updated_at<=(:mydate - INTERVAL :interval MINUTE)) AND status=:open", nativeQuery = true)
	void cleanSessions(@Param("closed") int closed, @Param("interval") int interval, @Param("open") int open, @Param("mydate") java.util.Date mydate);
	
	/**
	 * Retrieve last session of a Customer
	 * @param customer	Customer Id
	 * @return			Session
	 */
	@Query(value = "SELECT * FROM sessions WHERE customer_id=:customer ORDER BY id DESC LIMIT 1", nativeQuery = true)
	Session getCustomerLastSession(@Param("customer") long customer);
	
	
}
