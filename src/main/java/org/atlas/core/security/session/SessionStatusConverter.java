package org.atlas.core.security.session;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class SessionStatusConverter implements AttributeConverter<SessionStatus, Integer>{

	@Override
	public Integer convertToDatabaseColumn(SessionStatus attribute) {
		switch (attribute) {
	        case OPEN:
	            return 1;
	        case CLOSED:
	            return 2;
	        default:
	            throw new IllegalArgumentException("Unknown" + attribute);
	    }
	}

	@Override
	public SessionStatus convertToEntityAttribute(Integer dbData) {
		switch (dbData) {
	        case 1:
	            return SessionStatus.OPEN;
	        case 2:
	            return SessionStatus.CLOSED;
	        default:
	            throw new IllegalArgumentException("Unknown" + dbData);
	    }
	}

}
