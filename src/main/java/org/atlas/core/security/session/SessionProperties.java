package org.atlas.core.security.session;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Core Services ( Session )
 * @since 2016-07-27
 **/

@Component
@ConfigurationProperties("atlas.session")
public class SessionProperties {
	
	/**
	 * Session Clean up check every. In milliseconds
	 */
	@Value("${atlas.session.clean-up}")
    private int cleanUpInterval;
	
	/**
	 * Session Check up inteval. In minutes, used in MySQL Query
	 */
	@Value("${atlas.session.check-up-interval}")
    private int checkUpInterval;
	
	/**
	 * Token Expire after (In minutes)
	 */
	@Value("${atlas.session.expire-after:30}")
	private int expireAfter;
	

	public int getCleanUpInterval() {
		return cleanUpInterval;
	}

	public void setCleanUpInterval(int cleanUpInterval) {
		this.cleanUpInterval = cleanUpInterval;
	}

	public int getCheckUpInterval() {
		return checkUpInterval;
	}

	public void setCheckUpInterval(int checkUpInterval) {
		this.checkUpInterval = checkUpInterval;
	}
	
	public int getExpireAfter() {
		return expireAfter;
	}

	public void setExpireAfter(int expireAfter) {
		this.expireAfter = expireAfter;
	}
	
}
