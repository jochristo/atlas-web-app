package org.atlas.core.security.session;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System modules ( System Sessions )
 **/
public enum SessionStatus{
	
	/**
	 * Session Started
	 */
	OPEN,
	
	/**
	 * Session Closed
	 */
	CLOSED;

}
