package org.atlas.core.security.session;


/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Core Services ( Session Service Implementation )
 * @since 2016-07-27
 * @see com.sol4mob.api.services.SessionService
 **/

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.atlas.api.services.SessionService;
import org.atlas.modules.users.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

@Service
public class SessionServiceImpl implements SessionService {
	
	@Resource
	private SessionRepository sessionRepository;
	
	@Resource
	private SessionProperties sessionProperties;
	
	@Autowired
	private StringRedisTemplate redisInst;

	@Override
	public Session startSession(User customer) {
		Session session = new Session();
		session.setCustomer((int)customer.getId());
		session.setStatus(SessionStatus.OPEN);
		session.setToken(UUID.randomUUID().toString());
		session.setCreatedAt(new java.util.Date());
		sessionRepository.save(session);
		
		//Setting the Session ID to Redis
		ValueOperations<String, String> ops = this.redisInst.opsForValue();
		String key = session.getToken();
		if (this.redisInst.hasKey(key)) return null; //Session already exists in Redis		
		ops.set(key, String.valueOf(session.getCustomer()), sessionProperties.getExpireAfter(),TimeUnit.MINUTES);
		return session;		
	}

	@Override
	public Session closeSession(String token) {
		Session session = sessionRepository.findByTokenAndStatus(token, SessionStatus.OPEN);
		if( session == null ) return null;
		session.setStatus(SessionStatus.CLOSED);
		session.setUpdatedAt(new java.util.Date());
		session = sessionRepository.save(session);
		this.redisInst.delete(session.getToken());		
		return session;
	}

	@Override
	public void cleanSessions() {
		SessionStatusConverter converter = new SessionStatusConverter();
		sessionRepository.cleanSessions(converter.convertToDatabaseColumn(SessionStatus.CLOSED), sessionProperties.getCheckUpInterval(), converter.convertToDatabaseColumn(SessionStatus.OPEN), new java.util.Date());
	}

	@Override
	public Session updateSession(Session session) {
		session.setUpdatedAt(new java.util.Date());
		return sessionRepository.save(session);
	}

}
