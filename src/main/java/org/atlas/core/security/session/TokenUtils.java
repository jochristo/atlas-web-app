package org.atlas.core.security.session;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System core ( Security Token Utils )
 * @since 2016-07-25
 **/

@Service
public class TokenUtils {
	
	//@Resource
	//private SessionRepository sessionRepository;
	
	@Resource
	private SessionProperties sessionProperties;
	
	@Autowired
	private StringRedisTemplate redisInst;
	
	private static final Logger logger = LoggerFactory.getLogger(TokenUtils.class);
	
	public static UUID generateUUID(){
		return UUID.randomUUID();
	}
	
	public int getCustomerIdFromToken(String token){
		if( !this.isTokenValid(token) ) return 0;
		
		String value = this.redisInst.opsForValue().get(token);
		if( value != null ){
			this.redisInst.expire(token, sessionProperties.getExpireAfter(), TimeUnit.MINUTES);
			return Integer.parseInt(value);
		}
		
		//Session session = sessionRepository.findByTokenAndStatus(token, SessionStatus.OPEN);
		//if( session != null ){
		//	session.setUpdatedAt(new java.util.Date());
		//	sessionRepository.save(session);
		//	return session.getCustomer();
		//}
		
		return 0;
	}
	
	private boolean isTokenValid(String token){
		try{
		    UUID.fromString(token);
		    return true;
		} catch (IllegalArgumentException exception){
			logger.error("UUID("+token+") checked is invalid");
			return false;
		}
	}

}
