package org.atlas.core.security;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.atlas.core.security.session.TokenUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System core ( Security Authentication Filter )
 * @since 2016-07-25
 **/

public class BearerAuthenticationTokenFilter extends UsernamePasswordAuthenticationFilter{

	private static final Logger logger = LoggerFactory.getLogger(BearerAuthenticationTokenFilter.class);
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private TokenUtils tokenUtils;
	
	@Resource
	private CustomSecurityProperties customSecurityProperties;
	
	/**
	 * Main authentication entry point, Checking the Authorization header.
	 */
	@Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		logger.debug("Processing request received from: " + request.getRemoteAddr());
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		
		logger.debug("Request URI: " + httpRequest.getRequestURI());
		
		String authToken = httpRequest.getHeader(customSecurityProperties.getHeaderName());
		if(authToken != null){
			
			if( authToken.startsWith(customSecurityProperties.getHeaderPrefix()) ){
				authToken = authToken.substring(customSecurityProperties.getHeaderPrefix().length());
				logger.debug("Examining token: "+authToken);
				
				int userId = tokenUtils.getCustomerIdFromToken(authToken);
				logger.debug("Found user with Id: "+userId);
				
				if (userId > 0 && SecurityContextHolder.getContext().getAuthentication() == null) {
					logger.debug("Fetch user details for user with Id: "+userId);
					try{
						UserDetails userDetails = this.userDetailsService.loadUserByUsername(String.valueOf(userId));
						UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, authToken, userDetails.getAuthorities());
						authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
						SecurityContextHolder.getContext().setAuthentication(authentication);
					}catch(UsernameNotFoundException ex){}
				}
			}
		
		}
		
        chain.doFilter(request, response);
    }
	
	
	
}
