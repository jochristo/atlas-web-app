package org.atlas.core.security;

/**
 * Main System Security Module
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Core ( Security Configuration Middleware )
 * @since 2016-07-27
 **/

import org.atlas.api.ApiEndpoints;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	@Override
	@Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
	
	@Bean
	public BearerAuthenticationTokenFilter authenticationTokenFilterBean() throws Exception {
		BearerAuthenticationTokenFilter authenticationTokenFilter = new BearerAuthenticationTokenFilter();
		authenticationTokenFilter.setAuthenticationManager(authenticationManagerBean());
		return authenticationTokenFilter;
	}
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
		
		//Enable CSRF Protection ( Check if needed (TODO) )
		//.csrf().csrfTokenRepository(csrfTokenRepository()).and().addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class)
		
		http
			.csrf().disable()
			.exceptionHandling()
			.authenticationEntryPoint(new RestAuthenticationEntryPoint())
			.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and().addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);
		
		http.authorizeRequests()
			.antMatchers(HttpMethod.GET,"/","/*.html","/favicon.ico","/**/*.html","/**/*.css","/**/*.js","/**/*.eot","/**/*.svg","/**/*.ttf","/**/*.woff","/**/*.woff2","/**/*.png","/**/*.jpg","/**/*.map").permitAll()
			.antMatchers(HttpMethod.GET,"/**/*.mem","/**/*.data","/**/*.datagz","/**/*.jsgz","/**/*.memgz").permitAll()
			.antMatchers(permittedEndpoints()).permitAll()
			.antMatchers(OnlyUserEndpoints()).hasRole("USER")
			.antMatchers(BothAdminAndUserEndpoints()).hasAnyRole("USER","ADMIN")
			.antMatchers(OnlyAdminEndpoints()).hasRole("ADMIN")
			.antMatchers(OnlySuperUserEndpoints()).denyAll()
			.anyRequest().authenticated();
	}	
	
	/**
	 * Return the Rest Endpoints that is accessible without Authentication
	 * @return	List of Endpoints
	 * 
	 */
	private String[] permittedEndpoints(){
		return new String[]{
				ApiEndpoints.AUTHENTICATION_LOGIN,
				ApiEndpoints.AUTHENTICATION_SIGNUP,
				ApiEndpoints.SPRING_BOOT_ERRORS
		};
	}
	
	/**
	 * Return the Rest Endpoints that is accessible with Administrator Privilege
	 * @return	List of Endpoints
	 */
	private String[] OnlyAdminEndpoints(){
		return new String[]{};
	}
	
	/**
	 * Return the Rest Endpoints that is accessible with Logged User Privilege
	 * @return	List of Endpoints
	 */
	private String[] OnlyUserEndpoints(){
		return new String[]{
				ApiEndpoints.AUTHENTICATION_LOGOUT,
				ApiEndpoints.USER_PROFILE
		};
	}
	
	/**
	 * Return the Rest Endpoints that is accessible with Logged User & Administrator Privilege
	 * @return	List of Endpoints
	 */
	private String[] BothAdminAndUserEndpoints(){
		return new String[]{
				ApiEndpoints.COUNTRIES,
				ApiEndpoints.COUNTRY_BYID,
				ApiEndpoints.AALHOUSES,
				ApiEndpoints.AALHOUSES_BYID,
				ApiEndpoints.AALHOUSES_BYID_ENABLE,
				ApiEndpoints.GATEWAYS,
				ApiEndpoints.GATEWAYS_BYID,
				ApiEndpoints.GATEWAYS_BYID_ENABLE,
				ApiEndpoints.GATEWAYS_SENSORS,
				ApiEndpoints.GATEWAYS_SENSORS_BYID,
				ApiEndpoints.SENSORS,
				ApiEndpoints.SENSORS_BYID,
				ApiEndpoints.SENSORS_BYID_ENABLE,
				ApiEndpoints.SENSORS_BYID_DATATYPE,
				ApiEndpoints.DATATYPES,
				ApiEndpoints.MANUFACTURES,
				ApiEndpoints.EMULATION,
				ApiEndpoints.EMULATION_QUEUES
		};
	}
	
	/**
	 * Return Rest Endpoints that is accessible only by Super Users
	 * @return List of Endpoints
	 */
	private String[] OnlySuperUserEndpoints(){
		return new String[]{
				ApiEndpoints.SPRING_BOOT_PROFILE,
				ApiEndpoints.SPRING_BOOT_PROFILE_REPOSITORY
				
		};
	}
	
}
