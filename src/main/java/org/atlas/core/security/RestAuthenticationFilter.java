package org.atlas.core.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

public class RestAuthenticationFilter extends AbstractAuthenticationProcessingFilter{

	
	protected RestAuthenticationFilter( RequestMatcher requiresAuthenticationRequestMatcher) { 
		super(requiresAuthenticationRequestMatcher);
		// TODO Auto-generated constructor stub
	}

	@Override
    protected boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("Executing RestAuthenticationFilter(requiresAuthentication)");
        return true;
    }
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
		System.out.println("Executing RestAuthenticationFilter(attemptAuthentication)");
		String header = request.getHeader("Authorization");
		System.out.println("Get Authorization header with value: " + header);
		return null;
	}

}
