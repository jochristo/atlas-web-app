package org.atlas.core.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

//Handle authentication Error
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint{

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {		
		request.setAttribute("appCode", new String("401"));
		request.setAttribute("details", new String("Invalid access token, please signin..."));
		response.setContentType("application/json");
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "UNAUTHORIZED");		
	}

}
