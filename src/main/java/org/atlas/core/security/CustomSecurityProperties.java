package org.atlas.core.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 
 * Properties Class for Spring application.properties
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System modules ( Security )
 * @since 2016-07-25
 **/

@Component
@ConfigurationProperties("mobiweb.authorization")
public class CustomSecurityProperties {
	
	 /**
	  *	HTTP header where system will check for the Token
	  */
	@Value("${mobiweb.authorization.header:Authorization}")
	private String headerName;
	
	/**
	  * Authorization header prefix
	  */
	@Value("${mobiweb.authorization.header.prefix:Bearer }")
	private String headerPrefix;

	public String getHeaderName() {
		return headerName;
	}

	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}

	public String getHeaderPrefix() {
		return headerPrefix;
	}

	public void setHeaderPrefix(String headerPrefix) {
		this.headerPrefix = headerPrefix;
	}

}
