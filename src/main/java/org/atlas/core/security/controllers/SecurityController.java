package org.atlas.core.security.controllers;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.atlas.api.ApiEndpoints;
import org.atlas.api.errors.ApiError;
import org.atlas.api.exceptions.UserNotFoundException;
import org.atlas.api.exceptions.ForbiddenException;
import org.atlas.api.services.UserService;
import org.atlas.api.services.SessionService;
import org.atlas.core.jsonapi.JSONApiData;
import org.atlas.core.jsonapi.JSONApiDocument;
import org.atlas.core.security.session.Session;
import org.atlas.modules.users.domain.User;
import org.atlas.modules.users.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Core ( Security Controllers )
 * @since 2016-07-27
 * endpoints
 * 	1.	ApiEndpoints.AUTHENTICATION_LOGIN
 * 	2.	ApiEndpoints.AUTHENTICATION_LOGOUT
 **/

@RestController
public class SecurityController {
	
	private static final Logger logger = LoggerFactory.getLogger(SecurityController.class);
	
	/**
	 * Session Service
	 */
	@Autowired
	private SessionService sessionService;
	
	/**
	 * Customers Service
	 */
	@Autowired
	private UserService userService;
	
	/**
	 * Customers Repository
	 */
	@Resource
	private UserRepository userRepository;
	
	/**
	 * Customer sign in controller
	 * method POST
	 * @param document	JSONAPI document contains the data
	 * @return			JSONAPI Document
	 */
	@RequestMapping(value = ApiEndpoints.AUTHENTICATION_LOGIN, method = RequestMethod.POST)
	public JSONApiDocument doLogin(@RequestBody JSONApiDocument document) {
		JSONApiData payload = (JSONApiData) document.getData();
		User customerRequireLogin = (User) payload.convertTo(User.class);
		User customer = userService.signin(customerRequireLogin);
		if( customer == null ){
			logger.debug(ApiEndpoints.AUTHENTICATION_LOGIN + ", Invalid Credentials for email: " + customerRequireLogin.getEmail());
			throw new UserNotFoundException(ApiError.USER_WRONG_CREDENTIALS);
		}		
		Session session = sessionService.startSession(customer);
		if( session == null ){
			logger.debug(ApiEndpoints.AUTHENTICATION_LOGIN + ", Unable to start session.");
			throw new ForbiddenException(ApiError.UNABLE_TO_START_SESSION);
		}
		return new JSONApiDocument(session);
	}
	
	/**
	 * Customer sign out controller
     * @param request
	 * method GET
	 * @return			JSONAPI Document
	 */
	@RequestMapping(value = ApiEndpoints.AUTHENTICATION_LOGOUT, method = RequestMethod.GET)
	public JSONApiDocument doLogout(HttpServletRequest request){
		String token = (String) SecurityContextHolder.getContext().getAuthentication().getCredentials();
		return new JSONApiDocument(sessionService.closeSession(token));
	}

}
