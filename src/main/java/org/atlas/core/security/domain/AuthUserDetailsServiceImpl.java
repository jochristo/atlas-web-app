package org.atlas.core.security.domain;

import org.atlas.modules.users.domain.User;
import org.atlas.modules.users.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System core ( Security UserDetails )
 * @since 2016-07-25
 **/

@Service
public class AuthUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository customerRepository;

    /**
     * Fetching for given Id the Customer.
     * 						***ATTENTION***
     * Username is the UserId fetch from BearerAuthenticationTokenFilter
     * @see org.atlas.core.security.BearerAuthenticationTokenFilter
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User customer = customerRepository.findOne(Long.parseLong(username));
        if (customer == null) {
            throw new UsernameNotFoundException(String.format("No user found with userId '%s'.", username));
        } else {
        	AuthenticatedUser authUser = new AuthenticatedUser(customer.getId(), customer.getEmail());
            return authUser;
        }
    }
}
