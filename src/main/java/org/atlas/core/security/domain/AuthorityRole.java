package org.atlas.core.security.domain;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System core ( Security )
 * @since 2016-07-25
 **/
public enum AuthorityRole {
	
	/**
	 * Logged in User.
	 */
    ROLE_USER, 
    
    /**
     * Administration Role
     */
    ROLE_ADMIN,
    
    /** 
     * Super User (Is not Implement in this version)
     */
    ROLE_SUPERADM
}
