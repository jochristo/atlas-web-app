package org.atlas.core.handlers.errors;

/**
 * RestControllers Error Handler Advisor
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Core ( System Exception )
 * @since 2016-07-27
 **/

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.atlas.api.errors.ApiError;
import org.atlas.core.jsonapi.JSONApiDocument;
import org.atlas.core.jsonapi.JSONApiErrors;
import org.atlas.core.jsonapi.helpers.ErrorSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.util.NestedServletException;

@ControllerAdvice(annotations = {RestController.class})
public class RestControllersHandlerAdvisor{
	
	private static final Logger logger = LoggerFactory.getLogger(RestControllersHandlerAdvisor.class); 
	
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	@ExceptionHandler({org.springframework.transaction.TransactionSystemException.class})
	@ResponseBody
	public JSONApiDocument handleTransactionSystem(TransactionSystemException ex){
		
		logger.error("TransactionSystemException occured", ex.getMostSpecificCause());
		
		JSONApiDocument errorDocument = new JSONApiDocument();
		
		if( ex.getMostSpecificCause() instanceof ConstraintViolationException ){
			ConstraintViolationException constrainViolation = (ConstraintViolationException) ex.getMostSpecificCause();
			ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();
			JSONApiErrors error = null;
			
			Set<ConstraintViolation<?>> violations = constrainViolation.getConstraintViolations();
			for (ConstraintViolation<?> violation : violations ) {
				error = new JSONApiErrors();
				error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
				error.setSource(new ErrorSource(violation.getPropertyPath().toString()));
				error.setTitle("Invalid "+violation.getPropertyPath().toString());
				error.setDetail(violation.getMessage().substring(0, 1).toUpperCase() + violation.getMessage().substring(1));
				errorsList.add(error);
			}
			errorDocument.setErrors(errorsList);
		}else{
			System.out.println("Unimplement Transaction System Error");
		}
		
        return errorDocument;

	}
	
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	@ExceptionHandler({ConstraintViolationException.class})
	@ResponseBody
	public JSONApiDocument handleConstraintViolation(ConstraintViolationException ex){		
		
		logger.error("ConstraintViolationException occured", ex);
		
		JSONApiDocument errorDocument = new JSONApiDocument();
		ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();
		JSONApiErrors error = null;
		
		Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();
		for (ConstraintViolation<?> violation : violations ) {
			error = new JSONApiErrors();
			error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
			error.setSource(new ErrorSource(violation.getPropertyPath().toString()));
			error.setTitle("Invalid "+violation.getPropertyPath().toString());
			error.setDetail(violation.getMessage().substring(0, 1).toUpperCase() + violation.getMessage().substring(1));
			errorsList.add(error);
		}
		errorDocument.setErrors(errorsList);
        return errorDocument;
	}
	
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	@ExceptionHandler({MethodArgumentNotValidException.class})
	@ResponseBody
	public JSONApiDocument handleMethodArgumentNotValid(MethodArgumentNotValidException ex){	
		
		logger.error("MethodArgumentNotValidException occured", ex);
		
		JSONApiDocument errorDocument = new JSONApiDocument();
		ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();
		JSONApiErrors error = new JSONApiErrors();
		
		error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
		error.setCode(ApiError.CONSTRAIN_VIOLATION.getAppCode());
		error.setTitle(HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase());
		error.setDetail(ApiError.CONSTRAIN_VIOLATION.getDetails());
		errorDocument.setErrors(errorsList);
        return errorDocument;
	}
	
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	@ExceptionHandler({DataIntegrityViolationException.class})
	@ResponseBody
	public JSONApiDocument handleDataIntegrityViolation(DataIntegrityViolationException ex){
		
		logger.error("DataIntegrityViolationException occured", ex.getMostSpecificCause());
		
		JSONApiDocument errorDocument = new JSONApiDocument();
		ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();
		JSONApiErrors error = new JSONApiErrors();
		
		error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
		error.setCode(ApiError.CONSTRAIN_VIOLATION.getAppCode());
		error.setTitle(HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase());
		//error.setDetail(ApiError.CONSTRAIN_VIOLATION.getDetails());
		error.setDetail(ex.getMostSpecificCause().getMessage());
		
		errorsList.add(error);
		errorDocument.setErrors(errorsList);
        return errorDocument;
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({ HttpMessageNotReadableException.class, 
		MethodArgumentTypeMismatchException.class, 
		NumberFormatException.class,
		IllegalArgumentException.class,
		NullPointerException.class})
	@ResponseBody
	public JSONApiDocument handleMethodArgumentTypeMismatch(Exception ex){
		
		logger.error("handleMethodArgumentTypeMismatch occured", ex);
		
		JSONApiDocument errorDocument = new JSONApiDocument();
		ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();
		JSONApiErrors error = new JSONApiErrors();
		
		error.setStatus(HttpStatus.BAD_REQUEST.toString());
		error.setCode(ApiError.ILLEGAL_ARGUMENT_TYPE.getAppCode());
		error.setTitle(HttpStatus.BAD_REQUEST.getReasonPhrase());
		error.setDetail(ApiError.ILLEGAL_ARGUMENT_TYPE.getDetails());
		
		errorsList.add(error);
		errorDocument.setErrors(errorsList);
        return errorDocument;
	}
	
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	@ResponseBody
	public JSONApiDocument handleHttpRequestMethodNotSupported(Exception ex){
		
		logger.error("handleHttpRequestMethodNotSupported occured", ex);
		
		JSONApiDocument errorDocument = new JSONApiDocument();
		ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();
		JSONApiErrors error = new JSONApiErrors();
		
		error.setStatus(HttpStatus.BAD_REQUEST.toString());
		error.setCode(ApiError.ILLEGAL_ARGUMENT_TYPE.getAppCode());
		error.setTitle(HttpStatus.BAD_REQUEST.getReasonPhrase());
		error.setDetail(ApiError.ILLEGAL_ARGUMENT_TYPE.getDetails());
		errorsList.add(error);
		errorDocument.setErrors(errorsList);
        return errorDocument;
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler({IOException.class, UsernameNotFoundException.class, NestedServletException.class, ConversionNotSupportedException.class, HttpMessageNotWritableException.class})
	@ResponseBody
	public JSONApiDocument handleIOException(Exception ex){
		
		logger.error("handleIOException occured", ex);
		
		JSONApiDocument errorDocument = new JSONApiDocument();
		ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();
		JSONApiErrors error = new JSONApiErrors();
		
		error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
		error.setCode(ApiError.INTERNAL_SERVER_ERROR.getAppCode());
		error.setTitle(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
		error.setDetail(ApiError.INTERNAL_SERVER_ERROR.getDetails());
		errorsList.add(error);
		errorDocument.setErrors(errorsList);
        return errorDocument;
	}
	
	@ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
	@ExceptionHandler({HttpMediaTypeNotSupportedException.class})
	@ResponseBody
	public JSONApiDocument handleHttpMediaTypeNotSupportedException(Exception ex){
		
		logger.error("handleHttpMediaTypeNotSupportedException occured", ex);
		
		JSONApiDocument errorDocument = new JSONApiDocument();
		ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();
		JSONApiErrors error = new JSONApiErrors();
		
		error.setStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE.toString());
		error.setCode(ApiError.UNSUPPORTED_MEDIA_TYPE.getAppCode());
		error.setTitle(HttpStatus.UNSUPPORTED_MEDIA_TYPE.getReasonPhrase());
		error.setDetail(ApiError.UNSUPPORTED_MEDIA_TYPE.getDetails());
		errorsList.add(error);
		errorDocument.setErrors(errorsList);
        return errorDocument;
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({ MissingServletRequestParameterException.class, 
		ServletRequestBindingException.class, 
		MissingServletRequestPartException.class,
		TypeMismatchException.class})
	@ResponseBody
	public JSONApiDocument handleGeneralBadRequests(Exception ex){
		
		logger.error("handleGeneralBadRequests occured", ex);
		
		JSONApiDocument errorDocument = new JSONApiDocument();
		ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();
		JSONApiErrors error = new JSONApiErrors();
		
		error.setStatus(HttpStatus.BAD_REQUEST.toString());
		error.setCode(ApiError.ILLEGAL_ARGUMENT_TYPE.getAppCode());
		error.setTitle(HttpStatus.BAD_REQUEST.getReasonPhrase());
		error.setDetail(ApiError.ILLEGAL_ARGUMENT_TYPE.getDetails());
		
		errorsList.add(error);
		errorDocument.setErrors(errorsList);
        return errorDocument;
	}
	

}
