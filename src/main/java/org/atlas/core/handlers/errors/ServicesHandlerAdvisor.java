package org.atlas.core.handlers.errors;

/**
 * RestControllers Error Handler Advisor
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Core ( System Exception )
 * @since 2016-07-27
 **/

import java.util.ArrayList;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.atlas.api.errors.ApiError;
import org.atlas.core.jsonapi.JSONApiDocument;
import org.atlas.core.jsonapi.JSONApiErrors;
import org.atlas.core.jsonapi.helpers.ErrorSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice(annotations = {Service.class})
public class ServicesHandlerAdvisor{
	
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	@ExceptionHandler({ConstraintViolationException.class})
	@ResponseBody
	public JSONApiDocument handleConstraintViolation(ConstraintViolationException ex){				
		JSONApiDocument errorDocument = new JSONApiDocument();
		ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();
		JSONApiErrors error = null;
		
		Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();
		for (ConstraintViolation<?> violation : violations ) {
			error = new JSONApiErrors();
			error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
			error.setSource(new ErrorSource(violation.getPropertyPath().toString()));
			error.setTitle("Invalid "+violation.getPropertyPath().toString());
			error.setDetail(violation.getMessage().substring(0, 1).toUpperCase() + violation.getMessage().substring(1));
			errorsList.add(error);
		}
		errorDocument.setErrors(errorsList);
        return errorDocument;
	}
	
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	@ExceptionHandler({MethodArgumentNotValidException.class})
	@ResponseBody
	public JSONApiDocument handleMethodArgumentNotValid(MethodArgumentNotValidException ex){				
		JSONApiDocument errorDocument = new JSONApiDocument();
		ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();
		JSONApiErrors error = new JSONApiErrors();
		
		error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
		error.setCode(ApiError.CONSTRAIN_VIOLATION.getAppCode());
		error.setTitle(HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase());
		error.setDetail(ApiError.CONSTRAIN_VIOLATION.getDetails());
		errorDocument.setErrors(errorsList);
        return errorDocument;
	}
	
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	@ExceptionHandler({DataIntegrityViolationException.class})
	@ResponseBody
	public JSONApiDocument handleDataIntegrityViolation(DataIntegrityViolationException ex){
		
		JSONApiDocument errorDocument = new JSONApiDocument();
		ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();
		JSONApiErrors error = new JSONApiErrors();
		
		error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
		error.setCode(ApiError.CONSTRAIN_VIOLATION.getAppCode());
		error.setTitle(HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase());
		//error.setDetail(ApiError.CONSTRAIN_VIOLATION.getDetails());
		error.setDetail(ex.getMostSpecificCause().getMessage());
		
		errorsList.add(error);
		errorDocument.setErrors(errorsList);
        return errorDocument;
	}

}
