package org.atlas.core.handlers.errors;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.atlas.api.ApiEndpoints;
import org.atlas.core.jsonapi.JSONApiDocument;
import org.atlas.core.jsonapi.JSONApiErrors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System core ( Error Controller )
 * @since 2016-07-25
 * endpoints
 * 	1.	ApiEndpoints.SPRING_BOOT_ERRORS
 **/

@RestController
public class CustomErrorController implements ErrorController {
	
	@Autowired
    private ErrorAttributes errorAttributes;
	
	private static final Logger logger = LoggerFactory.getLogger(CustomErrorController.class);
	
	/**
	 * Handling the Errors occured.
	 * @param request	The Request produce the Errors
	 * @param response	The Response for the Errors occured.
	 * @return	JSONApiDocument
	 */
	@RequestMapping(value = ApiEndpoints.SPRING_BOOT_ERRORS, method={RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT}, produces = "application/json")
    JSONApiDocument error(HttpServletRequest request, HttpServletResponse response) {
		JSONApiDocument errorDocument = new JSONApiDocument();
		ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();
		JSONApiErrors error = new JSONApiErrors();
		
		Map<String, Object> errorAttributes = getErrorAttributes(request, false);
		
		String appCode = (String) request.getAttribute("appCode");
		String details = (String) request.getAttribute("details");
		String status = String.valueOf(errorAttributes.get("status"));
		String message = String.valueOf(errorAttributes.get("message"));
		//timestamp,status,error,exception,message,path
		
		switch( status ){
		
			case "403"://HttpStatus.METHOD_NOT_ALLOWED
				appCode = status;
				details = message;
				message = HttpStatus.FORBIDDEN.getReasonPhrase();
			break;
			
			case "404"://HttpStatus.NOT_FOUND
				appCode = status;
				details = message;
				message = HttpStatus.NOT_FOUND.getReasonPhrase();
			break;
			
			case "405"://HttpStatus.METHOD_NOT_ALLOWED
				appCode = status;
				details = message;
				message = HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase();
			break;
			
			case "500": //
				appCode = "500";
				details = "INTERNAL_SERVER_ERROR";
				message = HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
			break;
				
			default:
				logger.error("Application exception occured: Status: " + status + " ( " + message + " )");
			break;
		
		}
		
		error.setStatus(status);
		error.setCode((appCode==null)?String.valueOf(errorAttributes.get("appCode")):appCode);
		error.setTitle(message);
		error.setDetail((details==null)?String.valueOf(errorAttributes.get("details")):details);
		errorsList.add(error);
		errorDocument.setErrors(errorsList);
        return errorDocument;
    }
	
	/**
	 * Return the errors for the request.
	 * @param request 	The HTTP Request processed
	 * @param includeStackTrace 
	 * @return	Map
	 */
	private Map<String, Object> getErrorAttributes(HttpServletRequest request, boolean includeStackTrace) {
        RequestAttributes requestAttributes = new ServletRequestAttributes(request);
        return errorAttributes.getErrorAttributes(requestAttributes, includeStackTrace);
    }

	/**
	 * Return the Error Endpoint
	 * @return String	Endpoint Path
	 */
	@Override
	public String getErrorPath() {
		return ApiEndpoints.SPRING_BOOT_ERRORS;
	}

}
