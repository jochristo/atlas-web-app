package org.atlas.core.schedulers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Core Services ( Scheduler Properties )
 * @since 2016-07-27
 **/

@Component
@ConfigurationProperties("atlas.scheduler")
public class SchedulerProperties {

	/**
	 * Scheduler Max Runnable threads.
	 */
	@Value("${atlas.scheduler.max-threads}")
    private int maxThreads = 5;
	
	@Value("${atlas.scheduler.task-processor-interval}")
	private long taskProcessorInterval;
	
	public int getMaxThreads() {
		return maxThreads;
	}

	public void setMaxThreads(int maxThreads) {
		this.maxThreads = maxThreads;
	}
	
	public long getTaskProcessorInterval() {
		return taskProcessorInterval;
	}

	public void setTaskProcessorInterval(long taskProcessorInterval) {
		this.taskProcessorInterval = taskProcessorInterval;
	}
	
}
