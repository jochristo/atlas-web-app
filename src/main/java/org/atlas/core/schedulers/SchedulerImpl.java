package org.atlas.core.schedulers;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;

import org.atlas.api.Scheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Core Services ( Scheduler )
 * @since 2016-07-27
 **/

@Service
public class SchedulerImpl implements Scheduler{
	
	private static final Logger logger = LoggerFactory.getLogger(Scheduler.class);
	
	/**
	 * Scheduler Properties
	 */
	@Autowired
	private SchedulerProperties schedulerProperties;
	
	/**
	 * Stored tasks Queue
	 */
	private Queue<Runnable> scheduledEvents =  new ConcurrentLinkedQueue<>();
	
	/**
	 * Queue Executor
	 */
	private ExecutorService executor;
	
	@PostConstruct
	public void initializeLinkedQueue(){
		//logger.debug("Post Cunstruct initialization of Scheduler Max Threads to: " + this.schedulerProperties.getMaxThreads());
		this.executor = Executors.newFixedThreadPool(10);//this.schedulerProperties.getMaxThreads()
	}
	
	public SchedulerImpl() {}
	
	/**
	 * Process System events
	 */
	@Scheduled(fixedRateString="${atlas.scheduler.task-processor-interval}")
    public void processEvents() {
		Runnable curRunnable;
        while ((curRunnable = scheduledEvents.poll())!=null) {        	
            executor.submit(curRunnable);
        }
	}
	
	/**
	 * Submit a new job to be Executed.
	 * @param job	The Runnable to be executed.
	 */
	@Override
	public void submit(Runnable job){
		this.scheduledEvents.add(job);
	}

}
