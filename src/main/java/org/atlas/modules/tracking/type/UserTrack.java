package org.atlas.modules.tracking.type;

/**
 * 
 * Tracking customer actions
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System modules ( Core )
 * @since 2016-08-01
 **/

import org.atlas.api.Trackable;
import org.atlas.modules.tracking.TrackType;
import org.atlas.modules.users.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public class UserTrack implements Trackable {
	
	private static final Logger logger = LoggerFactory.getLogger(UserTrack.class);
	
	public UserTrack(User customer, TrackType type){		
	}

	@Override
	public Object track() {
		return null;
	}

}
