package org.atlas.modules.tracking;

/** 
 * System Actions (E.g User profile changed)
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System modules ( Core )
 * @since 2016-08-01
 **/
public enum TrackType{
	
	/**
	 * User profile change action
	 */
	USER_PROFILE_CHANGE;

}