package org.atlas.modules.tracking.converters;


/** 
 * TrackType convertor 
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System modules ( Core )
 * @since 2016-08-01
 **/

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.atlas.modules.tracking.TrackType;

@Converter
public class TrackTypeConverter implements AttributeConverter<TrackType, Integer>{

	@Override
	public Integer convertToDatabaseColumn(TrackType attribute) {
		switch (attribute) {
	        case USER_PROFILE_CHANGE:
	            return 1;
	        default:
	            throw new IllegalArgumentException("Unknown" + attribute);
	    }
	}

	@Override
	public TrackType convertToEntityAttribute(Integer dbData) {
		switch (dbData) {
		 	case 1:
	            return TrackType.USER_PROFILE_CHANGE;
	        default:
	            throw new IllegalArgumentException("Unknown" + dbData);
	    }
	}

}
