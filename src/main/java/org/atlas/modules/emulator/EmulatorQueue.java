package org.atlas.modules.emulator;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Module ( Customer Entity )
 * @since 2016-07-27
 **/


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="emulator_queues")
public class EmulatorQueue{

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JAPIIdentifier
    @JsonIgnore
    private long id;
    
    @JAPIAttribute
    @NotNull
    @Column(name = "queue_name")
    private String name;
    
    @JAPIAttribute
    @NotNull
    private String topic;
    
    @JAPIAttribute
    @NotNull
    private boolean json;    

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}
	
	public boolean isJson() {
		return json;
	}

	public void setJson(boolean json) {
		this.json = json;
	}
    
}