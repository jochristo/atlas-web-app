package org.atlas.modules.emulator;

public class EmulationModel {

	private String message;
	private long topic;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public long getTopic() {
		return topic;
	}

	public void setTopic(long topic) {
		this.topic = topic;
	}
	
}
