package org.atlas.modules.emulator;

import org.atlas.api.ApiEndpoints;
import org.atlas.api.services.EmulationService;
import org.atlas.core.jsonapi.JSONApiData;
import org.atlas.core.jsonapi.JSONApiDocument;
import org.atlas.modules.integrators.IntegrationGateway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Core ( Security Controllers )
 * @since 2016-07-27
 **/

@RestController
public class EmulationController {
	
	private static final Logger logger = LoggerFactory.getLogger(EmulationController.class);
	
	@Autowired
	private IntegrationGateway gateway;
	
	@Autowired
	private EmulationService emulationService;
	
	@RequestMapping(value = ApiEndpoints.EMULATION, method = RequestMethod.POST)
	public JSONApiDocument sendEmulationData(@RequestBody JSONApiDocument document){
		JSONApiData payload = (JSONApiData) document.getData();
		EmulationModel emulation = (EmulationModel)payload.convertTo(EmulationModel.class);
		emulationService.sendEmulation(emulation.getTopic(), emulation.getMessage());
		return new JSONApiDocument();
	}
	
	@RequestMapping(value = ApiEndpoints.EMULATION_QUEUES, method = RequestMethod.GET)
	public JSONApiDocument getEmulationQueues(){
		return new JSONApiDocument(emulationService.getQueues());
	}
	
	
}
