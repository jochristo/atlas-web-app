package org.atlas.modules.emulator;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.atlas.api.errors.ApiError;
import org.atlas.api.exceptions.EmulationException;
import org.atlas.api.services.EmulationService;
import org.atlas.core.helpers.Generator;
import org.atlas.modules.integrators.IntegrationGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class EmulationServiceImpl implements EmulationService{

	private ObjectMapper mapper = new ObjectMapper();
	
	@Resource
	private EmulatorQueuesRepository queuesRepo;
	
	@Autowired
	private IntegrationGateway gateway;
	
	@Override
	public EmulationData sendEmulation(long queueId, String message) {
		EmulatorQueue queue = this.queuesRepo.findOne(queueId);
		if( queue == null ) throw new EmulationException(ApiError.EMULATION_INVALID_QUEUE);
		
		if( queue.isJson() ){
			try {
				this.mapper.readTree(message);
			} catch (IOException e) {
				throw new EmulationException(ApiError.EMULATION_JSON_REQUIRED);
			}
		}
		EmulationData data = new EmulationData();
		data.setTopic(queue.getTopic());
		data.setMessage(message);
		data.setUuid(Generator.generateHash());
		return gateway.execUnityEmulation(data);
	}

	@Override
	public List<EmulatorQueue> getQueues() {
		return queuesRepo.findAll();
	}
	
}
