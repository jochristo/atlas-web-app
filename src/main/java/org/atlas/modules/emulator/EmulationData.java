package org.atlas.modules.emulator;

public class EmulationData {

	private String uuid;
	private String topic;
	private String message;
        
        private boolean emulation = true; // temporary solution - remove in normal operation
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

    public boolean isEmulation() {
        return emulation;
    }

    public void setEmulation(boolean emulation) {
        this.emulation = emulation;
    }

        
	
        
}
