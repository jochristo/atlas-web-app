package org.atlas.modules.localization;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Module ( Customer Entity )
 * @since 2016-07-27
 **/

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="locations")
public class Location{

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JAPIIdentifier
    @JsonIgnore
    private long id;
    
    @JAPIAttribute
    @Column(name = "aalhouse_id")
	private long aalhouse;
    
    @JAPIAttribute
    @Column(name = "created_at",insertable=true, updatable=false)
	private Date createdAt;
    
    @JAPIAttribute
    @Column(name = "object")
	private String object;

    @JAPIAttribute
    @Column(name = "location")
	private String location;

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getAalhouse() {
		return aalhouse;
	}

	public void setAalhouse(long aalhouse) {
		this.aalhouse = aalhouse;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
    
}