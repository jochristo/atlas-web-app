package org.atlas.modules.localization;


/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Core ( Locations Repository )
 * @since 2016-07-27
 **/

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {
	
	public List<Location> findTop20ByAalhouseAndObjectOrderByCreatedAtDesc(long aalhouse, String object);
	
}