package org.atlas.modules.localization;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Core ( User Service Implementation )
 * @since 2016-07-27
 * @see com.sol4mob.api.services.UserService
 **/

import java.util.List;

import javax.annotation.Resource;

import org.atlas.api.services.LocalizationService;
import org.springframework.stereotype.Service;

@Service
public class LocalizationServiceImpl implements LocalizationService {

	@Resource
	private LocationRepository locationRepo;
	
	@Override
	public List<Location> getObservedLocationsByHouse(long aalhouse, String keys) {
		return this.locationRepo.findTop20ByAalhouseAndObjectOrderByCreatedAtDesc(aalhouse, keys);
	}

}
