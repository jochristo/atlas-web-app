package org.atlas.modules.localization;

/**
 * 
 * Customer profiles Rest Controller
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System modules ( Customers )
 * @since 2016-07-29
 * @endpoints
 * 	1.	ApiEndpoints.USER_PROFILE
 **/

import java.util.List;

import org.atlas.api.ApiEndpoints;
import org.atlas.api.services.LocalizationService;
import org.atlas.core.jsonapi.JSONApiDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LocalizationController {
	
//	private static final Logger logger = LoggerFactory.getLogger(LocalizationController.class);
	
	@Autowired
	private LocalizationService localizServ;
	
	/**
	 * Retrieve history
     * @param aalhouseId The aal house id
     * @param object The object to retrieve
	 * @return	JSONApiDocument
	 */
	@RequestMapping(value = ApiEndpoints.RELATIVE_OBSERVATIONS, method = RequestMethod.GET)
	public JSONApiDocument getRelativeObservations(@PathVariable long aalhouseId, @PathVariable String object) {
		List<Location> locations = localizServ.getObservedLocationsByHouse(aalhouseId, object);
		return new JSONApiDocument(locations);
	}
	
	
}
