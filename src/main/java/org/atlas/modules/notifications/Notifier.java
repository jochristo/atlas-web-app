package org.atlas.modules.notifications;

import java.util.List;

/**
 * System Notifier interface
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Modules ( Notifications )
 * @since 2016-09-26
 **/

public interface Notifier {

	public List<Recipient> getRecipients();
	public Sender getSender();
	
}
