package org.atlas.modules.notifications;

/**
 * System Notifier (Sender)
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Modules ( Notifications )
 * @since 2016-09-26
 **/
public class Sender {
	
	private String fullname;
	private String email;
	private long phone;
	
	public Sender(){}
	
	public Sender(String email){
		this.email = email;
	}
	
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getPhone() {
		return phone;
	}
	public void setPhone(long phone) {
		this.phone = phone;
	}
	
	

}
