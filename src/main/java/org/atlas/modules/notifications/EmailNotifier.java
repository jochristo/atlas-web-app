package org.atlas.modules.notifications;

import java.util.List;

/**
 * System Notifier interface (Emails Notifier)
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Modules ( Notifications )
 * @since 2016-09-26
 **/

public interface EmailNotifier extends Notifier {

	public String getSubject();
	public String getTemplate();
	public List<TemplateVariable> getTemplateVariables();
	public List<Recipient> getCC();
	public List<Recipient> getBCC();
	public String getReferenceUrl();
	
}
