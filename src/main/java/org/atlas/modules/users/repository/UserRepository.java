package org.atlas.modules.users.repository;


/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Core ( Customer Repository )
 * @since 2016-07-27
 **/

import org.atlas.modules.users.domain.User;
import org.atlas.modules.users.domain.UserStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	/**
	 * Find a customer By Email Address
	 * @param email		Email to check
	 * @return			Customer
	 */
	User findByEmail(String email);
	
	/**
	 * Find a customer By Email and status
	 * @param email		Email to check
	 * @param status	Status to check
	 * @return			Customer
	 */
	User findByEmailAndStatus(String email, UserStatus status);
	
	/**
	 * Find a customer By Email,Password and status
	 * @param email		Email to check
         * @param password the  password
	 * @param status	Status to check
	 * @return			Customer
	 */
	User findByEmailAndPasswordAndStatus(String email, String password, UserStatus status);
	
	
	
}