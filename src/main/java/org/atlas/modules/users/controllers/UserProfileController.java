package org.atlas.modules.users.controllers;

/**
 * 
 * Customer profiles Rest Controller
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System modules ( Customers )
 * @since 2016-07-29
 * @endpoints
 * 	1.	ApiEndpoints.USER_PROFILE
 **/


import javax.annotation.Resource;

import org.atlas.api.ApiEndpoints;
import org.atlas.api.repositories.CountryRepository;
import org.atlas.api.services.UserService;
import org.atlas.core.jsonapi.JSONApiDocument;
import org.atlas.modules.users.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserProfileController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserProfileController.class);
	
	@Autowired
	private UserService customerService;
	
	@Resource
	private CountryRepository countryRepository;
	
	/**
	 * Retrieve Customer Profile
	 * @return	JSONApiDocument
	 */
	@RequestMapping(value = ApiEndpoints.USER_PROFILE, method = RequestMethod.GET)
	public JSONApiDocument getProfile() {
		User customer = customerService.getUser();
		return new JSONApiDocument(customer);
	}
	
	/**
	 * Update customer Profile
     * @param document the profile document
     * @return  JSONApiDocument
	 */
	@RequestMapping(value = ApiEndpoints.USER_PROFILE, method = RequestMethod.PUT)
	public JSONApiDocument updateProfile(@RequestBody JSONApiDocument document) {
		return new JSONApiDocument();
	}
	
}
