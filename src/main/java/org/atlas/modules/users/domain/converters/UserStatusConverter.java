package org.atlas.modules.users.domain.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.atlas.modules.users.domain.UserStatus;

@Converter
public class UserStatusConverter implements AttributeConverter<UserStatus, Integer>{

	@Override
	public Integer convertToDatabaseColumn(UserStatus attribute) {
		switch (attribute) {
	        case REGISTERED:
	            return 1;
	        case VERIFIED:
	            return 2;
	        case EMAIL_CHANGED:
	            return 3;
	        case PASSWORD_RESET:
	        	return 4;
	        case DISABLED:
	        	return 8;
	        case DELETED:
	        	return 9;
	        default:
	            throw new IllegalArgumentException("Unknown" + attribute);
	    }
	}

	@Override
	public UserStatus convertToEntityAttribute(Integer dbData) {
		switch (dbData) {
	        case 1:
	            return UserStatus.REGISTERED;
	        case 2:
	            return UserStatus.VERIFIED;
	        case 3:
	            return UserStatus.EMAIL_CHANGED;
	        case 4:
	            return UserStatus.PASSWORD_RESET;
	        case 8:
	            return UserStatus.DISABLED;
	        case 9:
	            return UserStatus.DELETED;
	        default:
	            throw new IllegalArgumentException("Unknown" + dbData);
	    }
	}
	
}
