package org.atlas.modules.users.domain;

/**
 * Customer Status
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System modules ( Customers )
 * @since 2016-07-31
 **/
public enum UserStatus{
	
	/**
	 * Customer Registered
	 */
	REGISTERED,
	
	/**
	 * Customer verify his/her Customer address.
	 */
	VERIFIED,
	
	/***
	 * Customer request Email address change
	 */
	EMAIL_CHANGED,
	
	/**
	 * Customer request password reset
	 */
	PASSWORD_RESET,
	
	/**
	 * Customer Disabled by Admin
	 */
	DISABLED,
	
	/**
	 * Customer deleted
	 */
	DELETED;

}
