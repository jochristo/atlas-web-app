package org.atlas.modules.users;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Core ( User Service Implementation )
 * @since 2016-07-27
 * @see com.sol4mob.api.services.UserService
 **/

import javax.annotation.Resource;

import org.atlas.api.Scheduler;
import org.atlas.api.services.CountriesService;
import org.atlas.api.services.UserService;
import org.atlas.core.security.domain.AuthenticatedUser;
import org.atlas.modules.users.domain.User;
import org.atlas.modules.users.domain.UserStatus;
import org.atlas.modules.users.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class UserServiceImpl implements UserService {
	
	/**
	 * Main system scheduler
	 */
	@Autowired
	private Scheduler scheduler;
	
	/**
	 * Customer Repository
	 */
	@Resource
	private UserRepository customerRepository;
	
	@Autowired
	private CountriesService countryService;
	
	private final ObjectMapper mapper = new ObjectMapper();

	/**
	 * Update Customer Profile
	 */
	@Override
	public User updateUserProfile(User user, UserStatus status){
		return customerRepository.save(user);
	}

	/**
	 * Verify Customer login credentials
	 */
	@Override
	public User signin(User user) {
		return customerRepository.findByEmailAndPasswordAndStatus(user.getEmail(), user.getPassword(), UserStatus.VERIFIED);
	}	

	@Override
	public User getUser() {
		AuthenticatedUser authUser = (AuthenticatedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return customerRepository.findOne(authUser.getId());
	}

	@Override
	public User getUser(long userId) {
		return customerRepository.findOne(userId);
	}

}
