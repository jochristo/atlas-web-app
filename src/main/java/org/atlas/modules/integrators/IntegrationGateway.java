package org.atlas.modules.integrators;

import org.atlas.modules.emulator.EmulationData;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

@MessagingGateway(name = "MainIntegrationGateway")
public interface IntegrationGateway {
	
    @Gateway(requestChannel="startEmulationChannel", replyChannel="emulationResultChannel", replyTimeout=5000)
    public EmulationData execUnityEmulation(EmulationData emulation);

}
