package org.atlas.modules.integrators;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.RendezvousChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.scheduling.PollerMetadata;
import org.springframework.scheduling.support.PeriodicTrigger;

@Configuration
@EnableIntegration
public class IntegrationConfig {
	
	private RendezvousChannel intermediateChannel;
	private RendezvousChannel replyChannel;
	
	@PostConstruct
	public void init(){
		this.intermediateChannel = new RendezvousChannel();
		this.replyChannel = new RendezvousChannel();
	}
	
	@Bean(name = PollerMetadata.DEFAULT_POLLER)
	public PollerMetadata defaultPoller() {
	    PollerMetadata pollerMetadata = new PollerMetadata();
	    pollerMetadata.setTrigger(new PeriodicTrigger(10));
	    return pollerMetadata;
	}
	
	public RendezvousChannel getIntermediateChannel(){
		return this.intermediateChannel;
	}
	
	public RendezvousChannel getReplyChannel(){
		return this.replyChannel;
	}
	
	@Bean
	public RendezvousChannel startEmulationChannel(){
		return new RendezvousChannel();
	}
	
	@Bean
	public RendezvousChannel intermediaEmulationChannel(){
		return this.getIntermediateChannel();
	}
	
	@Bean
	public RendezvousChannel emulationResultChannel(){
		return this.getReplyChannel();
	}	
	
}
