package org.atlas.modules.integrators;

public class MqttData {

	private String uuid;
	private String payload;
	
	public MqttData(){}
	
	public MqttData(String uuid, String payload){
		this.uuid = uuid;
		this.payload = payload;
	}
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getPayload() {
		return payload;
	}
	public void setPayload(String payload) {
		this.payload = payload;
	}
	
}
