/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.modules.integrators;

/**
 *
 * @author ic
 */
public abstract class ApiMqtt
{
    public static final String MONITORING_REQUEST_TOPIC_PREFIX = "atlas/monitoring/gateways/";    
    
    public static final String COMMANDS_REQUEST_TOPIC_PREFIX = "atlas/commands/gateways/";
    
    public static final String WSN_DATA_RESPONSE_TOPIC_PREFIX = "atlas/data/";
    
    public static final String WSN_DATA_HUMIDITY = "/humidity";
    
    public static final String WSN_DATA_LIGHT = "/light";
    
    public static final String WSN_DATA_TEMPERATURE = "/temperature";
    
    public static final String ALERTS_RESPONSE_TOPIC = "atlas/alerts"; 
    
    public static final String NOTIFICATIONS_RESPONSE_TOPIC = "atlas/notifications"; 
    
    public static final String PING_TYPE = "PING";
    
    public static final String RESOURCES_TYPE = "RESOURCES";
    
    public static final String DEVICE_TYPE = "DEVICE";
    
    public static final String OPERATING_SYSTEM_TYPE = "OPERATING_SYSTEM";    
    
    public static final String BATTERY_INDICATION_TYPE = "BATTERY_INDICATION";    
    
    public static final String APPLICATION_TYPE = "APPLICATION";  
    
}
