package org.atlas.modules.integrators;


import org.atlas.modules.emulator.EmulationData;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Component
public class UnityActivators {
	
	@ServiceActivator(inputChannel="startEmulationChannel", outputChannel="mqttOutboundChannel")
	public EmulationData getResponse(Message<EmulationData> msg) {
		return msg.getPayload();
	}
	
	@ServiceActivator(inputChannel="intermediaEmulationChannel", outputChannel="emulationResultChannel")
	public EmulationData getResponseFromIntermediate(Message<EmulationData> msg) {
		return msg.getPayload();
	}

}
