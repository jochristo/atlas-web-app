package org.atlas.modules.integrators;

import org.atlas.modules.mqtt.MqttMessageHandler;
import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.atlas.modules.emulator.EmulationData;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.atlas.api.services.AALHouseService;

@Component
public class MQTTHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(MQTTHandler.class);
	private ObjectMapper jsonMapper = new ObjectMapper();
	
	@Autowired
	private IntegrationConfig configuration;
        
	@Autowired
	private AALHouseService aalHouseService;        
	
	@Value("${org.atlas.cloud.mqtt.host}")
	private String cloudMqttServerHost;
	
	//Cloud Communication Broker
	private int qos = 0;
	private final String USERNAME = "admin";
	private final char[] PASSWORD = {'d','3','W','n','D','e','P','n','a','E','6','Y','G','T','i','8','F','E','l','P'};
	private MqttClient mqclient = null;
	private MemoryPersistence persistence = null;
        
        @Autowired
        private MqttMessageHandler mqttMessageHandler;
	
	private HashMap<String, Message<EmulationData>> channelMsgsMap;//TODO Fix better
	
	@PostConstruct
	public void  initMQTTHandler(){
		this.channelMsgsMap = new HashMap<String, Message<EmulationData>>();
		this.persistence = new MemoryPersistence();
		try {
			this.mqclient = new MqttClient(this.cloudMqttServerHost, "AtlasWebAppMqttHandler", this.persistence);
			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setUserName(USERNAME);
			connOpts.setPassword(PASSWORD);
			connOpts.setCleanSession(true);
                        // set automatic reconnect
                        connOpts.setAutomaticReconnect(true);
			this.mqclient.connect(connOpts);
			logger.info("Cloud connection established...");
			logger.info("Setting messages listener");
			this.mqclient.setCallback(new CloudListener(this, configuration.getIntermediateChannel(), configuration.getReplyChannel(),mqttMessageHandler));
			logger.info("Setting messages listener (Done)");
			
                        // subscribe to topics : COMMANDS
                        this.mqclient.subscribe("/emulation/sensor/response");//TODO must be dynamic
                        this.mqclient.subscribe(ApiMqtt.MONITORING_REQUEST_TOPIC_PREFIX + "#", 2);
                        logger.info("Subscribing to " + ApiMqtt.MONITORING_REQUEST_TOPIC_PREFIX + "#" + " topic");
                        this.mqclient.subscribe(ApiMqtt.COMMANDS_REQUEST_TOPIC_PREFIX + "+" + "/response", 2);
                        logger.info("Subscribing to " + ApiMqtt.COMMANDS_REQUEST_TOPIC_PREFIX + "+" + "/response" + " topic");
                        
                        // DYNAMIC SUBSCIPTION TO GATEWAY TOPICS
                        /*
                        List<Gateway> gateways = aalHouseService.getGateways();                        
                        for(Gateway gw : gateways){
                            this.mqclient.subscribe(ApiMqtt.MONITORING_REQUEST_TOPIC_PREFIX + gw.getIdentity(),0);                            
                            logger.info("Subscribing to " + ApiMqtt.MONITORING_REQUEST_TOPIC_PREFIX + gw.getIdentity() + " topic");
                            this.mqclient.subscribe(ApiMqtt.COMMANDS_REQUEST_TOPIC_PREFIX + gw.getIdentity() + "/response",0);                            
                            logger.info("Subscribing to " + ApiMqtt.COMMANDS_REQUEST_TOPIC_PREFIX + gw.getIdentity() + "/response" + " topic");
                        }
                        */
                        
		} catch (MqttException e) {
			logger.error("Error while trying to connect to Cloud provider: ",e); 
		}
	}
	
	@Bean
    public MessageChannel mqttOutboundChannel() {
        return new DirectChannel();
    }
	
	@ServiceActivator(inputChannel="mqttOutboundChannel")
	public void handleMQTTOutboundMessages(Message<EmulationData> msg){
		logger.info("Sending message with UUID: " + msg.getPayload().getUuid() + ", on Topic: " + msg.getPayload().getTopic());
		try {                        
                        if(!msg.getPayload().isEmulation()) // temp
                        {
                            this.channelMsgsMap.put(msg.getPayload().getUuid(), msg);                            
                            MqttMessage mqttMsg = new MqttMessage(msg.getPayload().getMessage().getBytes());
                            this.mqclient.publish(msg.getPayload().getTopic(), mqttMsg);                               
                        }
                        else
                        {
                            this.channelMsgsMap.put(msg.getPayload().getUuid(), msg);
                            MqttData data = new MqttData(msg.getPayload().getUuid(), msg.getPayload().getMessage());
                            MqttMessage mqttMsg = new MqttMessage(this.jsonMapper.writeValueAsBytes(data));
                            this.mqclient.publish(msg.getPayload().getTopic(), mqttMsg);
                        }
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Message<EmulationData> getLastMessage(String uuid){
		Message<EmulationData> lastMsg = this.channelMsgsMap.get(uuid);
		this.channelMsgsMap.remove(uuid);
		return lastMsg;
	}

}
