package org.atlas.modules.integrators;

import org.atlas.modules.mqtt.MqttMessageHandler;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.channel.RendezvousChannel;
import org.springframework.integration.core.MessagingTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

public class CloudListener implements MqttCallback{
	
	private static final Logger logger = LoggerFactory.getLogger(CloudListener.class);
	private ObjectMapper jsonMapper = new ObjectMapper();

	private RendezvousChannel channel;
	private RendezvousChannel replyChannel;
	private MQTTHandler handler;
	private MessagingTemplate template;  
    
        @Autowired
        MqttMessageHandler mqttMessageHandler;
	
        @Autowired
	public CloudListener(MQTTHandler handler, RendezvousChannel channel, RendezvousChannel replyChannel, final MqttMessageHandler mqttMessageHandler){
		this.channel = channel;
		this.replyChannel = replyChannel;
		this.handler = handler;
		this.template = new MessagingTemplate();
                this.mqttMessageHandler = mqttMessageHandler;
	}
	
	@Override
	public void connectionLost(Throwable cause) {
		// TODO Auto-generated method stub
		logger.error("Connection lost", cause);
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception
        {		                
            // Handle message according to type
            logger.info("Message received on topic:" + topic);
            mqttMessageHandler.setTopic(topic);
            mqttMessageHandler.setMqttMessage(message);
            mqttMessageHandler.setTimestamp(new java.util.Date());
            mqttMessageHandler.handle(message.getPayload(), topic);

            //MqttData data = this.jsonMapper.readValue(message.getPayload(), MqttData.class);
            //MqttMessage mqttMessage = this.jsonMapper.readValue(message.getPayload(), MqttMessage.class);
            //Message<EmulationData> mymessage = MessageBuilder.fromMessage(this.handler.getLastMessage(data.getUuid())).build();                                
            //this.template.send(this.channel, mymessage);
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token)
        {
            // TODO Auto-generated method stub
            // TODO Check what means here delivered, delivered to broker, or delivered on subscriber
            // If on broker, get message 
            logger.info("Message Delivered with ID: " + token.getMessageId());
	}

}
