/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.modules.mqtt.handlers;

/**
 * Designates a gateway command handler.
 * @author ic
 */
public interface IGatewayCommandHandler
{
    /**
     * Handles received MQTT command message.
     */
    public abstract void handle();    
    
}
