package org.atlas.modules.mqtt.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.atlas.api.services.AALHouseService;
import org.atlas.cache.CacheService;
import org.atlas.modules.integrators.ApiMqtt;
import org.atlas.modules.mqtt.GatewayCommand;
import org.atlas.modules.mqtt.model.CommandResponse;
import org.atlas.modules.mqtt.model.MonitoringResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.stereotype.Component;

/**
 * Implements methods to deliver gateway command handling and type resolving.
 * @author ic
 * @param <T> The type  of the 
 */
@Component("gatewayCommandResponseResolver")
public class GatewayCommandResponseResolveImpl<T extends Object> implements IGatewayCommandResponseResolve
{
    // message fields
    private byte [] payload;  
    private String topic;
    
    // cache fields
    private String keyOfHash = null; // key of hash to store in cache
    private String hashKey = null; // the hashkey in cache  
    private T object = null; // the value for hashkey at keyofhash
    private Class<T> responseType;
    private ObjectMapper mapper = new ObjectMapper();         
    
    @Autowired
    private CacheService cacheService; // redis cache client service 
    
    @Autowired
    private AALHouseService aalHouseService;  
    
    private IGatewayCommandHandler gatewayCommandHandler; // gateway command response handler

    public GatewayCommandResponseResolveImpl() {
    }    
    
    public GatewayCommandResponseResolveImpl(byte[] payload, String topic) {
        this.payload = payload;
        this.topic = topic;
        try {            
            resolveCommandType(payload,topic);
        } catch (IOException ex) {
            Logger.getLogger(GatewayCommandResponseResolveImpl.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    
    public GatewayCommandResponseResolveImpl(byte[] payload, String topic, final CacheService cacheService) {
        this.payload = payload;
        this.topic = topic;
        this.cacheService = cacheService;
        try {            
            resolveCommandType(payload,topic);
        } catch (IOException ex) {
            Logger.getLogger(GatewayCommandResponseResolveImpl.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
        
    public GatewayCommandResponseResolveImpl(byte[] payload, String topic, final CacheService cacheService, final AALHouseService aalHouseService) {
        this.payload = payload;
        this.topic = topic;
        this.cacheService = cacheService;
        this.aalHouseService = aalHouseService;
        try {            
            resolveCommandType(payload,topic);
        } catch (IOException ex) {
            Logger.getLogger(GatewayCommandResponseResolveImpl.class.getName()).log(Level.SEVERE, null, ex);
        }         
    }
    
    /**
     * Resolves command type.
     * @param payload The command message payload
     * @param topic The topic of the command message
     * @throws IOException Thrown if ObjectMapper read operations fail
     */
    @Override
    public void resolveCommandType(byte[] payload, String topic) throws IOException
    {
        this.payload = payload;
        this.topic = topic;
        
        if(this.topic.startsWith(ApiMqtt.COMMANDS_REQUEST_TOPIC_PREFIX) && this.topic.endsWith("response")) // COMMANDS RESPONSE
        {        
            CommandResponse commandResponse = mapper.readValue(this.payload, CommandResponse.class);
            MonitoringResponse monitoringResponse = mapper.readValue(this.payload, MonitoringResponse.class);
            boolean isResourcesCommand = !commandResponse.getAdditionalProperties().isEmpty();
            if(commandResponse != null && !isResourcesCommand)
            {
                this.responseType = (Class<T>) GenericTypeResolver.resolveTypeArgument(getClass(), commandResponse.getClass());
                this.responseType = (Class<T>) commandResponse.getClass();
                this.object = (T)commandResponse;                
                String[] split = this.topic.split("/");
                String gatewayIdentity = split[split.length - 2]; // use this as hashkey in cache                
                this.hashKey = commandResponse.getType(); 
                this.keyOfHash = gatewayIdentity;
                // define handler of command type
                CommandHandlerItem<CommandResponse> commandHandlerItem = new CommandHandlerItem(keyOfHash, hashKey, object);                
                String type = commandResponse.getType();
                switch(type)
                {
                    case GatewayCommand.PING:                         
                        this.gatewayCommandHandler = new PingCommandHandlerImpl(commandHandlerItem, aalHouseService, cacheService);
                        break; 
                    case GatewayCommand.BATTERY_INDICATION:
                        this.gatewayCommandHandler = new BatteryIndicationCommandHandlerImpl(commandHandlerItem, aalHouseService, cacheService);
                        break;  
                    case GatewayCommand.APPLICATION:
                        this.gatewayCommandHandler = new ApplicationCommandHandlerImpl(commandHandlerItem, aalHouseService, cacheService);
                        break;
                    case GatewayCommand.DEVICE:
                        this.gatewayCommandHandler = new DeviceCommandHandlerImpl(commandHandlerItem, aalHouseService, cacheService);      
                        break;
                }
            } 
            else if(monitoringResponse != null && isResourcesCommand) // this includes OPERATING_SYSTEM command too
            {
                this.responseType = (Class<T>) GenericTypeResolver.resolveTypeArgument(getClass(), monitoringResponse.getClass());
                this.responseType = (Class<T>) monitoringResponse.getClass();
                this.object = (T)monitoringResponse;                
                String[] split = this.topic.split("/");
                String gatewayIdentity = split[split.length - 1]; // use this as hashkey in cache
                this.keyOfHash = gatewayIdentity; 
                this.hashKey = ApiMqtt.RESOURCES_TYPE;
                CommandHandlerItem<MonitoringResponse> commandHandlerItemres = new CommandHandlerItem(keyOfHash, hashKey, object);  
                this.gatewayCommandHandler = new ResourcesCommandHandlerImpl(commandHandlerItemres, cacheService);              
            }
                
        }
        else if(this.topic.startsWith(ApiMqtt.MONITORING_REQUEST_TOPIC_PREFIX))
        {
            MonitoringResponse monitoringResponse = mapper.readValue(this.payload, MonitoringResponse.class); 
            if(monitoringResponse != null)
            {
                this.responseType = (Class<T>) GenericTypeResolver.resolveTypeArgument(getClass(), monitoringResponse.getClass());
                this.responseType = (Class<T>) monitoringResponse.getClass();
                this.object = (T)monitoringResponse;                
                String[] split = this.topic.split("/");
                String gatewayIdentity = split[split.length - 1]; // use this as hashkey in cache
                this.keyOfHash = gatewayIdentity; 
                this.hashKey = ApiMqtt.RESOURCES_TYPE; 
                CommandHandlerItem<MonitoringResponse> commandHandlerItemres = new CommandHandlerItem(keyOfHash, hashKey, object);  
                this.gatewayCommandHandler = new ResourcesCommandHandlerImpl(commandHandlerItemres, cacheService);                
            }            
        }      

        if(this.object == null)
        {
            //throw custom exception here
            //ApiError.INVALID_GATEWAY_RESPONSE_FORMAT);
        }
    }   

    public String getKeyOfHash() {
        return keyOfHash;
    }

    public String getHashKey() {
        return hashKey;
    }

    public T getObject() {
        return object;
    }  

    public Class<T> getResponseType() {
        return responseType;
    }

    protected void setResponseType(Class<T> responseType) {
        this.responseType = responseType;
    }    

    
    //@Override
    public void resolve() {
        this.gatewayCommandHandler.handle();
    }
    
    
    @Override
    public void handleCommand() {
        if(this.gatewayCommandHandler != null)
        {
            this.gatewayCommandHandler.handle();
        }
    }
    
    
}
