package org.atlas.modules.mqtt.handlers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Level;
import org.atlas.cache.CacheService;
import org.atlas.modules.mqtt.model.MonitoringResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handling class for OPERATING_SYSTEM command upon receipt of MQTT message.
 * @author ic
 */
public class OperatingSystemCommandHandlerImpl extends GatewayCommandHandlerImpl<MonitoringResponse>
{
    private final CommandHandlerItem<MonitoringResponse> commandHandlerItem;    
    private final CacheService cacheService;
    private static final Logger logger = LoggerFactory.getLogger(OperatingSystemCommandHandlerImpl.class);
    private final ObjectMapper jsonMapper = new ObjectMapper();
    private String jsonObject = "";

    /**
     * Initializes a new instance of {@link org.atlas.modules.mqtt.handlers.OperatingSystemCommandHandlerImpl} class 
     * with given handler item, and caching service instance.
     * @param commandHandlerItem The command item for the handler
     * @param cacheService The instance of cache service component
     */    
    public OperatingSystemCommandHandlerImpl(CommandHandlerItem<MonitoringResponse> commandHandlerItem, CacheService cacheService) {
        this.commandHandlerItem = commandHandlerItem;
        this.cacheService = cacheService;
    }
    
    /**
     * Caches the handler item in the underlying caching system.
     * @throws JsonProcessingException Throw if json parsing fails
     */ 
    protected void cache() throws JsonProcessingException
    {        
        String keyName = commandHandlerItem.getKeyOfHash();
        Object object = commandHandlerItem.getObject();
        String hashKey = commandHandlerItem.getHashKey();
        // cache message to redis                   

        object = jsonMapper.writeValueAsString(object); // convert object to json string
        boolean hasKey = cacheService.hasKey(keyName);        
        if(hasKey)
        {
            if(cacheService.hExistsHashKey(keyName, hashKey))
            {
                cacheService.hAdd(keyName, hashKey, object, 60);
                logger.info("Updating command response to redis with key name: " + keyName +" and hash key -> "
                        +hashKey + ", response: "+jsonMapper.writeValueAsString(object));            
            }
            else
            {
                cacheService.hPutIfAbsent(keyName, hashKey, object, 60);
                logger.info("Storing command response to redis with new key name: " + keyName +" and hash key -> "
                        +hashKey + ", response: "+jsonMapper.writeValueAsString(object));                
            }
        }
        else
        {
            cacheService.hPutIfAbsent(keyName, hashKey, object, 60);
            logger.info("Storing command response to redis with new key name: " + keyName +" and hash key -> "
                    +hashKey + ", response: "+jsonMapper.writeValueAsString(object));
        }
    }

    /**
     * Handles the item for this command type.
     */      
    @Override
    public void handle() {
        try {
            cache();
        } catch (JsonProcessingException ex) {
            java.util.logging.Logger.getLogger(OperatingSystemCommandHandlerImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
