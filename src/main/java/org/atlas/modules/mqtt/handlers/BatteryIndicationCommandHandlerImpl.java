package org.atlas.modules.mqtt.handlers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.atlas.api.services.AALHouseService;
import org.atlas.cache.CacheService;
import org.atlas.modules.aalhouse.domain.Sensor;
import org.atlas.modules.mqtt.GatewayCommand;
import org.atlas.modules.mqtt.model.CommandResponse;
import org.atlas.modules.mqtt.model.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handling class for BATTERY_INDICATION command upon receipt of MQTT message.
 * @author ic
 */
public class BatteryIndicationCommandHandlerImpl extends GatewayCommandHandlerImpl<CommandResponse>
{
    private final CommandHandlerItem<CommandResponse> commandHandlerItem;
    private final AALHouseService aalHouseService;             
    private final CacheService cacheService;
    private final ObjectMapper jsonMapper = new ObjectMapper();
    private String jsonObject = "";
    private static final Logger logger = LoggerFactory.getLogger(BatteryIndicationCommandHandlerImpl.class);    

    /**
     * Initializes a new instance of
     * {@link org.atlas.modules.mqtt.handlers.BatteryIndicationCommandHandlerImpl}
     * class with given handler item, base repository service, and caching
     * service instances.
     *
     * @param commandHandlerItem The command item for the handler
     * @param aalHouseService The instance of the Aalhouse service component 
     * @param cacheService The instance of cache service component
     */ 
    public BatteryIndicationCommandHandlerImpl(final CommandHandlerItem<CommandResponse> commandHandlerItem, final AALHouseService aalHouseService, CacheService cacheService) {
        this.commandHandlerItem = commandHandlerItem;
        this.aalHouseService = aalHouseService;
        this.cacheService = cacheService;
    }    
    
    /**
     * Handles the item for this command type.
     */        
    @Override
    public void handle() {
        CommandResponse response = commandHandlerItem.getObject();                
        response.getResults().forEach((item)->
        {
            Result result = (Result)item;
            if(result.getStatus().equals(GatewayCommand.SUCCESSFUL) && result.getConnected() == 1) // update only successful responses
            {
                // Update battery indication and last update
                String identity = result.getIdentity();
                Sensor sensor = this.aalHouseService.getSensor(identity);
                sensor.setUpdatedAt(new java.util.Date());
                sensor.setBattery(result.getBattery().intValue());
                this.aalHouseService.updateSensor(sensor.getId(), sensor);
            }
        }); 
    }
    
    /**
     * Caches the handler item in the underlying caching system.
     * @throws JsonProcessingException Thrown if json processing fails
     */     
    protected void cache() throws JsonProcessingException
    {        
        String keyName = commandHandlerItem.getKeyOfHash();
        Object object = commandHandlerItem.getObject();
        String hashKey = commandHandlerItem.getHashKey();
        // cache message to redis                   

        object = jsonMapper.writeValueAsString(object); // convert object to json string
        boolean hasKey = cacheService.hasKey(keyName);        
        if(hasKey)
        {
            if(cacheService.hExistsHashKey(keyName, hashKey))
            {
                cacheService.hAdd(keyName, hashKey, object, 60);
                logger.info("Updating command response to redis with key name: " + keyName +" and hash key -> "
                        +hashKey + ", response: "+jsonMapper.writeValueAsString(object));            
            }
            else
            {
                cacheService.hPutIfAbsent(keyName, hashKey, object, 60);
                logger.info("Storing command response to redis with new key name: " + keyName +" and hash key -> "
                        +hashKey + ", response: "+jsonMapper.writeValueAsString(object));                
            }
        }
        else
        {
            cacheService.hPutIfAbsent(keyName, hashKey, object, 60);
            logger.info("Storing command response to redis with new key name: " + keyName +" and hash key -> "
                    +hashKey + ", response: "+jsonMapper.writeValueAsString(object));
        }
    }     
    
}
