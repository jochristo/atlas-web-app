package org.atlas.modules.mqtt.handlers;

import org.springframework.core.GenericTypeResolver;

/**
 * Represents a cache-storable command handler item.
 * Contains:
 * the keyOfHash: e.g. key name for this hash i.e. the gateway identity of the command
 * the hashKey: e.g. the hash key i.e. type  of the command, for instance PING
 * the object: e.g. the object stored in cache i.e. CommandResponse or MonitoringResponse instance
 * @author ic
 * @param <T> The type of command object
 */
public class CommandHandlerItem<T extends Object>
{    
    private String keyOfHash;
    private String hashKey;
    private T object;
    private Class<T> responseType;

    /**
     * Initializes a new instance of {@link org.atlas.modules.mqtt.handlers.CommandHandlerItem} class 
     * with given attributes: the key name for this hash, the hash key, and the storable object.
     * @param keyOfHash The key name for this storable hash
     * @param hashKey The hash key of the storable item
     * @param object The storable item
     */
    public CommandHandlerItem(String keyOfHash, String hashKey, T object) {
        this.keyOfHash = keyOfHash;
        this.hashKey = hashKey;
        this.object = object;
        this.responseType = (Class<T>) GenericTypeResolver.resolveTypeArgument(getClass(), this.object.getClass());
    }    
        
    public String getKeyOfHash() {
        return keyOfHash;
    }

    public void setKeyOfHash(String keyOfHash) {
        this.keyOfHash = keyOfHash;
    }

    public String getHashKey() {
        return hashKey;
    }

    public void setHashKey(String hashKey) {
        this.hashKey = hashKey;
    }

    public T getObject() {
        return object;
    }

    public void setObject(T object) {
        this.object = object;
    }

    public Class<T> getResponseType() {
        return responseType;
    }

    public void setResponseType(Class<T> responseType) {
        this.responseType = responseType;
    }

    
}
