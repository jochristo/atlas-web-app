/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.modules.mqtt.handlers;

/**
 * Abstract base class for gateway command handler classes.
 * @author ic
 * @param <T> The type of the item to handle.
 */
public abstract class GatewayCommandHandlerImpl<T extends Object> implements IGatewayCommandHandler
{

    @Override
    public abstract void handle();
    
}
