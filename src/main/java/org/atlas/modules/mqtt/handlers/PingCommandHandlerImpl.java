package org.atlas.modules.mqtt.handlers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Level;
import org.atlas.api.services.AALHouseService;
import org.atlas.cache.CacheService;
import org.atlas.modules.aalhouse.domain.Sensor;
import org.atlas.modules.mqtt.GatewayCommand;
import org.atlas.modules.mqtt.model.CommandResponse;
import org.atlas.modules.mqtt.model.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handling class for PING command upon receipt of MQTT message.
 * @author ic
 */
public class PingCommandHandlerImpl extends GatewayCommandHandlerImpl<CommandResponse>
{
    private CommandHandlerItem<CommandResponse> commandHandlerItem;
    private AALHouseService aalHouseService; 
    private CacheService cacheService;
    private static final Logger logger = LoggerFactory.getLogger(PingCommandHandlerImpl.class);
    private final ObjectMapper jsonMapper = new ObjectMapper();

    /**
     * Initializes a new instance of {@link org.atlas.modules.mqtt.handlers.PingCommandHandlerImpl} class 
     * with given handler item, and caching service instance.
     * @param commandHandlerItem The command item for the handler
     * @param aalHouseService The instance of the Aalhouse service component 
     * @param cacheService The instance of cache service component
     */      
    public PingCommandHandlerImpl(CommandHandlerItem<CommandResponse> commandHandlerItem, final AALHouseService aalHouseService, final CacheService cacheService) {
        this.commandHandlerItem = commandHandlerItem;
        this.aalHouseService = aalHouseService;
        this.cacheService = cacheService;
    }        
        
    protected void updateSensor()
    {   
        String keyName = commandHandlerItem.getKeyOfHash();
        Object object = commandHandlerItem.getObject();
        CommandResponse response = (CommandResponse) object;         
        if(response.getUuid().isEmpty()) //test
        {
            logger.info("Pinged gateway: " + keyName + ", Result: NO RESPONSE");            
        }
        
        response.getResults().forEach((item)->
        {
            Result result = (Result)item;            
            if(result.getStatus().equals(GatewayCommand.SUCCESSFUL) && result.getConnected() == 1) // update only successful responses
            {
                // update last ping date
                String identity = result.getIdentity();
                Sensor sensor = this.aalHouseService.getSensor(identity);                                                    
                sensor.setLastPing(new java.util.Date());                                
                aalHouseService.updateSensor(sensor.getId(), sensor);                
                logger.info("Pinged sensor: "+ sensor.getName() + ", MAC address: " + sensor.getAddress() + ", Result: OK");                 
            }
            else
            {
                logger.info("Pinged gateway: " + keyName + ", Result: No response from sensor"); 
            }

        });        
    }

    @Override
    public void handle() {
        
        // store to cache for UI client requiring access
        try {
            cache();
        } catch (JsonProcessingException ex) {
            java.util.logging.Logger.getLogger(ApplicationCommandHandlerImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Update sensor and last ping field
        this.updateSensor();
    }
    
    protected void cache() throws JsonProcessingException
    {        
        String keyName = commandHandlerItem.getKeyOfHash();
        Object object = commandHandlerItem.getObject();
        String hashKey = commandHandlerItem.getHashKey();
        // cache message to redis                   

        object = jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object); // convert object to json string
        boolean hasKey = cacheService.hasKey(keyName);        
        if(hasKey)
        {
            if(cacheService.hExistsHashKey(keyName, hashKey))
            {
                cacheService.hAdd(keyName, hashKey, object, 60);
                logger.info("Updating command response to redis with key name: " + keyName +" and hash key -> "
                        +hashKey + ", response: "+jsonMapper.writeValueAsString(object));            
            }
            else
            {
                cacheService.hPutIfAbsent(keyName, hashKey, object, 60);
                logger.info("Storing command response to redis with new key name: " + keyName +" and hash key -> "
                        +hashKey + ", response: "+jsonMapper.writeValueAsString(object));                
            }
        }
        else
        {
            cacheService.hPutIfAbsent(keyName, hashKey, object, 60);
            logger.info("Storing command response to redis with new key name: " + keyName +" and hash key -> "
                    +hashKey + ", response: "+jsonMapper.writeValueAsString(object));
        }
    }     
    
}
