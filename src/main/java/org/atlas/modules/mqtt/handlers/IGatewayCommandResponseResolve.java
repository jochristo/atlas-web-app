/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.modules.mqtt.handlers;

import java.io.IOException;

/**
 * Resolves gateway command type and handles the command.
 * @author ic
 */
public interface IGatewayCommandResponseResolve
{
    public void resolveCommandType(byte[] payload, String topic) throws IOException;
    public void handleCommand();
}
