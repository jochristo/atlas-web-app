/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.modules.mqtt;

/**
 * Static resources for gateway commands.
 * @author ic
 */
public class GatewayCommand
{
    //TYPE
    public static final String PING = "PING";
    public static final String RESOURCES = "RESOURCES";
    public static final String BATTERY_INDICATION = "BATTERY_INDICATION";
    public static final String DEVICE = "DEVICE";
    public static final String ADVERTISEMENT = "ADVERTISEMENT";
    public static final String OPERATING_SYSTEM = "OPERATING_SYSTEM";
    public static final String APPLICATION = "APPLICATION";
    public static final String WIRELESS = "WIRELESS";
    public static final String MONITORING = "MONITORING";
    
    // STATUS
    public static final String SUCCESSFUL = "SUCCESSFUL";
    public static final String EXECUTED = "EXECUTED";
    public static final String TIMEOUT = "TIMEOUT";
    public static final String ERROR = "ERROR";
    
    //MAIN OPERATIONS: DEVICE/OPERATING_SYSTEM/APPLICATION commands
    public static final String CHECK = "CHECK";
    public static final String CONNECT = "CONNECT";
    public static final String CONFIGURE = "CONFIGURE";
    public static final String DISCONNECT = "DISCONNECT";
    public static final String LIST = "LIST";
    public static final String REBOOT = "REBOOT";    
    public static final String RESTART = "RESTART";
    public static final String START = "START";
    public static final String SHUTDOWN = "SHUTDOWN";
    
            
    public enum Type
    {
        PING, RESOURCES, BATTERY_INDICATION, DEVICE, ADVERTISEMENT, OPERATING_SYSTEM, APPLICATION, WIRELESS
    }
    
    public static String translateCommandType(String commandType)
    {
        String commandName = commandType;
        switch(commandType){
            case "battery" :
                commandName =  "BATTERY_INDICATION";
                break;
            case "os" :
                commandName =  "OPERATING_SYSTEM";
                break;                
        }
        return commandName;
    }
    
}
