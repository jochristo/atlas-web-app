
package org.atlas.modules.mqtt.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;

/**
 * Represents a gateway command response object.
 * @author ic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "uuid",
    "type",
    "operation",
    "results"
})
public class CommandResponse implements  Serializable
{  
    @JAPIIdentifier
    @JsonIgnore    
    private String id;     

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }    

    public CommandResponse() {
        //this.id = UUID.randomUUID().toString();
    }        
    
    @JsonProperty("uuid")
    @JAPIAttribute
    private String uuid;
    
    @JsonProperty("type")
    @JAPIAttribute
    private String type;
    
    @JsonProperty("operation")
    @JAPIAttribute
    private String operation;    
    
    @JsonProperty("results")
    private List<Result> results = null;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("uuid")
    public String getUuid() {
        return uuid;
    }

    @JsonProperty("uuid")
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }
        
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("operation")
    public String getOperation() {
        return operation;
    }

    @JsonProperty("operation")
    public void setOperation(String operation) {
        this.operation = operation;
    }    
    
    @JsonProperty("results")
    public List<Result> getResults() {
        return results;
    }

    @JsonProperty("results")
    public void setResults(List<Result> results) {
        this.results = results;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
