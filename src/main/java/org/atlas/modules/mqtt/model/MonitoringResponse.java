
package org.atlas.modules.mqtt.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.Serializable;
import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;

/**
 * Represents a gateway monitoring response object after a monitoring information response is received.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "system_info",
    "system_partitions",
    "system_memories"
})
public class MonitoringResponse implements Serializable
{
    @JAPIIdentifier
    @JsonIgnore    
    private String id;     

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public MonitoringResponse() {
        //this.id = UUID.randomUUID().toString();
    }
    
    @JsonCreator
    public static MonitoringResponse Create(String jsonString) throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        MonitoringResponse module = null;
        module = mapper.readValue(jsonString, MonitoringResponse.class);
        return module;
    }    
    
    @JsonProperty("system_info")
    @JAPIAttribute
    private SystemInfo systemInfo;
    
    @JsonProperty("system_partitions")    
    private List<SystemPartition> systemPartitions = null;
    
    @JsonProperty("system_memories")    
    private List<SystemMemory> systemMemories = null;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("system_info")    
    public SystemInfo getSystemInfo() {
        return systemInfo;
    }

    @JsonProperty("system_info")
    public void setSystemInfo(SystemInfo systemInfo) {
        this.systemInfo = systemInfo;
    }

    @JsonProperty("system_partitions")
    public List<SystemPartition> getSystemPartitions() {
        return systemPartitions;
    }

    @JsonProperty("system_partitions")
    public void setSystemPartitions(List<SystemPartition> systemPartitions) {
        this.systemPartitions = systemPartitions;
    }

    @JsonProperty("system_memories")
    public List<SystemMemory> getSystemMemories() {
        return systemMemories;
    }

    @JsonProperty("system_memories")
    public void setSystemMemories(List<SystemMemory> systemMemories) {
        this.systemMemories = systemMemories;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
