
package org.atlas.modules.mqtt.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.UUID;
import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "user_space",
    "kernel_space",
    "low_priority_processes",
    "idle_operations",
    "io_peripherals_waiting",
    "hardware_interrupts_routines",
    "software_interrupts_routines",
    "virtual_cpu"
})
public class CpusInfo implements Serializable{

    @JAPIIdentifier
    @JsonIgnore    
    private String id;     

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }      
    
    public CpusInfo() {
        this.id = UUID.randomUUID().toString();
    }    
    
    @JsonProperty("user_space")
    @JAPIAttribute
    private Integer userSpace;
    @JsonProperty("kernel_space")
    @JAPIAttribute
    private Double kernelSpace;
    @JsonProperty("low_priority_processes")
    @JAPIAttribute
    private Integer lowPriorityProcesses;
    @JsonProperty("idle_operations")
    @JAPIAttribute
    private Double idleOperations;
    @JsonProperty("io_peripherals_waiting")
    @JAPIAttribute
    private Double ioPeripheralsWaiting;
    @JsonProperty("hardware_interrupts_routines")
    @JAPIAttribute
    private Integer hardwareInterruptsRoutines;
    @JsonProperty("software_interrupts_routines")
    @JAPIAttribute
    private Integer softwareInterruptsRoutines;
    @JsonProperty("virtual_cpu")
    @JAPIAttribute
    private Integer virtualCpu;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("user_space")
    public Integer getUserSpace() {
        return userSpace;
    }

    @JsonProperty("user_space")
    public void setUserSpace(Integer userSpace) {
        this.userSpace = userSpace;
    }

    @JsonProperty("kernel_space")
    public Double getKernelSpace() {
        return kernelSpace;
    }

    @JsonProperty("kernel_space")
    public void setKernelSpace(Double kernelSpace) {
        this.kernelSpace = kernelSpace;
    }

    @JsonProperty("low_priority_processes")
    public Integer getLowPriorityProcesses() {
        return lowPriorityProcesses;
    }

    @JsonProperty("low_priority_processes")
    public void setLowPriorityProcesses(Integer lowPriorityProcesses) {
        this.lowPriorityProcesses = lowPriorityProcesses;
    }

    @JsonProperty("idle_operations")
    public Double getIdleOperations() {
        return idleOperations;
    }

    @JsonProperty("idle_operations")
    public void setIdleOperations(Double idleOperations) {
        this.idleOperations = idleOperations;
    }

    @JsonProperty("io_peripherals_waiting")
    public Double getIoPeripheralsWaiting() {
        return ioPeripheralsWaiting;
    }

    @JsonProperty("io_peripherals_waiting")
    public void setIoPeripheralsWaiting(Double ioPeripheralsWaiting) {
        this.ioPeripheralsWaiting = ioPeripheralsWaiting;
    }

    @JsonProperty("hardware_interrupts_routines")
    public Integer getHardwareInterruptsRoutines() {
        return hardwareInterruptsRoutines;
    }

    @JsonProperty("hardware_interrupts_routines")
    public void setHardwareInterruptsRoutines(Integer hardwareInterruptsRoutines) {
        this.hardwareInterruptsRoutines = hardwareInterruptsRoutines;
    }

    @JsonProperty("software_interrupts_routines")
    public Integer getSoftwareInterruptsRoutines() {
        return softwareInterruptsRoutines;
    }

    @JsonProperty("software_interrupts_routines")
    public void setSoftwareInterruptsRoutines(Integer softwareInterruptsRoutines) {
        this.softwareInterruptsRoutines = softwareInterruptsRoutines;
    }

    @JsonProperty("virtual_cpu")
    public Integer getVirtualCpu() {
        return virtualCpu;
    }

    @JsonProperty("virtual_cpu")
    public void setVirtualCpu(Integer virtualCpu) {
        this.virtualCpu = virtualCpu;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
