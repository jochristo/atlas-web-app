
package org.atlas.modules.mqtt.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.UUID;
import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "total",
    "running",
    "sleeping",
    "stopped",
    "zombie"
})
public class TasksInfo implements Serializable{

    @JAPIIdentifier
    @JsonIgnore    
    private String id;     

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }    
    
    public TasksInfo() {
        this.id = UUID.randomUUID().toString();
    }    
    
    @JsonProperty("total")
    @JAPIAttribute
    private Integer total;
    @JsonProperty("running")
    @JAPIAttribute
    private Integer running;
    @JsonProperty("sleeping")
    @JAPIAttribute
    private Integer sleeping;
    @JsonProperty("stopped")
    @JAPIAttribute
    private Integer stopped;
    @JsonProperty("zombie")
    @JAPIAttribute
    private Integer zombie;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("total")
    public Integer getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Integer total) {
        this.total = total;
    }

    @JsonProperty("running")
    public Integer getRunning() {
        return running;
    }

    @JsonProperty("running")
    public void setRunning(Integer running) {
        this.running = running;
    }

    @JsonProperty("sleeping")
    public Integer getSleeping() {
        return sleeping;
    }

    @JsonProperty("sleeping")
    public void setSleeping(Integer sleeping) {
        this.sleeping = sleeping;
    }

    @JsonProperty("stopped")
    public Integer getStopped() {
        return stopped;
    }

    @JsonProperty("stopped")
    public void setStopped(Integer stopped) {
        this.stopped = stopped;
    }

    @JsonProperty("zombie")
    public Integer getZombie() {
        return zombie;
    }

    @JsonProperty("zombie")
    public void setZombie(Integer zombie) {
        this.zombie = zombie;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
