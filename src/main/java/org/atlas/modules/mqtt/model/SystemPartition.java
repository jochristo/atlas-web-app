
package org.atlas.modules.mqtt.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.UUID;
import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "identity",
    "total_space",
    "free_space",
    "usable_space"
})
public class SystemPartition implements Serializable
{
    @JAPIIdentifier
    @JsonIgnore    
    private String id;     

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }     

    public SystemPartition() {
        this.id = UUID.randomUUID().toString();
    }    
    
    @JsonProperty("identity")
    @JAPIAttribute
    private String identity;
    @JsonProperty("total_space")
    @JAPIAttribute
    private BigInteger totalSpace;
    @JsonProperty("free_space")
    @JAPIAttribute
    private BigInteger freeSpace;
    @JsonProperty("usable_space")
    @JAPIAttribute
    private BigInteger usableSpace;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("identity")
    public String getIdentity() {
        return identity;
    }

    @JsonProperty("identity")
    public void setIdentity(String identity) {
        this.identity = identity;
    }

    @JsonProperty("total_space")
    public BigInteger getTotalSpace() {
        return totalSpace;
    }

    @JsonProperty("total_space")
    public void setTotalSpace(BigInteger totalSpace) {
        this.totalSpace = totalSpace;
    }

    @JsonProperty("free_space")
    public BigInteger getFreeSpace() {
        return freeSpace;
    }

    @JsonProperty("free_space")
    public void setFreeSpace(BigInteger freeSpace) {
        this.freeSpace = freeSpace;
    }

    @JsonProperty("usable_space")
    public BigInteger getUsableSpace() {
        return usableSpace;
    }

    @JsonProperty("usable_space")
    public void setUsableSpace(BigInteger usableSpace) {
        this.usableSpace = usableSpace;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
