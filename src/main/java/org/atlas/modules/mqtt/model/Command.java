package org.atlas.modules.mqtt.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;

/**
 * Represents a command item sent to a gateway device.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "uuid",
    "type",
    "identities",
    "operation",
    " timeout",
    "advertisement"
})
public class Command {

    @JsonProperty("uuid")
    @JAPIIdentifier
    private String uuid;
    
    @JsonProperty("type")
    @JAPIAttribute
    private String type;
    
    @JsonProperty("identities")
    @JAPIAttribute
    private List<String> identities = null;
    
    @JsonProperty("operation")
    @JAPIAttribute
    private String operation;
    
    @JsonProperty("timeout")
    @JAPIAttribute
    private Integer timeout;
    
    @JsonProperty("advertisement")
    @JAPIAttribute
    private Advertisement advertisement;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("uuid")
    public String getUuid() {
        return uuid;
    }

    @JsonProperty("uuid")
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("identities")
    public List<String> getIdentities() {
        return identities;
    }

    @JsonProperty("identities")
    public void setIdentities(List<String> identities) {
        this.identities = identities;
    }

    @JsonProperty("operation")
    public String getOperation() {
        return operation;
    }

    @JsonProperty("operation")
    public void setOperation(String operation) {
        this.operation = operation;
    }

    @JsonProperty("timeout")
    public Integer getTimeout() {
        return timeout;
    }

    @JsonProperty("timeout")
    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    @JsonProperty("advertisement")
    public Advertisement getAdvertisement() {
        return advertisement;
    }

    @JsonProperty("advertisement")
    public void setAdvertisement(Advertisement advertisement) {
        this.advertisement = advertisement;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
