
package org.atlas.modules.mqtt.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.UUID;
import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "last_minute_load_average",
    "last_five_minutes_load_average",
    "last_fifteen_minutes_load_average"
})
public class SystemLoad implements Serializable{

    @JAPIIdentifier
    @JsonIgnore    
    private String id;     

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }   
    
    public SystemLoad() {
        this.id = UUID.randomUUID().toString();
    }    
    
    @JsonProperty("last_minute_load_average")
    @JAPIAttribute
    private Double lastMinuteLoadAverage;
    @JsonProperty("last_five_minutes_load_average")
    @JAPIAttribute
    private Double lastFiveMinutesLoadAverage;
    @JsonProperty("last_fifteen_minutes_load_average")
    @JAPIAttribute
    private Double lastFifteenMinutesLoadAverage;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("last_minute_load_average")
    public Double getLastMinuteLoadAverage() {
        return lastMinuteLoadAverage;
    }

    @JsonProperty("last_minute_load_average")
    public void setLastMinuteLoadAverage(Double lastMinuteLoadAverage) {
        this.lastMinuteLoadAverage = lastMinuteLoadAverage;
    }

    @JsonProperty("last_five_minutes_load_average")
    public Double getLastFiveMinutesLoadAverage() {
        return lastFiveMinutesLoadAverage;
    }

    @JsonProperty("last_five_minutes_load_average")
    public void setLastFiveMinutesLoadAverage(Double lastFiveMinutesLoadAverage) {
        this.lastFiveMinutesLoadAverage = lastFiveMinutesLoadAverage;
    }

    @JsonProperty("last_fifteen_minutes_load_average")
    public Double getLastFifteenMinutesLoadAverage() {
        return lastFifteenMinutesLoadAverage;
    }

    @JsonProperty("last_fifteen_minutes_load_average")
    public void setLastFifteenMinutesLoadAverage(Double lastFifteenMinutesLoadAverage) {
        this.lastFifteenMinutesLoadAverage = lastFifteenMinutesLoadAverage;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
