
package org.atlas.modules.mqtt.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "data",
    "interval",
    "repeats",
    "start_at",
    "stop_at"
})
public class Advertisement {

    @JsonProperty("data")
    private String data;
    
    @JsonProperty("interval")
    private Integer interval;
    
    @JsonProperty("repeats")
    private Integer repeats;
    
    @JsonProperty("start_at")
    private String startAt;
    
    @JsonProperty("stop_at")
    private String stopAt;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("data")
    public String getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(String data) {
        this.data = data;
    }

    @JsonProperty("interval")
    public Integer getInterval() {
        return interval;
    }

    @JsonProperty("interval")
    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    @JsonProperty("repeats")
    public Integer getRepeats() {
        return repeats;
    }

    @JsonProperty("repeats")
    public void setRepeats(Integer repeats) {
        this.repeats = repeats;
    }

    @JsonProperty("start_at")
    public String getStartAt() {
        return startAt;
    }

    @JsonProperty("start_at")
    public void setStartAt(String startAt) {
        this.startAt = startAt;
    }

    @JsonProperty("stop_at")
    public String getStopAt() {
        return stopAt;
    }

    @JsonProperty("stop_at")
    public void setStopAt(String stopAt) {
        this.stopAt = stopAt;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
