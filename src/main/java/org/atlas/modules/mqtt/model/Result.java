
package org.atlas.modules.mqtt.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.UUID;
import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;

/**
 * Represents a result attribute of a command or monitoring response object.
 * @author ic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "identity",
    "signal",
    "battery",
    "connected",
    "pid",
    "ip",
    "status",
    "error"
})
public class Result implements Serializable{

    @JAPIIdentifier
    @JsonIgnore    
    private String id;     

    public String getId() {        
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }   

    public Result() {
        this.id = UUID.randomUUID().toString();
    }        
    
    @JsonProperty("identity")
    @JAPIAttribute
    private String identity;
    @JsonProperty("signal")
    @JAPIAttribute
    private Integer signal;
    @JsonProperty("battery")
    @JAPIAttribute
    private Integer battery;
    @JsonProperty("connected")
    @JAPIAttribute
    private Integer connected;   
    @JsonProperty("ip")
    @JAPIAttribute
    private String ip;        
    @JsonProperty("pid")
    @JAPIAttribute
    private Integer pid;
    @JsonProperty("status")
    @JAPIAttribute
    private String status;
    @JsonProperty("error")
    @JAPIAttribute
    private String error;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("identity")
    public String getIdentity() {
        return identity;
    }

    @JsonProperty("identity")
    public void setIdentity(String identity) {
        this.identity = identity;
    }

    @JsonProperty("signal")
    public Integer getSignal() {
        return signal;
    }

    @JsonProperty("signal")
    public void setSignal(Integer signal) {
        this.signal = signal;
    }

    @JsonProperty("battery")
    public Integer getBattery() {
        return battery;
    }

    @JsonProperty("battery")
    public void setBattery(Integer battery) {
        this.battery = battery;
    }

    @JsonProperty("connected")
    public Integer getConnected() {
        return connected;
    }

    @JsonProperty("connected")
    public void setConnected(Integer connected) {
        this.connected = connected;
    }

    @JsonProperty("pid")
    public Integer getPid() {
        return pid;
    }

    @JsonProperty("pid")
    public void setPid(Integer pid) {
        this.pid = pid;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("error")
    public String getError() {
        return error;
    }

    @JsonProperty("error")
    public void setError(String error) {
        this.error = error;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonProperty("ip")    
    public String getIp() {
        return ip;
    }

    @JsonProperty("ip")    
    public void setIp(String ip) {
        this.ip = ip;
    }
    
    

}
