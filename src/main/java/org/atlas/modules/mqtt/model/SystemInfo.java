
package org.atlas.modules.mqtt.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.UUID;
import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "cores",
    "os_name",
    "os_version",
    "os_arch",
    "local_time",
    "up_time",
    "system_load",
    "tasks_info",
    "cpus_info",
    "ip_addresses",
    "public_ip"
})
public class SystemInfo implements Serializable{

    @JAPIIdentifier
    @JsonIgnore    
    private String id;     

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }      
    
    public SystemInfo() {
        this.id = UUID.randomUUID().toString();
    }    
    
    @JsonProperty("cores")
    @JAPIAttribute
    private Integer cores;
    @JsonProperty("os_name")
    @JAPIAttribute
    private String osName;
    @JsonProperty("os_version")
    @JAPIAttribute
    private String osVersion;
    @JsonProperty("os_arch")
    @JAPIAttribute
    private String osArch;
    @JsonProperty("local_time")
    @JAPIAttribute
    private String localTime;
    @JsonProperty("up_time")
    @JAPIAttribute
    private Integer upTime;
    @JsonProperty("system_load")
    @JAPIAttribute
    private SystemLoad systemLoad;
    @JsonProperty("tasks_info")
    @JAPIAttribute
    private TasksInfo tasksInfo;
    @JsonProperty("cpus_info")
    @JAPIAttribute
    private CpusInfo cpusInfo;
    @JsonProperty("ip_addresses")    
    private List<String> ipAddresses = null;
    @JsonProperty("public_ip")
    @JAPIAttribute
    private String publicIp;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("cores")
    public Integer getCores() {
        return cores;
    }

    @JsonProperty("cores")
    public void setCores(Integer cores) {
        this.cores = cores;
    }

    @JsonProperty("os_name")
    public String getOsName() {
        return osName;
    }

    @JsonProperty("os_name")
    public void setOsName(String osName) {
        this.osName = osName;
    }

    @JsonProperty("os_version")
    public String getOsVersion() {
        return osVersion;
    }

    @JsonProperty("os_version")
    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    @JsonProperty("os_arch")
    public String getOsArch() {
        return osArch;
    }

    @JsonProperty("os_arch")
    public void setOsArch(String osArch) {
        this.osArch = osArch;
    }

    @JsonProperty("local_time")
    public String getLocalTime() {
        return localTime;
    }

    @JsonProperty("local_time")
    public void setLocalTime(String localTime) {
        this.localTime = localTime;
    }

    @JsonProperty("up_time")
    public Integer getUpTime() {
        return upTime;
    }

    @JsonProperty("up_time")
    public void setUpTime(Integer upTime) {
        this.upTime = upTime;
    }

    @JsonProperty("system_load")
    public SystemLoad getSystemLoad() {
        return systemLoad;
    }

    @JsonProperty("system_load")
    public void setSystemLoad(SystemLoad systemLoad) {
        this.systemLoad = systemLoad;
    }

    @JsonProperty("tasks_info")
    public TasksInfo getTasksInfo() {
        return tasksInfo;
    }

    @JsonProperty("tasks_info")
    public void setTasksInfo(TasksInfo tasksInfo) {
        this.tasksInfo = tasksInfo;
    }

    @JsonProperty("cpus_info")
    public CpusInfo getCpusInfo() {
        return cpusInfo;
    }

    @JsonProperty("cpus_info")
    public void setCpusInfo(CpusInfo cpusInfo) {
        this.cpusInfo = cpusInfo;
    }

    @JsonProperty("ip_addresses")
    public List<String> getIpAddresses() {
        return ipAddresses;
    }

    @JsonProperty("ip_addresses")
    public void setIpAddresses(List<String> ipAddresses) {
        this.ipAddresses = ipAddresses;
    }

    @JsonProperty("public_ip")
    public String getPublicIp() {
        return publicIp;
    }

    @JsonProperty("public_ip")
    public void setPublicIp(String publicIp) {
        this.publicIp = publicIp;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
