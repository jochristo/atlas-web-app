
package org.atlas.modules.mqtt.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.UUID;
import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "total",
    "used",
    "free",
    "shared",
    "cached",
    "available"
})
public class SystemMemory implements Serializable
{
    @JAPIIdentifier
    @JsonIgnore    
    private String id;     

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }     

    public SystemMemory() {
        this.id = UUID.randomUUID().toString();
    }    
    
    @JsonProperty("type")
    @JAPIAttribute
    private String type;
    @JsonProperty("total")
    @JAPIAttribute
    private Integer total;
    @JsonProperty("used")
    @JAPIAttribute
    private Integer used;
    @JsonProperty("free")
    @JAPIAttribute
    private Integer free;
    @JsonProperty("shared")
    @JAPIAttribute
    private Integer shared;
    @JsonProperty("cached")
    @JAPIAttribute
    private Integer cached;
    @JsonProperty("available")
    @JAPIAttribute
    private Integer available;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("total")
    public Integer getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Integer total) {
        this.total = total;
    }

    @JsonProperty("used")
    public Integer getUsed() {
        return used;
    }

    @JsonProperty("used")
    public void setUsed(Integer used) {
        this.used = used;
    }

    @JsonProperty("free")
    public Integer getFree() {
        return free;
    }

    @JsonProperty("free")
    public void setFree(Integer free) {
        this.free = free;
    }

    @JsonProperty("shared")
    public Integer getShared() {
        return shared;
    }

    @JsonProperty("shared")
    public void setShared(Integer shared) {
        this.shared = shared;
    }

    @JsonProperty("cached")
    public Integer getCached() {
        return cached;
    }

    @JsonProperty("cached")
    public void setCached(Integer cached) {
        this.cached = cached;
    }

    @JsonProperty("available")
    public Integer getAvailable() {
        return available;
    }

    @JsonProperty("available")
    public void setAvailable(Integer available) {
        this.available = available;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
