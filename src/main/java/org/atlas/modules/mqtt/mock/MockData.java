/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.modules.mqtt.mock;

/**
 *
 * @author ic
 */
public class MockData {
    
    /**
     *
     */
    public final static String MONITORING_RESPONSE_MOCK = "{\n" +
"\"system_info\": {\n" +
"\"cores\": 4,\n" +
"\"os_name\": \"Linux\",\n" +
"\"os_version\": \"4.4.34-v7+\",\n" +
"\"os_arch\": \"arm\",\n" +
"\"local_time\": \"2017-02-15 08:24:59\",\n" +
"\"up_time\": 137880,\n" +
"\"system_load\": {\n" +
"\"last_minute_load_average\": 0.38,\n" +
"\"last_five_minutes_load_average\": 0.08,\n" +
"\"last_fifteen_minutes_load_average\": 0.03},\n" +
"\"tasks_info\": {\n" +
"\"total\": 124,\n" +
"\"running\": 1,\n" +
"\"sleeping\": 123,\n" +
"\"stopped\": 0,\n" +
"\"zombie\": 0\n" +
"},\n" +
"\"cpus_info\": {\n" +
"\"user_space\": 1,\n" +
"\"kernel_space\": 0.2,\n" +
"\"low_priority_processes\": 0,\n" +
"\"idle_operations\": 98.7,\n" +
"\"io_peripherals_waiting\": 0.1,\n" +
"\"hardware_interrupts_routines\": 0,\n" +
"\"software_interrupts_routines\": 0,\n" +
"\"virtual_cpu\": 0\n" +
"},\n" +
"\"ip_addresses\": [\"10.10.10.137\"],\n" +
"\"public_ip\": \"81.186.68.126\"\n" +
"},\n" +
"\"system_partitions\": [{\n" +
"\"identity\": \"/\",\n" +
"\"total_space\": 7722041344,\n" +
"\"free_space\": 6157643776,\n" +
"\"usable_space\": 5813436416\n" +
"}],\n" +
"\"system_memories\": [{\n" +
"\"type\": \"RAM\",\n" +
"\"total\": 947732,\n" +
"\"used\": 386324,\n" +
"\"free\": 561408,\n" +
"\"shared\": 6308,\n" +
"\"cached\": 44012,\n" +
"\"available\": 215348\n" +
"}, {\n" +
"\"type\": \"SWAP\",\n" +
"\"total\": 102396,\n" +
"\"used\": 0,\n" +
"\"free\": 102396,\n" +
"\"shared\": 0,\n" +
"\"cached\": 0,\n" +
"\"available\": 0\n" +
"}, {\n" +
"\"type\": \"VIRTUAL\",\n" +
"\"total\": 235274240,\n" +
"\"used\": 0,\n" +
"\"free\": 11118048,\n" +
"\"shared\": 0,\n" +
"\"cached\": 0,\n" +
"\"available\": 33615872\n" +
"}]\n" +
"}";
    
}
