/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.modules.mqtt;

import java.io.IOException;
import java.util.Date;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.atlas.modules.mqtt.handlers.IGatewayCommandResponseResolve;

/**
 * Service class to deliver command type resolving and command handling.
 * @author ic
 */
@Service
public class MqttMessageHandler {
    
    private String topic;
    private MqttMessage mqttMessage;    
    private Date timestamp;
    
    @Autowired    
    @Qualifier("gatewayCommandResponseResolver")    
    private IGatewayCommandResponseResolve responseResolver;      
    
    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public MqttMessage getMqttMessage() {
        return mqttMessage;
    }

    public void setMqttMessage(MqttMessage mqttMessage) {
        this.mqttMessage = mqttMessage;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }    
        
    /**
     * Resolves command type and handles the command.
     * @param payload The payload of the message.
     * @param topic The topic of  the message.
     * @throws java.io.IOException Thrown when ObjectMapper read operations fail.
     */
    public void handle(byte [] payload, String topic) throws IOException
    {
        responseResolver.resolveCommandType(payload,topic);
        responseResolver.handleCommand();
    }

    
}
