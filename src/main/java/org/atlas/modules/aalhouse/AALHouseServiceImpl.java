package org.atlas.modules.aalhouse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;

import org.atlas.api.services.AALHouseService;
import org.atlas.cache.CacheService;
import org.atlas.core.helpers.Generator;
import org.atlas.modules.aalhouse.domain.AALHouse;
import org.atlas.modules.aalhouse.domain.AALHouseStatus;
import org.atlas.modules.aalhouse.domain.Datatype;
import org.atlas.modules.aalhouse.domain.Gateway;
import org.atlas.modules.aalhouse.domain.GatewaySensor;
import org.atlas.modules.aalhouse.domain.GatewaySensorPK;
import org.atlas.modules.aalhouse.domain.GatewaySensorStatus;
import org.atlas.modules.aalhouse.domain.GatewayStatus;
import org.atlas.modules.aalhouse.domain.Manufacturer;
import org.atlas.modules.aalhouse.domain.Sensor;
import org.atlas.modules.aalhouse.domain.SensorStatus;
import org.atlas.modules.aalhouse.repositories.AALHousesRepository;
import org.atlas.modules.aalhouse.repositories.DatatypeRepository;
import org.atlas.modules.aalhouse.repositories.GatewaySensorRepository;
import org.atlas.modules.aalhouse.repositories.GatewaysRepository;
import org.atlas.modules.aalhouse.repositories.ManufacturerRepository;
import org.atlas.modules.aalhouse.repositories.SensorsRepository;
import org.atlas.modules.emulator.EmulationData;
import org.atlas.modules.integrators.ApiMqtt;
import org.atlas.modules.mqtt.model.Command;
import org.atlas.modules.integrators.IntegrationGateway;
import org.atlas.modules.mqtt.GatewayCommand;
import org.atlas.modules.mqtt.mock.MockData;
import org.atlas.modules.mqtt.model.CommandResponse;
import org.atlas.modules.mqtt.model.MonitoringResponse;
import org.atlas.modules.mqtt.model.Result;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Core ( AALHouse Service Implementation )
 * @since 2016-07-27
 * @see org.atlas.api.services.AALHouseService
 **/
@Service
public class AALHouseServiceImpl implements AALHouseService {
private static final org.slf4j.Logger logger = LoggerFactory.getLogger(AALHouseServiceImpl.class);

    @Resource
    private AALHousesRepository housesRepository;

    @Resource
    private GatewaysRepository gatewaysRepository;

    @Resource
    private SensorsRepository sensorsRepository;

    @Resource
    private DatatypeRepository datatypesRepo;

    @Resource
    private ManufacturerRepository manufacturerRepo;
    
    @Resource
    private GatewaySensorRepository gatewaySensorRepository;    

    @Autowired
    private IntegrationGateway integrationGateway;

    @Autowired
    private CacheService cacheService; // redis cache client service 

    private final ObjectMapper jsonMapper = new ObjectMapper();

    @Override
    public List<AALHouse> getHouses() {
        return housesRepository.findAll();
    }

    @Override
    public AALHouse getHouse(long houseId) {
        return housesRepository.findOne(houseId);
    }

    @Override
    public AALHouse getHouse(String name) {
        return housesRepository.findByName(name);
    }

    @Override
    public List<Gateway> getHouseGateways(long houseId) {        
        return gatewaysRepository.findByAalhouse(houseId);
    }

    @Override
    public AALHouse addHouse(AALHouse house) {
        AALHouse aalhouse = new AALHouse();
        aalhouse.setAddress(house.getAddress());
        aalhouse.setCreatedAt(new java.util.Date());
        aalhouse.setLocation("");
        aalhouse.setName(house.getName());
        aalhouse.setSquare_meters(house.getSquare_meters());
        aalhouse.setStatus(AALHouseStatus.ENABLED);
        aalhouse.setTotal_rooms(house.getTotal_rooms());
        return housesRepository.save(aalhouse);
    }

    @Override
    public AALHouse updateHouse(long houseId, AALHouse house) {
        AALHouse aalhouse = this.getHouse(houseId);
        aalhouse.setAddress(house.getAddress());
        aalhouse.setUpdatedAt(new java.util.Date());
        aalhouse.setLocation("");
        aalhouse.setName(house.getName());
        aalhouse.setSquare_meters(house.getSquare_meters());
        aalhouse.setStatus(AALHouseStatus.ENABLED);
        aalhouse.setTotal_rooms(house.getTotal_rooms());
        return housesRepository.save(aalhouse);
    }

    @Override
    public AALHouse deleteHouse(long houseId) {
        AALHouse aalhouse = this.getHouse(houseId);
        aalhouse.setStatus(AALHouseStatus.DISABLED);
        aalhouse.setUpdatedAt(new java.util.Date());
        return housesRepository.save(aalhouse);
    }

    @Override
    public Gateway removeGatewayFromHouse(long gwId, long houseId) {
        Gateway gateway = gatewaysRepository.findByIdAndAalhouse(gwId, houseId);
        gateway.setStatus(GatewayStatus.ENABLED);
        gateway.setAalhouse(0);
        gateway.setUpdatedAt(new java.util.Date());
        return gatewaysRepository.save(gateway);
    }
    
    @Override
    public Gateway attachGatewayToHouse(long gwId, long houseId) {
        Gateway gateway = gatewaysRepository.findByIdAndAalhouse(gwId, houseId);
        AALHouse house = housesRepository.findOne(houseId);
        gateway.setStatus(GatewayStatus.ENABLED);
        if(house != null){
            gateway.setAalhouse(house.getId());
        }
        gateway.setUpdatedAt(new java.util.Date());
        return gatewaysRepository.save(gateway);
    }    

    @Override
    public Gateway addGateway(Gateway gw) {
        Gateway gateway = new Gateway();
        gateway.setGateway(gw.getGateway());
        gateway.setCreatedAt(new java.util.Date());
        gateway.setStatus(GatewayStatus.ENABLED);
        gateway.setToken(Generator.generateHash());
        gateway.setAalhouse(0);//Default if 0
        gateway.setIdentity(gw.getIdentity()); // identity added                
        return gatewaysRepository.save(gateway);
    }

    @Override
    public Gateway updateGateway(long gwId, Gateway gw) {
        Gateway gateway = this.getGateway(gwId);
        gateway.setAalhouse(gateway.getAalhouse());//TODO Check.
        gateway.setGateway(gw.getGateway());
        gateway.setStatus(gateway.getStatus());
        gateway.setUpdatedAt(new java.util.Date());
        gateway.setIdentity(gw.getIdentity()); // identity added
        gateway.setGatewaySensor(gw.getGatewaySensor());
        return gatewaysRepository.save(gateway);
    }

    @Override
    public Gateway deleteGateway(long gwId) {
        Gateway gateway = this.getGateway(gwId);
        gateway.setStatus(GatewayStatus.DISABLED);
        gateway.setAalhouse(0);
        gateway.setUpdatedAt(new java.util.Date());
        return gatewaysRepository.save(gateway);
    }

    @Override
    public Gateway enableGateway(long gwId) {
        Gateway gateway = this.getGateway(gwId);
        gateway.setUpdatedAt(new java.util.Date());
        gateway.setStatus(GatewayStatus.ENABLED);
        return gatewaysRepository.save(gateway);
    }

    @Override
    public Gateway detachSensorFromGateway(long sensorId, long gwId)
    {       
        Gateway gateway = gatewaysRepository.findOne(gwId);
        Sensor sensor = sensorsRepository.findOne(sensorId); 
        GatewaySensor gatewaySensor = getGatewaySensor(gateway.getId(), sensor.getId());       
        if(gatewaySensor != null){                
            gatewaySensor.setStatus(GatewaySensorStatus.DETACHED);
            updateGatewaySensor(gwId, sensorId, gatewaySensor);
            return gatewaysRepository.findOne(gwId);
        }        
        return gateway;
    }
    
    @Override
    public Gateway attachSensorToGateway(long sensorId, long gwId)
    {
        Gateway gateway = gatewaysRepository.findOne(gwId);
        Sensor sensor = sensorsRepository.findOne(sensorId);
        GatewaySensor gatewaySensor = getGatewaySensor(gateway.getId(), sensor.getId());
        Set<GatewaySensor> gatewaySensors = new HashSet<GatewaySensor>();        
        if(gatewaySensor == null){
            gatewaySensor = new GatewaySensor();
            gatewaySensor.setGateway(gateway);
            gatewaySensor.setSensor(sensor);                                        
            // create gatewaysensor entity and persist
            addGatewaySensor(gatewaySensor);                
        }
        gatewaySensors = gateway.getGatewaySensor();
        gatewaySensor.setStatus(GatewaySensorStatus.ATTACHED);
        updateGatewaySensor(gateway.getId(), sensor.getId(), gatewaySensor);
        gatewaySensors.add(gatewaySensor);             
        gateway.setGatewaySensor(gatewaySensors); // set gatewaysensor set
        return updateGateway(gwId, gateway);        
    }      
    
    @Override
    public List<Gateway> getGatewaysOfHouse(long houseId)
    {
        return gatewaysRepository.findByAalhouse(houseId);
    }

    @Override
    public List<Gateway> getGateways() {        
        return gatewaysRepository.findAll();        
    }

    @Override
    public Gateway getGateway(long gwId) {        
        return gatewaysRepository.findOne(gwId);
    }

    @Override
    public Gateway getGateway(String gateway) {
        return gatewaysRepository.findByGateway(gateway);
    }

    @Override
    public List<Sensor> getHouseSensors(long houseId) {        
        List<Gateway> gateways = gatewaysRepository.findByAalhouse(houseId);
        List<Sensor> sensors = new ArrayList();        
        gateways.forEach((g)-> g.getGatewaySensor().forEach((i) -> sensors.add(i.getSensor())));        
        return sensors;
    }

    @Override
    public List<Sensor> getGatewayAllSensors(long gwId) {

        Gateway gateway = getGateway(gwId);
        List<Sensor> sensors = new ArrayList();        
        gateway.getGatewaySensor().forEach((i) -> sensors.add(i.getSensor()));        
        return sensors;
    }
    
    @Override
    public List<Sensor> getGatewayAttachedSensors(long gwId) {

        Gateway gateway = getGateway(gwId);
        List<Sensor> sensors = new ArrayList();                
        gateway.getGatewaySensor().stream().filter((gs) -> (gs.getStatus() == GatewaySensorStatus.ATTACHED)).forEachOrdered((gs) -> {
            sensors.add(gs.getSensor());
    });
        return sensors;
    }    

    @Override
    public Sensor addSensor(Sensor sensor, HashSet<Datatype> datatypes) {
        Sensor nSensor = new Sensor();
        nSensor.setAddress(sensor.getAddress());
        nSensor.setManufacturer(sensor.getManufacturer());
        nSensor.setCreatedAt(new java.util.Date());
        nSensor.setName(sensor.getName());
        nSensor.setStatus(SensorStatus.ENABLED);
        nSensor.setConnectable(sensor.isConnectable());
        nSensor.setConnection(sensor.getConnection());
        nSensor.setDatatypes(datatypes);
        return sensorsRepository.save(nSensor);
    }

    @Override
    public Sensor updateSensor(long sensorId, Sensor sensor, HashSet<Datatype> datatypes) {
        Sensor uSensor = this.getSensor(sensorId);
        uSensor.setAddress(sensor.getAddress());
        uSensor.setManufacturer(sensor.getManufacturer());
        uSensor.setUpdatedAt(new java.util.Date());
        uSensor.setName(sensor.getName());
        uSensor.setConnectable(sensor.isConnectable());
        uSensor.setConnection(sensor.getConnection());
        uSensor.setDatatypes(datatypes);
        return sensorsRepository.save(uSensor);
    }

    @Override
    public Sensor deleteSensor(long sensorId) {
        Sensor uSensor = this.getSensor(sensorId);
        uSensor.setUpdatedAt(new java.util.Date());
        uSensor.setStatus(SensorStatus.DISABLED);
        //TODO Remove it if attached to Gateway. -> DONE
        uSensor.getGatewaySensor().forEach((i)->detachSensorFromGateway(sensorId, i.getGateway().getId())); // remove flag = DETACHED
        return sensorsRepository.save(uSensor);
    }

    @Override
    public Sensor enableSensor(long sensorId) {
        Sensor uSensor = this.getSensor(sensorId);
        uSensor.setUpdatedAt(new java.util.Date());
        uSensor.setStatus(SensorStatus.ENABLED);
        return sensorsRepository.save(uSensor);
    }

    @Override
    public AALHouse enableHouse(long houseId) {
        AALHouse house = this.getHouse(houseId);
        house.setUpdatedAt(new java.util.Date());
        house.setStatus(AALHouseStatus.ENABLED);
        return housesRepository.save(house);
    }

    @Override
    public Sensor getSensor(long sensorId) {
        return sensorsRepository.findOne(sensorId);
    }

    @Override
    public Sensor getSensor(String address) {
        return sensorsRepository.findByAddress(address);
    }

    @Override
    public List<Datatype> getSensorData(long sensorId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Datatype> getDatatypes() {
        return datatypesRepo.findAll();
    }

    @Override
    public Datatype getDatatype(long typeId) {
        return datatypesRepo.findOne(typeId);
    }

    @Override
    public List<Sensor> getSensors() {
        return sensorsRepository.findAll();
    }

    @Override
    public List<Manufacturer> getManufactures() {
        return manufacturerRepo.findAll();
    }

    @Override
    public GatewaySensor addGatewaySensor(GatewaySensor gs) {
        gs.setStatus(GatewaySensorStatus.ATTACHED);
        gs.setCreatedAt(new java.util.Date());
        gs.setUpdatedAt(new java.util.Date());        
        return gatewaySensorRepository.save(gs);
    }     
    
    @Override
    public GatewaySensor deleteGatewaySensor(GatewaySensor gs)
    {
        gs.setStatus(GatewaySensorStatus.DETACHED);        
        gs.setUpdatedAt(new java.util.Date());        
        return gatewaySensorRepository.save(gs);        
    }
    
    @Override
    public GatewaySensor updateGatewaySensor(long gatewayId, long sensorId, GatewaySensor gs) {
        
        GatewaySensor gatewaySensor = gatewaySensorRepository.findOne(new GatewaySensorPK(gatewayId, sensorId));        
        gatewaySensor.setGateway(gs.getGateway());
        gatewaySensor.setSensor(gs.getSensor());        
        gatewaySensor.setUpdatedAt(new java.util.Date());
        gatewaySensor.setStatus(gs.getStatus());
        gatewaySensorRepository.save(gatewaySensor);
        return gatewaySensor;
    }   
    
    @Override
    public GatewaySensor getGatewaySensor(long gatewayId, long sensorId) {
        return gatewaySensorRepository.findOne(new GatewaySensorPK(gatewayId, sensorId));
    }        

    @Override
    public List<Sensor> getAttachedSensors() {
        return sensorsRepository.findAttached();
    }    
    
    @Override
    public List<Sensor> getDetachedSensors() {
        return sensorsRepository.findDetached();
    }

    @Override
    public List<Sensor> getUnallocatedSensors() {
        return sensorsRepository.findUnallocated();
    }       

    @Override
    public List<Sensor> getSensorsByGatewayIdAndDetached(long gatewayId) {        
        return null;
    }
   
    @Override
    public Gateway getGatewayByIdentity(String identity) {
        return gatewaysRepository.findByIdentity(identity);
    }

    @Override
    public Sensor updateSensor(long sensorId, Sensor sensor) {
        Sensor uSensor = this.getSensor(sensorId);
        uSensor.setAddress(sensor.getAddress());
        uSensor.setManufacturer(sensor.getManufacturer());
        uSensor.setUpdatedAt(new java.util.Date());
        uSensor.setName(sensor.getName());
        uSensor.setConnectable(sensor.isConnectable());
        uSensor.setConnection(sensor.getConnection());
        uSensor.setDatatypes(sensor.getDatatypes());
        uSensor.setLastPing(sensor.getLastPing()); // new
        return sensorsRepository.save(uSensor);
    }

    @Override
    public Gateway updateGateway(long gwId, Gateway gw, HashSet<Sensor> sensors) {
        Gateway gateway = this.getGateway(gwId);
        gateway.setAalhouse(gateway.getAalhouse());//TODO Check.
        gateway.setGateway(gw.getGateway());
        gateway.setStatus(gateway.getStatus());
        gateway.setUpdatedAt(new java.util.Date());
        gateway.setIdentity(gw.getIdentity()); // identity added        
        return gatewaysRepository.save(gateway);
    }

    @Override
    public Gateway addGateway(Gateway gw, HashSet<Sensor> sensors) {
        Gateway gateway = new Gateway();
        gateway.setGateway(gw.getGateway());
        gateway.setCreatedAt(new java.util.Date());
        gateway.setStatus(GatewayStatus.ENABLED);
        gateway.setToken(Generator.generateHash());
        gateway.setAalhouse(0);//Default if 0
        gateway.setIdentity(gw.getIdentity()); // identity added            
        return gatewaysRepository.save(gateway);
    }
    
    // GET COMMAND RESPONSES FROM CACHE SERVICE
    
    @Override
    public Object getCommandResponse(long gatewayId, String commandtype) throws IOException
    {   
        Gateway gateway = this.getGateway(gatewayId);
        MonitoringResponse monitoringResponse = new MonitoringResponse();
        CommandResponse commandResponse = new CommandResponse();
        Object responseObject = new Object();
        boolean hasKey = this.cacheService.hasKey(gateway.getIdentity()); // cache key
        if(hasKey)
        {            
            // get key stored in redis : battery and os commands requestS are stored as: BATTERY_INDICATION/OPERATING_SYSTEM
            commandtype = GatewayCommand.translateCommandType(commandtype);
            
            // type is stored as HASH KEY in upper case: convert! IMPORTANT!
            if(this.cacheService.hExistsHashKey(gateway.getIdentity(), commandtype.toUpperCase()))
            {   
                //get command response based on type for this gateway from cache service                
                Object object = this.cacheService.hGet(gateway.getIdentity(), commandtype.toUpperCase());
                Logger.getLogger(AALHouseServiceImpl.class.getName()).log(Level.INFO, "Received " + commandtype.toUpperCase() + " response from Gateway: {0}", gateway.getIdentity());                
                
                if(commandtype.equalsIgnoreCase(ApiMqtt.OPERATING_SYSTEM_TYPE) || commandtype.equalsIgnoreCase(ApiMqtt.RESOURCES_TYPE)){                                        
                    monitoringResponse = jsonMapper.convertValue(object, MonitoringResponse.class);
                    monitoringResponse.setId(UUID.randomUUID().toString());
                    return monitoringResponse;
                }
                else{                   
                    
                    commandResponse = jsonMapper.convertValue(object, CommandResponse.class);
                    commandResponse.setId(UUID.randomUUID().toString());
                    return commandResponse;
                }                 
            }
        }

        // NOT FOUND IN CACHE
        if (commandtype.equalsIgnoreCase(ApiMqtt.OPERATING_SYSTEM_TYPE) || commandtype.equalsIgnoreCase(ApiMqtt.RESOURCES_TYPE)) {
            monitoringResponse = new MonitoringResponse();
            monitoringResponse.setId(UUID.randomUUID().toString());
            responseObject = jsonMapper.convertValue(monitoringResponse, MonitoringResponse.class);
        } 
        else{
            commandResponse = new CommandResponse();
            commandResponse.setId(UUID.randomUUID().toString());
            responseObject = jsonMapper.convertValue(commandResponse, CommandResponse.class);
        }          
        return responseObject;
    }     

    @Override
    public Command sendPingCommand(long gatewayId) {
        Gateway gateway = gatewaysRepository.findOne(gatewayId);
        Set<GatewaySensor> sensors = gateway.getGatewaySensor();
        Command command = new Command();                
        String uuid = UUID.randomUUID().toString();
        command.setUuid(uuid);
        command.setType(ApiMqtt.PING_TYPE);
        command.setTimeout(5000);
        List<String> identities = new ArrayList();
        if (!sensors.isEmpty()) {
            sensors.stream().filter((s) -> (s.getStatus().equals(GatewaySensorStatus.ATTACHED))).map((s) -> s.getSensor().getAddress()).forEachOrdered((address) -> {
                identities.add(address);
            }); // add only ATTACHED devices!
        }
        command.setIdentities(identities);
        EmulationData emulationData = new EmulationData();
        emulationData.setUuid(uuid);
        emulationData.setTopic(ApiMqtt.COMMANDS_REQUEST_TOPIC_PREFIX + gateway.getIdentity());

        try {
            String message  = this.jsonMapper.writeValueAsString(command);
            emulationData.setMessage(message);
            emulationData.setEmulation(false);
            this.integrationGateway.execUnityEmulation(emulationData);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(AALHouseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return command;
    }    
    
    @Override
    public Command sendPingCommand(long gatewayId, String identity) {
        Gateway gateway = gatewaysRepository.findOne(gatewayId);        
        Command command = new Command();        
        String uuid = UUID.randomUUID().toString();
        command.setUuid(uuid);
        command.setType(ApiMqtt.PING_TYPE);
        command.setTimeout(5000);       
        List<String> identities = new ArrayList();
        identities.add(identity);
        command.setIdentities(identities);
        EmulationData emulationData = new EmulationData();
        emulationData.setUuid(uuid);
        emulationData.setTopic(ApiMqtt.COMMANDS_REQUEST_TOPIC_PREFIX + gateway.getIdentity());

        try {
            String message  = this.jsonMapper.writeValueAsString(command);
            emulationData.setMessage(message);
            emulationData.setEmulation(false);
            this.integrationGateway.execUnityEmulation(emulationData);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(AALHouseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return command;
    }    
    
    @Override
    public Command sendResourcesCommand(long gatewayId) {
        Gateway gateway = gatewaysRepository.findOne(gatewayId);
        Command command = new Command();
        String uuid = UUID.randomUUID().toString();
        command.setUuid(uuid);
        command.setType(ApiMqtt.RESOURCES_TYPE);
        EmulationData emulationData = new EmulationData();
        emulationData.setUuid(uuid);
        emulationData.setTopic(ApiMqtt.COMMANDS_REQUEST_TOPIC_PREFIX + gateway.getIdentity());
        try {
            String message  = this.jsonMapper.writeValueAsString(command);
            emulationData.setMessage(message);
            emulationData.setEmulation(false);
            this.integrationGateway.execUnityEmulation(emulationData);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(AALHouseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return command;
    }
    
    @Override
    public Command sendDeviceCommand(long gatewayId, String operation) {
        Gateway gateway = gatewaysRepository.findOne(gatewayId);
        Set<GatewaySensor> sensors = gateway.getGatewaySensor();
        Command command = new Command();
        String uuid = UUID.randomUUID().toString();
        command.setUuid(uuid);
        command.setType(ApiMqtt.DEVICE_TYPE);
        command.setOperation(operation.toUpperCase()); // TO UPPER CASE!
        EmulationData emulationData = new EmulationData();
        emulationData.setUuid(uuid);
        // find attached devices
        List<String> identities = new ArrayList();
        if (!sensors.isEmpty()) {
            sensors.stream().filter((s) -> (s.getStatus().equals(GatewaySensorStatus.ATTACHED))).map((s) -> s.getSensor().getAddress()).forEachOrdered((address) -> {
                identities.add(address);
            }); // add only ATTACHED devices!
        }
        command.setIdentities(identities);        
        emulationData.setTopic(ApiMqtt.COMMANDS_REQUEST_TOPIC_PREFIX + gateway.getIdentity());
        try {
            String message = this.jsonMapper.writeValueAsString(command);
            emulationData.setMessage(message);
            emulationData.setEmulation(false);
            this.integrationGateway.execUnityEmulation(emulationData);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(AALHouseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return command;
    }    
    
    @Override
    public Command sendDeviceCommand(long gatewayId, String operation, String identity) {
        Gateway gateway = gatewaysRepository.findOne(gatewayId);        
        Command command = new Command();
        String uuid = UUID.randomUUID().toString();
        command.setUuid(uuid);
        command.setType(ApiMqtt.DEVICE_TYPE);
        command.setOperation(operation.toUpperCase()); // TO UPPER CASE!
        
        EmulationData emulationData = new EmulationData();
        emulationData.setUuid(uuid);
        List<String> identities = new ArrayList();
        identities.add(identity);
        command.setIdentities(identities);      
        emulationData.setTopic(ApiMqtt.COMMANDS_REQUEST_TOPIC_PREFIX + gateway.getIdentity());
        try {
            String message = this.jsonMapper.writeValueAsString(command);
            emulationData.setMessage(message);
            emulationData.setEmulation(false);
            this.integrationGateway.execUnityEmulation(emulationData);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(AALHouseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return command;
    }      
    
    @Override
    public Command sendBatteryCommand(long gatewayId) {
        Gateway gateway = gatewaysRepository.findOne(gatewayId);
        Set<GatewaySensor> sensors = gateway.getGatewaySensor();        
        Command command = new Command();
        String uuid = UUID.randomUUID().toString();
        command.setUuid(uuid);
        command.setType(ApiMqtt.BATTERY_INDICATION_TYPE);
        EmulationData emulationData = new EmulationData();
        emulationData.setUuid(uuid);
        // find attached devices
        List<String> identities = new ArrayList();
        if (!sensors.isEmpty()) {
            sensors.stream().filter((s) -> (s.getStatus().equals(GatewaySensorStatus.ATTACHED))).map((s) -> s.getSensor().getAddress()).forEachOrdered((address) -> {
                identities.add(address);
            }); // add only ATTACHED devices!
        }
        command.setIdentities(identities);         
        emulationData.setTopic(ApiMqtt.COMMANDS_REQUEST_TOPIC_PREFIX + gateway.getIdentity());
        try {
            String message = this.jsonMapper.writeValueAsString(command);
            emulationData.setMessage(message);
            emulationData.setEmulation(false);
            this.integrationGateway.execUnityEmulation(emulationData);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(AALHouseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return command;
    }
    
    @Override
    public Command sendBatteryCommand(long gatewayId, String identity) {
        Gateway gateway = gatewaysRepository.findOne(gatewayId);        
        Command command = new Command();
        String uuid = UUID.randomUUID().toString();
        command.setUuid(uuid);
        command.setType(ApiMqtt.BATTERY_INDICATION_TYPE);
        EmulationData emulationData = new EmulationData();
        emulationData.setUuid(uuid);
        List<String> identities = new ArrayList();
        identities.add(identity);
        command.setIdentities(identities);        
        emulationData.setTopic(ApiMqtt.COMMANDS_REQUEST_TOPIC_PREFIX + gateway.getIdentity());
        try {
            String message = this.jsonMapper.writeValueAsString(command);
            emulationData.setMessage(message);
            emulationData.setEmulation(false);
            this.integrationGateway.execUnityEmulation(emulationData);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(AALHouseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return command;
    }    

    @Override
    public Command sendOperatingSystemCommand(long gatewayId, String operation) {
        Gateway gateway = gatewaysRepository.findOne(gatewayId);
        Command command = new Command();
        String uuid = UUID.randomUUID().toString();
        command.setUuid(uuid);
        command.setType(ApiMqtt.OPERATING_SYSTEM_TYPE);
        command.setOperation(operation.toUpperCase()); // TO UPPER CASE!
        EmulationData emulationData = new EmulationData();
        emulationData.setUuid(uuid);
        emulationData.setTopic(ApiMqtt.COMMANDS_REQUEST_TOPIC_PREFIX + gateway.getIdentity());
        try {
            String message = this.jsonMapper.writeValueAsString(command);
            emulationData.setMessage(message);
            emulationData.setEmulation(false);
            this.integrationGateway.execUnityEmulation(emulationData);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(AALHouseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return command;
    }
    
    @Override
    public Command sendApplicationCommand(long gatewayId, String operation) {
        Gateway gateway = gatewaysRepository.findOne(gatewayId);
        Command command = new Command();
        String uuid = UUID.randomUUID().toString();
        command.setUuid(uuid);
        command.setType(ApiMqtt.APPLICATION_TYPE);
        EmulationData emulationData = new EmulationData();
        emulationData.setUuid(uuid);
        emulationData.setTopic(ApiMqtt.COMMANDS_REQUEST_TOPIC_PREFIX + gateway.getIdentity());
        command.setOperation(operation.toUpperCase()); // TO UPPER CASE!
        try {
            String message = this.jsonMapper.writeValueAsString(command);
            emulationData.setMessage(message);
            emulationData.setEmulation(false);
            this.integrationGateway.execUnityEmulation(emulationData);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(AALHouseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return command;
    }    
   
    
    // commands mockups
    @Override
    public Object sendPingCommandMockup(long gatewayId, Command command) throws JsonProcessingException, IOException
    {     
        Gateway gateway = gatewaysRepository.findOne(gatewayId);
        CommandResponse cr = new CommandResponse();
        cr.setId(UUID.randomUUID().toString()); // JSON API identifier
        cr.setUuid(command.getUuid());
        cr.setType(command.getType());
        List<Result> results = new ArrayList();
        // get identities
        List<String> identities = command.getIdentities();
        Random rn = new Random();
        identities.forEach((id) -> {
            Result result = new Result();
            result.setId(UUID.randomUUID().toString()); // JSON API identifier
            result.setIdentity(id);
            result.setStatus(GatewayCommand.SUCCESSFUL);    
            result.setSignal(rn.nextInt(100+1));
            Sensor sensor = sensorsRepository.findByAddress(id);
            if(sensor == null){
                result = new Result();
                result.setId(UUID.randomUUID().toString()); // JSON API identifier
                result.setIdentity(id);                
                result.setStatus(GatewayCommand.TIMEOUT);    
                result.setError("Device does not respond");
            } else{
                sensor.setLastPing(new java.util.Date());
                updateSensor(sensor.getId(), sensor);                
            }            
            results.add(result);            
        }
        );
        cr.setResults(results);
        
        String keyName = gateway.getIdentity();
        String hashKey = command.getType();             
        String object = jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(cr); // convert object to json stringcr;
        
        boolean hasKey = cacheService.hasKey(keyName);        
        if(hasKey)
        {
            if(cacheService.hExistsHashKey(keyName, hashKey))
            {
                cacheService.hAdd(keyName, hashKey, cr, 60);
                logger.info("Updating command response to redis with key name: " + keyName +" and hash key -> "
                        +hashKey + ", response: "+jsonMapper.writeValueAsString(object));            
            }
            else
            {
                cacheService.hPutIfAbsent(keyName, hashKey, cr, 60);
                logger.info("Storing command response to redis with new key name: " + keyName +" and hash key -> "
                        +hashKey + ", response: "+jsonMapper.writeValueAsString(object));                
            }
        }
        else
        {
            cacheService.hPutIfAbsent(keyName, hashKey, cr, 60);
            logger.info("Storing command response to redis with new key name: " + keyName +" and hash key -> "
                    +hashKey + ", response: "+jsonMapper.writeValueAsString(object));
        }        
        CommandResponse response = (CommandResponse) getCommandResponse(gateway.getId(), command.getType());        
        return response;
    }     

    @Override
    public Object sendPingCommandMockup(long gatewayId) throws JsonProcessingException, IOException
    {     
        Gateway gateway = gatewaysRepository.findOne(gatewayId);
        List<Result> results = new ArrayList();
        CommandResponse cr = new CommandResponse();
        String uuid = UUID.randomUUID().toString();
        cr.setId(uuid); // JSON API identifier
        cr.setUuid(uuid);
        cr.setType("PING");      
        gateway.getGatewaySensor().stream().filter((gs) -> (gs.getStatus() == GatewaySensorStatus.ATTACHED)).forEachOrdered((gs) -> 
        {            
            Result res1 = new Result();
            res1.setId(uuid); // JSON API identifier
            res1.setIdentity(gs.getSensor().getAddress());
            res1.setStatus(GatewayCommand.SUCCESSFUL);
            res1.setSignal(-47);
            res1.setConnected(1);
            results.add(res1);      
            Sensor sensor = getSensor(gs.getSensor().getId());
            sensor.setLastPing(new java.util.Date());
            updateSensor(sensor.getId(), sensor);
        });

        cr.setResults(results);
        
        String keyName = gateway.getIdentity();
        String hashKey = "PING";     
        String object = jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(cr); // convert object to json stringcr;
        
        boolean hasKey = cacheService.hasKey(keyName);        
        if(hasKey)
        {
            if(cacheService.hExistsHashKey(keyName, hashKey))
            {
                cacheService.hAdd(keyName, hashKey, cr, 60);
                logger.info("Updating command response to redis with key name: " + keyName +" and hash key -> "
                        +hashKey + ", response: "+jsonMapper.writeValueAsString(object));            
            }
            else
            {
                cacheService.hPutIfAbsent(keyName, hashKey, cr, 60);
                logger.info("Storing command response to redis with new key name: " + keyName +" and hash key -> "
                        +hashKey + ", response: "+jsonMapper.writeValueAsString(object));                
            }
        }
        else
        {
            cacheService.hPutIfAbsent(keyName, hashKey, cr, 60);
            logger.info("Storing command response to redis with new key name: " + keyName +" and hash key -> "
                    +hashKey + ", response: "+jsonMapper.writeValueAsString(object));
        }        
        CommandResponse response = (CommandResponse) getCommandResponse(gateway.getId(), "PING");        
        return response;
    }    
    
    @Override
    public Object sendResourcesCommandMockup(long gatewayId, Command command) throws JsonProcessingException, IOException
    {     
        Gateway gateway = gatewaysRepository.findOne(gatewayId);
        MonitoringResponse mr = jsonMapper.readValue(MockData.MONITORING_RESPONSE_MOCK, MonitoringResponse.class);
        String keyName = gateway.getIdentity();
        String hashKey = command.getType();
        String object = jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(mr); // convert object to json string;

        boolean hasKey = cacheService.hasKey(keyName);
        if (hasKey) {
            if (cacheService.hExistsHashKey(keyName, hashKey)) {
                cacheService.hAdd(keyName, hashKey, mr, 60);
                logger.info("Updating command response to redis with key name: " + keyName + " and hash key -> "
                        + hashKey + ", response: " + jsonMapper.writeValueAsString(object));
            } else {
                cacheService.hPutIfAbsent(keyName, hashKey, mr, 60);
                logger.info("Storing command response to redis with new key name: " + keyName + " and hash key -> "
                        + hashKey + ", response: " + jsonMapper.writeValueAsString(object));
            }
        } else {
            cacheService.hPutIfAbsent(keyName, hashKey, mr, 60);
            logger.info("Storing command response to redis with new key name: " + keyName + " and hash key -> "
                    + hashKey + ", response: " + jsonMapper.writeValueAsString(object));
        }
        MonitoringResponse response = (MonitoringResponse) getCommandResponse(gateway.getId(), command.getType());
        return response;
    }      
    
    @Override
    public Gateway getDashboard(long gatewayId)
    {
        // query redis cache to get results
        Gateway gateway = this.gatewaysRepository.findOne(gatewayId);
        boolean hasKey = this.cacheService.hasKey(gateway.getIdentity()); // cache key        
        return gateway;
    }
    
    @Override
    public Object sendDeviceCommandMockup(long gatewayId, Command command) throws JsonProcessingException, IOException
    {     
        Gateway gateway = gatewaysRepository.findOne(gatewayId);
        CommandResponse cr = new CommandResponse();
        cr.setId(UUID.randomUUID().toString()); // JSON API identifier
        cr.setUuid(command.getUuid());
        cr.setType(command.getType());
        cr.setOperation(command.getOperation());
        List<Result> results = new ArrayList();
        
        // get identities
        List<String> identities = command.getIdentities();
        identities.forEach((id) -> {
            Result result = new Result();
            result.setId(UUID.randomUUID().toString()); // JSON API identifier
            result.setIdentity(id);
            result.setStatus(GatewayCommand.SUCCESSFUL);            
            results.add(result);            
        }
        );
        cr.setResults(results);
        
        String keyName = gateway.getIdentity();
        String hashKey = command.getType();             
        String object = jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(cr); // convert object to json stringcr;
        
        boolean hasKey = cacheService.hasKey(keyName);        
        if(hasKey)
        {
            if(cacheService.hExistsHashKey(keyName, hashKey))
            {
                cacheService.hAdd(keyName, hashKey, cr, 60);
                logger.info("Updating command response to redis with key name: " + keyName +" and hash key -> "
                        +hashKey + ", response: "+jsonMapper.writeValueAsString(object));            
            }
            else
            {
                cacheService.hPutIfAbsent(keyName, hashKey, cr, 60);
                logger.info("Storing command response to redis with new key name: " + keyName +" and hash key -> "
                        +hashKey + ", response: "+jsonMapper.writeValueAsString(object));                
            }
        }
        else
        {
            cacheService.hPutIfAbsent(keyName, hashKey, cr, 60);
            logger.info("Storing command response to redis with new key name: " + keyName +" and hash key -> "
                    +hashKey + ", response: "+jsonMapper.writeValueAsString(object));
        }        
        CommandResponse response = (CommandResponse) getCommandResponse(gateway.getId(), command.getType());        
        return response;
    }    
    
    @Override
    public Object sendDeviceCommandMockup(long gatewayId) throws JsonProcessingException, IOException {
        Gateway gateway = gatewaysRepository.findOne(gatewayId);
        List<Result> results = new ArrayList();
        CommandResponse cr = new CommandResponse();
        String uuid = UUID.randomUUID().toString();
        cr.setId(uuid); // JSON API identifier
        cr.setUuid(uuid);
        cr.setType("DEVICE");  
        Random rn = new Random();
        gateway.getGatewaySensor().stream().filter((gs) -> (gs.getStatus() == GatewaySensorStatus.ATTACHED)).forEachOrdered((gs) -> 
        {            
            Result result = new Result();
            result.setId(uuid); // JSON API identifier
            result.setIdentity(gs.getSensor().getAddress());
            result.setStatus(GatewayCommand.SUCCESSFUL);                                    
            results.add(result);                  
        });
        cr.setResults(results);
        
        String keyName = gateway.getIdentity();
        String hashKey = "DEVICE";     
        String object = jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(cr); // convert object to json stringcr;
        
        boolean hasKey = cacheService.hasKey(keyName);        
        if(hasKey)
        {
            if(cacheService.hExistsHashKey(keyName, hashKey))
            {
                cacheService.hAdd(keyName, hashKey, cr, 60);
                logger.info("Updating command response to redis with key name: " + keyName +" and hash key -> "
                        +hashKey + ", response: "+jsonMapper.writeValueAsString(object));            
            }
            else
            {
                cacheService.hPutIfAbsent(keyName, hashKey, cr, 60);
                logger.info("Storing command response to redis with new key name: " + keyName +" and hash key -> "
                        +hashKey + ", response: "+jsonMapper.writeValueAsString(object));                
            }
        }
        else
        {
            cacheService.hPutIfAbsent(keyName, hashKey, cr, 60);
            logger.info("Storing command response to redis with new key name: " + keyName +" and hash key -> "
                    +hashKey + ", response: "+jsonMapper.writeValueAsString(object));
        }        
        CommandResponse response = (CommandResponse) getCommandResponse(gateway.getId(), "DEVICE");        
        return response;
    }    
    
    @Override
    public Object sendBatteryCommandMockup(long gatewayId, Command command) throws JsonProcessingException, IOException
    {     
        Gateway gateway = gatewaysRepository.findOne(gatewayId);
        CommandResponse cr = new CommandResponse();
        cr.setId(UUID.randomUUID().toString()); // JSON API identifier
        cr.setUuid(command.getUuid());
        cr.setType(command.getType());
        List<Result> results = new ArrayList();
        // get identities
        List<String> identities = command.getIdentities();
        Random rn = new Random();
        identities.forEach((id) -> {
            Result result = new Result();
            result.setId(UUID.randomUUID().toString()); // JSON API identifier
            result.setIdentity(id);
            result.setStatus(GatewayCommand.SUCCESSFUL);    
            result.setBattery(rn.nextInt(100+1));
            Sensor sensor = sensorsRepository.findByAddress(id);
            if(sensor == null){
                result = new Result();
                result.setId(UUID.randomUUID().toString()); // JSON API identifier
                result.setIdentity(id);                
                result.setStatus(GatewayCommand.TIMEOUT);    
                result.setError("Device does not respond");
            } else{
                sensor.setBattery(result.getBattery());
                updateSensor(sensor.getId(), sensor);                
            }            
            results.add(result);            
        }
        );
        cr.setResults(results);
        
        String keyName = gateway.getIdentity();
        String hashKey = command.getType();             
        String object = jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(cr); // convert object to json stringcr;
        
        boolean hasKey = cacheService.hasKey(keyName);        
        if(hasKey)
        {
            if(cacheService.hExistsHashKey(keyName, hashKey))
            {
                cacheService.hAdd(keyName, hashKey, cr, 60);
                logger.info("Updating command response to redis with key name: " + keyName +" and hash key -> "
                        +hashKey + ", response: "+jsonMapper.writeValueAsString(object));            
            }
            else
            {
                cacheService.hPutIfAbsent(keyName, hashKey, cr, 60);
                logger.info("Storing command response to redis with new key name: " + keyName +" and hash key -> "
                        +hashKey + ", response: "+jsonMapper.writeValueAsString(object));                
            }
        }
        else
        {
            cacheService.hPutIfAbsent(keyName, hashKey, cr, 60);
            logger.info("Storing command response to redis with new key name: " + keyName +" and hash key -> "
                    +hashKey + ", response: "+jsonMapper.writeValueAsString(object));
        }        
        CommandResponse response = (CommandResponse) getCommandResponse(gateway.getId(), command.getType());        
        return response;
    }  
    
    @Override
    public Object sendBatteryCommandMockup(long gatewayId) throws JsonProcessingException, IOException
    {     
        Gateway gateway = gatewaysRepository.findOne(gatewayId);
        List<Result> results = new ArrayList();
        CommandResponse cr = new CommandResponse();
        String uuid = UUID.randomUUID().toString();
        cr.setId(uuid); // JSON API identifier
        cr.setUuid(uuid);
        cr.setType("BATTERY_INDICATION");  
        Random rn = new Random();
        gateway.getGatewaySensor().stream().filter((gs) -> (gs.getStatus() == GatewaySensorStatus.ATTACHED)).forEachOrdered((gs) -> 
        {            
            Result result = new Result();
            result.setId(uuid); // JSON API identifier
            result.setIdentity(gs.getSensor().getAddress());
            result.setStatus(GatewayCommand.SUCCESSFUL);            
            result.setConnected(1);
            result.setBattery(rn.nextInt(100+1));
            results.add(result);      
            Sensor sensor = getSensor(gs.getSensor().getId());
            sensor.setBattery(result.getBattery());
            updateSensor(sensor.getId(), sensor);
        });

        cr.setResults(results);
        
        String keyName = gateway.getIdentity();
        String hashKey = "BATTERY_INDICATION";     
        String object = jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(cr); // convert object to json stringcr;
        
        boolean hasKey = cacheService.hasKey(keyName);        
        if(hasKey)
        {
            if(cacheService.hExistsHashKey(keyName, hashKey))
            {
                cacheService.hAdd(keyName, hashKey, cr, 60);
                logger.info("Updating command response to redis with key name: " + keyName +" and hash key -> "
                        +hashKey + ", response: "+jsonMapper.writeValueAsString(object));            
            }
            else
            {
                cacheService.hPutIfAbsent(keyName, hashKey, cr, 60);
                logger.info("Storing command response to redis with new key name: " + keyName +" and hash key -> "
                        +hashKey + ", response: "+jsonMapper.writeValueAsString(object));                
            }
        }
        else
        {
            cacheService.hPutIfAbsent(keyName, hashKey, cr, 60);
            logger.info("Storing command response to redis with new key name: " + keyName +" and hash key -> "
                    +hashKey + ", response: "+jsonMapper.writeValueAsString(object));
        }        
        CommandResponse response = (CommandResponse) getCommandResponse(gateway.getId(), "BATTERY");        
        return response;
    }     
    
    @Override
    public Object sendOperatingSystemCommandMockup(long gatewayId, Command command) throws JsonProcessingException, IOException
    {     
        Gateway gateway = gatewaysRepository.findOne(gatewayId);
        MonitoringResponse mr = jsonMapper.readValue(MockData.MONITORING_RESPONSE_MOCK, MonitoringResponse.class);
        String keyName = gateway.getIdentity();
        String hashKey = command.getType();
        String object = jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(mr); // convert object to json string;

        // simulate command operation REBOOT
        if(command.getOperation().equalsIgnoreCase("reboot")){
            boolean hasKey = cacheService.hasKey(keyName);
            if (hasKey) {
                if (cacheService.hExistsHashKey(keyName, hashKey)) {
                    cacheService.hAdd(keyName, hashKey, mr, 60);
                    logger.info("Updating command response to redis with key name: " + keyName + " and hash key -> "
                            + hashKey + ", response: " + jsonMapper.writeValueAsString(object));
                } else {
                    cacheService.hPutIfAbsent(keyName, hashKey, mr, 60);
                    logger.info("Storing command response to redis with new key name: " + keyName + " and hash key -> "
                            + hashKey + ", response: " + jsonMapper.writeValueAsString(object));
                }
            } else {
                cacheService.hPutIfAbsent(keyName, hashKey, mr, 60);
                logger.info("Storing command response to redis with new key name: " + keyName + " and hash key -> "
                        + hashKey + ", response: " + jsonMapper.writeValueAsString(object));
            }
            MonitoringResponse response = (MonitoringResponse) getCommandResponse(gateway.getId(), command.getType());
            return response;
        }
        MonitoringResponse monitoringResponse = new MonitoringResponse();
        monitoringResponse.setId(UUID.randomUUID().toString());
        return monitoringResponse;
    }     


}
