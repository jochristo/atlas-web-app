package org.atlas.modules.aalhouse.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;

import org.atlas.api.ApiEndpoints;
import org.atlas.api.services.AALHouseService;
import org.atlas.core.jsonapi.JSONApiData;
import org.atlas.core.jsonapi.JSONApiDocument;
import org.atlas.modules.aalhouse.domain.AALHouse;
import org.atlas.modules.aalhouse.domain.Datatype;
import org.atlas.modules.aalhouse.domain.Gateway;
import org.atlas.modules.aalhouse.domain.GatewaySensor;
import org.atlas.modules.aalhouse.domain.GatewaySensorStatus;
import org.atlas.modules.aalhouse.domain.Sensor;
import org.atlas.modules.mqtt.model.Command;
import org.atlas.modules.mqtt.model.CommandResponse;
import org.atlas.modules.mqtt.model.MonitoringResponse;
import org.atlas.modules.mqtt.model.Result;
import org.atlas.modules.mqtt.model.SystemMemory;
import org.atlas.modules.mqtt.model.SystemPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Core ( Security Controllers )
 * @since 2016-07-27
 **/

@RestController
public class HouseController {
	
    private static final Logger logger = LoggerFactory.getLogger(HouseController.class);

    @Autowired
    private AALHouseService aalHouseService;

    /**
     * List all aalhouses
     * @param request The body request
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.AALHOUSES, method = RequestMethod.GET)
    public JSONApiDocument listAALhouses(HttpServletRequest request) {
        return new JSONApiDocument(aalHouseService.getHouses());
    }

    /**
     * Adds new Aalhouse
     * @param document the body document
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.AALHOUSES, method = RequestMethod.POST)
    public JSONApiDocument addAALhouse(@RequestBody JSONApiDocument document) {
        JSONApiData payload = (JSONApiData) document.getData();
        AALHouse aalhouse = (AALHouse) payload.convertTo(AALHouse.class);
        return new JSONApiDocument(aalHouseService.addHouse(aalhouse));
    }

    @RequestMapping(value = ApiEndpoints.AALHOUSES_BYID, method = RequestMethod.GET)
    public JSONApiDocument getAALhouse(@PathVariable long houseId) {
        return new JSONApiDocument(aalHouseService.getHouse(houseId));
    }

    @RequestMapping(value = ApiEndpoints.AALHOUSES_BYID, method = RequestMethod.PUT)
    public JSONApiDocument updateAALhouse(@PathVariable long houseId, @RequestBody JSONApiDocument document) {
        JSONApiData payload = (JSONApiData) document.getData();
        AALHouse aalhouse = (AALHouse) payload.convertTo(AALHouse.class);
        return new JSONApiDocument(aalHouseService.updateHouse(houseId, aalhouse));
    }

    @RequestMapping(value = ApiEndpoints.AALHOUSES_BYID, method = RequestMethod.DELETE)
    public JSONApiDocument deleteAALhouse(@PathVariable long houseId) {
        return new JSONApiDocument(aalHouseService.deleteHouse(houseId));
    }

    @RequestMapping(value = ApiEndpoints.AALHOUSES_BYID_ENABLE, method = RequestMethod.GET)
    public JSONApiDocument enableAalhouse(@PathVariable long houseId) {
        return new JSONApiDocument(aalHouseService.enableHouse(houseId));
    }

    /**
     * Returns a json list of all gateways "attached" to given AALHouse.
     * @param houseId The id of the aalhouse
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.AALHOUSES_GATEWAYS, method = RequestMethod.GET)
    public JSONApiDocument listAalhouseGateways(@PathVariable long houseId) {
        List<Gateway> gateways = aalHouseService.getHouseGateways(houseId);
        return new JSONApiDocument(gateways);
    }

    /**
     * Returns a json list of all sensors "attached" to gateways of given AALHouse.
     * @param houseId the id of the aalhouse
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.AALHOUSES_SENSORS, method = RequestMethod.GET)
    public JSONApiDocument listAalhouseSensors(@PathVariable long houseId) {
        List<Sensor> sensors = aalHouseService.getHouseSensors(houseId);
        return new JSONApiDocument(sensors);
    }

    /**
     * Attaches given gateway to given AALHouse and returns updated AALHouse.
     * @param gatewayId The id of the gateway
     * @param houseId The id of the aalhouse
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.AALHOUSES_GATEWAYS_BYID, method = RequestMethod.GET)
    public JSONApiDocument attachGateway(@PathVariable long houseId, @PathVariable long gatewayId) {
        AALHouse house = aalHouseService.getHouse(houseId);
        Gateway gateway = aalHouseService.attachGatewayToHouse(gatewayId, houseId);
        List<Gateway> gateways = aalHouseService.getGatewaysOfHouse(houseId);
        JSONApiDocument document = new JSONApiDocument(house);
        document.includeDataAsArrayList((ArrayList<?>) gateways);
        return document;
    }

    /**
     * Detaches given gateway from given AALHouse and returns updated AALHouse.
     * @param gatewayId The id of the gateway
     * @param houseId The id of the aalhouse
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.AALHOUSES_GATEWAYS_BYID, method = RequestMethod.DELETE)
    public JSONApiDocument detachGateway(@PathVariable long houseId, @PathVariable long gatewayId) {
        AALHouse house = aalHouseService.getHouse(houseId);
        Gateway gateway = aalHouseService.removeGatewayFromHouse(gatewayId, houseId);
        List<Gateway> gateways = aalHouseService.getGatewaysOfHouse(houseId);
        JSONApiDocument document = new JSONApiDocument(house);
        document.includeDataAsArrayList((ArrayList<?>) gateways);
        return document;
    }

    @RequestMapping(value = ApiEndpoints.GATEWAYS, method = RequestMethod.GET)
    public JSONApiDocument listGateways() {
        return new JSONApiDocument(aalHouseService.getGateways());
    }

    @RequestMapping(value = ApiEndpoints.GATEWAYS, method = RequestMethod.POST)
    public JSONApiDocument addGateway(@RequestBody JSONApiDocument document) {
        JSONApiData payload = (JSONApiData) document.getData();
        Gateway gateway = (Gateway) payload.convertTo(Gateway.class);
        //include sensors
        ArrayList<JSONApiData> included = (ArrayList<JSONApiData>) document.getIncluded();
        HashSet<Sensor> sensors = new HashSet<Sensor>();
        for (JSONApiData data : included) {
            if (data.getType().equals("sensor")) {
                sensors.add(aalHouseService.getSensor(Long.parseLong(data.getId())));
            }
        }
        return new JSONApiDocument(aalHouseService.addGateway(gateway, sensors));
    }

    /**
     * Returns gateway of given id with attached sensors.     
     * @param gatewayId The id of the gateway
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.GATEWAYS_BYID, method = RequestMethod.GET)
    public JSONApiDocument getGateway(@PathVariable long gatewayId) {
        Gateway gateway = aalHouseService.getGateway(gatewayId);
        JSONApiDocument document = new JSONApiDocument(gateway);
        Set<Sensor> sensors = new HashSet<>();
        gateway.getGatewaySensor().stream().filter((gs) -> (gs.getStatus() == GatewaySensorStatus.ATTACHED)).forEachOrdered((gs) ->  
        {
            sensors.add(gs.getSensor());
        });        
        document.appendIncludedDataCollection(sensors);        
        return document;
    }

    /**
     * Updates given gateway with included (if any) sensors in the body. Updates
     * sensor's status: ATTACHED or DETACHED.     
     * @param gatewayId The id of the gateway
     * @param document The body of the document
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.GATEWAYS_BYID, method = RequestMethod.PUT)
    public JSONApiDocument updateGateway(@PathVariable long gatewayId, @RequestBody JSONApiDocument document) {
        JSONApiData payload = (JSONApiData) document.getData();
        Gateway documentGateway = (Gateway) payload.convertTo(Gateway.class);
        Gateway gateway = aalHouseService.getGateway(gatewayId);
        //include sensors
        Set<GatewaySensor> gatewaySensors = new HashSet<GatewaySensor>();
        ArrayList<JSONApiData> included = (ArrayList<JSONApiData>) document.getIncluded();
        for (JSONApiData data : included) {
            if (data.getType().equals("sensor")) {
                Sensor sensor = (Sensor) data.convertTo(Sensor.class);
                Sensor fetched = aalHouseService.getSensor(Long.parseLong(data.getId()));
                GatewaySensor gatewaySensor = aalHouseService.getGatewaySensor(gateway.getId(), fetched.getId());
                // ATTACHED FOR THIS GATEWAY FLAG (ATTACH)
                if (sensor.isEnabled()) //.getStatus() == SensorStatus.ENABLED){                         
                {
                    if (gatewaySensor == null) {
                        gatewaySensor = new GatewaySensor();
                        gatewaySensor.setGateway(gateway);
                        gatewaySensor.setSensor(fetched);
                        aalHouseService.addGatewaySensor(gatewaySensor);
                    }
                    gatewaySensor.setStatus(GatewaySensorStatus.ATTACHED);
                    aalHouseService.updateGatewaySensor(gateway.getId(), fetched.getId(), gatewaySensor);
                    gatewaySensors.add(gatewaySensor);

                } // DELETED FOR THIS GATEWAY FLAG (DETACH)
                else //if(sensor.getStatus() == SensorStatus.DELETED){                        
                {
                    if (gatewaySensor == null) {
                        gatewaySensor = new GatewaySensor();
                        gatewaySensor.setGateway(gateway);
                        gatewaySensor.setSensor(fetched);
                        aalHouseService.addGatewaySensor(gatewaySensor);
                    }
                    gatewaySensor.setStatus(GatewaySensorStatus.DETACHED);
                    aalHouseService.updateGatewaySensor(gateway.getId(), fetched.getId(), gatewaySensor);
                    gatewaySensors.add(gatewaySensor);
                }
            }
        }
        gateway.setAalhouse(documentGateway.getAalhouse());
        gateway.setGateway(documentGateway.getGateway());
        gateway.setIdentity(documentGateway.getIdentity());
        gateway.setGatewaySensor(gatewaySensors); // set gatewaysensor set            
        aalHouseService.updateGateway(gatewayId, gateway);
        Set<Sensor> sensors = new HashSet<Sensor>();
        gateway.getGatewaySensor().forEach((i) -> sensors.add(i.getSensor()));
        document.includeDataAsSet(sensors);
        return document;
    }

    @RequestMapping(value = ApiEndpoints.GATEWAYS_BYID, method = RequestMethod.DELETE)
    public JSONApiDocument deleteGateway(@PathVariable long gatewayId) {
        return new JSONApiDocument(aalHouseService.deleteGateway(gatewayId));
    }

    @RequestMapping(value = ApiEndpoints.GATEWAYS_BYID_ENABLE, method = RequestMethod.GET)
    public JSONApiDocument enableGateway(@PathVariable long gatewayId) {
        return new JSONApiDocument(aalHouseService.enableGateway(gatewayId));
    }

    /**
     * Returns a list of all gateway-sensor pairs in all states.
     * @param gatewayId The id of the gateway
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.GATEWAYS_SENSORS, method = RequestMethod.GET)
    public JSONApiDocument listGatewaySensors(@PathVariable long gatewayId) {
        List<Sensor> sensors = aalHouseService.getGatewayAttachedSensors(gatewayId);
        return new JSONApiDocument(sensors);
    }

    /**
     * Attaches given sensor to given gateway and sets sensor's status to ATTACHED.
     * @param gatewayId The id of the gateway
     * @param sensorId The id of the sensor
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.GATEWAYS_SENSORS_BYID, method = RequestMethod.GET)
    public JSONApiDocument attachSensor(@PathVariable long gatewayId, @PathVariable long sensorId) {
        Gateway gateway = aalHouseService.attachSensorToGateway(sensorId, gatewayId);
        JSONApiDocument document = new JSONApiDocument(gateway);
        Set<Sensor> sensors = new HashSet<>();
        gateway.getGatewaySensor().forEach((i) -> sensors.add(i.getSensor()));
        document.includeDataAsSet(sensors);
        return document;
    }

    /**
     * Detaches given sensor from given gateway and sets sensor's status to DETACHED.
     * @param gatewayId The id of the gateway
     * @param sensorId The id of the sensor
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.GATEWAYS_SENSORS_BYID, method = RequestMethod.DELETE)
    public JSONApiDocument detachSensor(@PathVariable long gatewayId, @PathVariable long sensorId) {
        Gateway gateway = aalHouseService.detachSensorFromGateway(sensorId, gatewayId);
        JSONApiDocument document = new JSONApiDocument(gateway);
        Set<Sensor> sensors = new HashSet<Sensor>();
        gateway.getGatewaySensor().forEach((i) -> sensors.add(i.getSensor()));
        document.includeDataAsSet(sensors);
        return document;
    }

    /**
     * Updates given sensor of given gateway with new values.
     * @param gatewayId The id of the gateway
     * @param sensorId The id of the sensor
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.GATEWAYS_SENSORS_BYID, method = RequestMethod.PUT)
    public JSONApiDocument updateSensorOfGateway(@PathVariable long gatewayId, @PathVariable long sensorId) {
        Gateway gateway = aalHouseService.getGateway(gatewayId);
        Sensor fetched = aalHouseService.getSensor(sensorId);
        GatewaySensor gatewaySensor = aalHouseService.getGatewaySensor(gateway.getId(), fetched.getId());
        if (gatewaySensor == null) {
            gatewaySensor = new GatewaySensor();
            gatewaySensor.setGateway(gateway);
            gatewaySensor.setSensor(fetched);
            aalHouseService.addGatewaySensor(gatewaySensor);
            Gateway newGateway = aalHouseService.getGateway(gatewayId);
            JSONApiDocument document = new JSONApiDocument(newGateway);
            Set<Sensor> sensors = new HashSet<Sensor>();
            newGateway.getGatewaySensor().forEach((i) -> sensors.add(i.getSensor()));
            document.includeDataAsSet(sensors);
            return document;
        }
        return new JSONApiDocument();
    }

    @RequestMapping(value = ApiEndpoints.SENSORS, method = RequestMethod.GET)
    public JSONApiDocument listSensors() {
        return new JSONApiDocument(aalHouseService.getSensors());
    }

    /**
     * Returns a list of all sensors in attached state.
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.ATTACHED_SENSORS, method = RequestMethod.GET)
    public JSONApiDocument listAttachedSensors() {
        return new JSONApiDocument(aalHouseService.getAttachedSensors());
    }

    /**
     * Returns a list of all sensors in detached state.
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.DETACHED_SENSORS, method = RequestMethod.GET)
    public JSONApiDocument listDetachedSensors() {
        return new JSONApiDocument(aalHouseService.getDetachedSensors());
    }

    /**
     * Returns a list of all unallocated sensors (either available or in
     * detached state)
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.UNALLOCATED_SENSORS, method = RequestMethod.GET)
    public JSONApiDocument listUnallocatedSensors() {
        return new JSONApiDocument(aalHouseService.getUnallocatedSensors());
    }

    @RequestMapping(value = ApiEndpoints.SENSORS, method = RequestMethod.POST)
    public JSONApiDocument addSensor(@RequestBody JSONApiDocument document) {
        JSONApiData payload = (JSONApiData) document.getData();
        Sensor sensor = (Sensor) payload.convertTo(Sensor.class);

        ArrayList<JSONApiData> included = (ArrayList<JSONApiData>) document.getIncluded();
        HashSet<Datatype> datatypes = new HashSet<Datatype>();
        for (JSONApiData data : included) {
            if (data.getType().equals("datatype")) {
                Datatype datatype = (Datatype) data.convertTo(Datatype.class);
                if (datatype.isEnabled()) {
                    datatypes.add(aalHouseService.getDatatype(Long.parseLong(data.getId())));
                }
            }
        }
        return new JSONApiDocument(aalHouseService.addSensor(sensor, datatypes));
    }

    @RequestMapping(value = ApiEndpoints.SENSORS_BYID, method = RequestMethod.GET)
    public JSONApiDocument getSensor(@PathVariable long sensorId) {
        Sensor sensor = aalHouseService.getSensor(sensorId);
        JSONApiDocument document = new JSONApiDocument(sensor);
        document.includeDataAsSet(sensor.getDatatypes());
        return document;
    }

    @RequestMapping(value = ApiEndpoints.SENSORS_BYID, method = RequestMethod.PUT)
    public JSONApiDocument updateSensor(@PathVariable long sensorId, @RequestBody JSONApiDocument document) {
        JSONApiData payload = (JSONApiData) document.getData();
        Sensor sensor = (Sensor) payload.convertTo(Sensor.class);

        ArrayList<JSONApiData> included = (ArrayList<JSONApiData>) document.getIncluded();
        HashSet<Datatype> datatypes = new HashSet<Datatype>();
        for (JSONApiData data : included) {
            if (data.getType().equals("datatype")) {
                Datatype datatype = (Datatype) data.convertTo(Datatype.class);
                if (datatype.isEnabled()) {
                    datatypes.add(aalHouseService.getDatatype(Long.parseLong(data.getId())));
                }
            }
        }
        return new JSONApiDocument(aalHouseService.updateSensor(sensorId, sensor, datatypes));
    }

    @RequestMapping(value = ApiEndpoints.SENSORS_BYID, method = RequestMethod.DELETE)
    public JSONApiDocument deleteSensor(@PathVariable long sensorId) {
        return new JSONApiDocument(aalHouseService.deleteSensor(sensorId));
    }

    @RequestMapping(value = ApiEndpoints.SENSORS_BYID_ENABLE, method = RequestMethod.GET)
    public JSONApiDocument getSensorDataType(@PathVariable long sensorId) {
        return new JSONApiDocument(aalHouseService.enableSensor(sensorId));
    }

    @RequestMapping(value = ApiEndpoints.SENSORS_BYID_DATATYPE, method = RequestMethod.GET)
    public JSONApiDocument getSensorDataType(HttpServletRequest request) {
        //TODO
        return new JSONApiDocument();
    }

    @RequestMapping(value = ApiEndpoints.SENSORS_BYID_DATATYPE, method = RequestMethod.PUT)
    public JSONApiDocument updateSensorDataType(HttpServletRequest request) {
        //TODO
        return new JSONApiDocument();
    }

    @RequestMapping(value = ApiEndpoints.DATATYPES, method = RequestMethod.GET)
    public JSONApiDocument getDatatypes() {
        return new JSONApiDocument(aalHouseService.getDatatypes());
    }

    @RequestMapping(value = ApiEndpoints.MANUFACTURES, method = RequestMethod.GET)
    public JSONApiDocument getManufactures() {
        return new JSONApiDocument(aalHouseService.getManufactures());
    }

    // GATEWAY COMMANDS REQUESTS  - START
    
    /**
     * Returns a gateway command response for given gateway id and command type
     * persisted in caching system. Usage example: commandType e.g. DEVICE, OS,
     * BATTERY, RESOURCES, PING, APPLICATION, etc. Returned data is converted to
     * {@link CommandResponse} or {@link MonitoringResponse} object stored in cache.
     * @param gatewayId The id of the gateway
     * @param commandType The TYPE of the command: e.g. PING, DEVICE, OS, APPLICATION, BATTERY, etc.
     * @return JSONApiDocument
     * @throws java.io.IOException Thrown if ObjectMapper read operations fail
     */
    @RequestMapping(value = ApiEndpoints.GATEWAYS_COMMAND_RESPONSE, method = RequestMethod.GET)
    public JSONApiDocument requestCommandResponse(@PathVariable long gatewayId, @PathVariable String commandType) throws IOException 
    {
        Object object = aalHouseService.getCommandResponse(gatewayId, commandType);

        // DETERMINE OBJECT'S TYPE AND DELIVER JSON DOCUMENT:                                
        if (object.getClass().isInstance(new MonitoringResponse())) { // MonitoringResponse
            MonitoringResponse mr = MonitoringResponse.class.cast(object);
            JSONApiDocument document = new JSONApiDocument(mr);
            List<SystemMemory> systemMemories = new ArrayList<>();            
            if (mr.getSystemMemories() != null) {
                mr.getSystemMemories().forEach((i) -> systemMemories.add(i));                
                document.appendIncludedDataCollection(systemMemories);
            }
            List<SystemPartition> systemPartitions = new ArrayList<>();            
            if (mr.getSystemPartitions() != null) {
                mr.getSystemPartitions().forEach((i) -> systemPartitions.add(i));                
                document.appendIncludedDataCollection(systemPartitions);
            }
            return document;
        } else if (object.getClass().isInstance(new CommandResponse())) { // OR CommandResponse
            CommandResponse cr = CommandResponse.class.cast(object);
            JSONApiDocument document = new JSONApiDocument(cr);
            List<Result> results = new ArrayList<>();            
            if (cr.getResults() != null) {
                cr.getResults().forEach((i) -> results.add(i));                
                document.appendIncludedDataCollection(results);
            }
            return document;
        }
        return new JSONApiDocument();
    }

    /**
     * Makes the request to send a ping command to reach given gateway and given
     * sensor identity.
     * @param gatewayId The id of the gateway
     * @param identity The identity of the sensor
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.GATEWAYS_PING_COMMAND, method = RequestMethod.GET)
    public JSONApiDocument requestGatewayPing(@PathVariable long gatewayId, @RequestParam(value = "identity", required = false) String identity) {
        if (StringUtils.isEmpty(identity) || identity == null) {
            return new JSONApiDocument(aalHouseService.sendPingCommand(gatewayId));
        }
        return new JSONApiDocument(aalHouseService.sendPingCommand(gatewayId, identity));
    }

    /**
     * Makes the request to send a resources command to reach given gateway.
     * @param gatewayId The id of the gateway
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.GATEWAYS_RESOURCES_COMMAND, method = RequestMethod.GET)
    public JSONApiDocument requestGatewayResources(@PathVariable long gatewayId) {
        return new JSONApiDocument(aalHouseService.sendResourcesCommand(gatewayId));
    }

    /**
     * Makes the request to send a device command with given operation to reach
     * given gateway.
     * @param gatewayId The id of the gateway
     * @param operation The operation attribute of the command
     * @param identity The sensor identity
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.GATEWAYS_DEVICE_COMMAND, method = RequestMethod.GET)
    public JSONApiDocument requestGatewayDevice(@PathVariable long gatewayId, 
        @RequestParam(value = "operation", required = true) String operation, 
        @RequestParam(value = "identity", required = false) String identity)
    {
        if (StringUtils.isEmpty(identity) || identity == null) {
            return new JSONApiDocument(aalHouseService.sendDeviceCommand(gatewayId, operation));
        }        
        return new JSONApiDocument(aalHouseService.sendDeviceCommand(gatewayId, operation, identity));
    }

    /**
     * Makes the request to send a resources command with given operation to reach given gateway.
     * @param gatewayId The id of the gateway
     * @param identity The sensor identity
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.GATEWAYS_BATTERY_INDICATION_COMMAND, method = RequestMethod.GET)
    public JSONApiDocument requestGatewayBattery(@PathVariable long gatewayId, @RequestParam(value = "identity", required = false) String identity) {        
        if (StringUtils.isEmpty(identity) || identity == null) {
            return new JSONApiDocument(aalHouseService.sendBatteryCommand(gatewayId));
        }        
        return new JSONApiDocument(aalHouseService.sendBatteryCommand(gatewayId, identity));       
    } 
    
    /**
     * Makes the request to send an operating system (OS) command with given
     * operation to reach given gateway.
     * @param gatewayId The id of the gateway
     * @param operation The operation attribute of the command
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.GATEWAYS_OS_COMMAND, method = RequestMethod.GET)
    public JSONApiDocument requestGatewayOperatingSystem(@PathVariable long gatewayId, @RequestParam(value = "operation", required = true) String operation) {
        return new JSONApiDocument(aalHouseService.sendOperatingSystemCommand(gatewayId, operation));
    }

    /**
     * Makes the request to send an application command with given operation to
     * reach given gateway.
     * @param gatewayId The id of the gateway
     * @param operation The operation attribute of the command
     * @return JSONApiDocument
     */
    @RequestMapping(value = ApiEndpoints.GATEWAYS_APPLICATION_COMMAND, method = RequestMethod.GET)
    public JSONApiDocument requestGatewayApplication(@PathVariable long gatewayId, @RequestParam(value = "operation", required = true) String operation) {
        return new JSONApiDocument(aalHouseService.sendApplicationCommand(gatewayId, operation));
    }

    // GATEWAY COMMANDS REQUESTS  - END
    // temporary COMMAND REQUEST MOCKUPS   
   
    @RequestMapping(value = ApiEndpoints.GATEWAYS_MOCK_COMMAND + "/ping", method = RequestMethod.POST)
    public JSONApiDocument requestGatewayPingMockup(@PathVariable long gatewayId,@RequestBody(required = false) String document) throws IOException {  
        CommandResponse cr = new CommandResponse();
        if(document != null || !StringUtils.isEmpty(document) ){
            ObjectMapper mapper = new ObjectMapper();
            Command command = mapper.readValue(document, Command.class);            
            cr = (CommandResponse) aalHouseService.sendPingCommandMockup(gatewayId, command);
        }
        else{
            cr = (CommandResponse) aalHouseService.sendPingCommandMockup(gatewayId);
        }
        JSONApiDocument document1 = new JSONApiDocument(cr);
        List<Result> results = new ArrayList<>();
        cr.getResults().forEach((i) -> results.add(i));
        document1.includeDataAsArrayList((ArrayList<?>) results);
        return document1;
    }     
    
    @RequestMapping(value = ApiEndpoints.GATEWAYS_MOCK_COMMAND + "/resources", method = RequestMethod.POST)
    public JSONApiDocument requestGatewayResourcesMockup(@PathVariable long gatewayId, @RequestBody(required = true) String document) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Command command = mapper.readValue(document, Command.class);
        MonitoringResponse mr = (MonitoringResponse) aalHouseService.sendResourcesCommandMockup(gatewayId, command);
        JSONApiDocument document1 = new JSONApiDocument(mr);
        document1.appendIncludedDataCollection(mr.getSystemMemories());
        document1.appendIncludedDataCollection(mr.getSystemPartitions());
        return document1;
    }    
    
    /**
     * Requests the dashboard information of given gateway id: resources, battery levels, last update, etc
     * @param gatewayId the id of  the gateway
     * @return {@link JSONApiDocument}
     */
    @RequestMapping(value = ApiEndpoints.GATEWAYS_DASHBOARD, method = RequestMethod.GET)
    public JSONApiDocument requestGatewayDashboard(@PathVariable long gatewayId)
    {
        // get gateway
        Gateway gateway = aalHouseService.getGateway(gatewayId);        
        JSONApiDocument document = new JSONApiDocument(gateway);
        
        // attach sensors info: battery + last ping       
        Set<Sensor> sensors = new HashSet<>();
        gateway.getGatewaySensor().stream().filter((gs) -> (gs.getStatus() == GatewaySensorStatus.ATTACHED)).forEachOrdered((gs) ->  
        {
            sensors.add(gs.getSensor());
        });
        document.appendIncludedDataCollection(sensors);
        
        //attach monitoring response from redis if any
        try
        {
            MonitoringResponse monitoringResponse = (MonitoringResponse) aalHouseService.getCommandResponse(gatewayId, "resources");
            document.appendIncludedData(monitoringResponse);
            
        } catch (IOException ex) 
        {
            java.util.logging.Logger.getLogger(HouseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return document;                
    }
    
    @RequestMapping(value = ApiEndpoints.GATEWAYS_MOCK_COMMAND + "/device", method = RequestMethod.POST)
    public JSONApiDocument requestGatewayDeviceMockup(@PathVariable long gatewayId, @RequestBody(required = false) String document) throws IOException {
        CommandResponse cr = new CommandResponse();
        if(document != null || !StringUtils.isEmpty(document) ){
            ObjectMapper mapper = new ObjectMapper();
            Command command = mapper.readValue(document, Command.class);            
            cr = (CommandResponse) aalHouseService.sendDeviceCommandMockup(gatewayId, command);
        }
        else{
            cr = (CommandResponse) aalHouseService.sendDeviceCommandMockup(gatewayId);
        }
        JSONApiDocument document1 = new JSONApiDocument(cr);
        document1.appendIncludedDataCollection(cr.getResults());
        return document1;
    }   
    
    @RequestMapping(value = ApiEndpoints.GATEWAYS_MOCK_COMMAND + "/battery", method = RequestMethod.POST)
    public JSONApiDocument requestGatewayBatteryMockup(@PathVariable long gatewayId,@RequestBody(required = false) String document) throws IOException {  
        CommandResponse cr = new CommandResponse();
        if(document != null || !StringUtils.isEmpty(document) ){
            ObjectMapper mapper = new ObjectMapper();
            Command command = mapper.readValue(document, Command.class);            
            cr = (CommandResponse) aalHouseService.sendBatteryCommandMockup(gatewayId, command);
        }
        else{
            cr = (CommandResponse) aalHouseService.sendBatteryCommandMockup(gatewayId);
        }
        JSONApiDocument document1 = new JSONApiDocument(cr);
        List<Result> results = new ArrayList<>();
        cr.getResults().forEach((i) -> results.add(i));
        document1.includeDataAsArrayList((ArrayList<?>) results);
        return document1;
    }     
    
    @RequestMapping(value = ApiEndpoints.GATEWAYS_MOCK_COMMAND + "/os", method = RequestMethod.POST)
    public JSONApiDocument requestGatewayOperatingSystemMockup(@PathVariable long gatewayId,@RequestBody(required = true) String document) throws IOException {  
        ObjectMapper mapper = new ObjectMapper();
        Command command = mapper.readValue(document, Command.class);
        MonitoringResponse mr = (MonitoringResponse) aalHouseService.sendOperatingSystemCommandMockup(gatewayId, command);
        JSONApiDocument document1 = new JSONApiDocument(mr);
        if(mr.getSystemMemories() != null && mr.getSystemPartitions() != null){
            document1.appendIncludedDataCollection(mr.getSystemMemories());
            document1.appendIncludedDataCollection(mr.getSystemPartitions());
        }
        return document1;
    }     
    
    
    @RequestMapping(value = ApiEndpoints.APPLICATIONS, method = RequestMethod.GET)
    public JSONApiDocument listApplications()
    {
        // TO DO
        return new JSONApiDocument();
    }       
    
    @RequestMapping(value = ApiEndpoints.APPLICATIONS_UPLOAD, method = RequestMethod.POST)
    public JSONApiDocument uploadApplication(@RequestBody(required = true) String document)
    {
        // TO DO
        return new JSONApiDocument();
    }     
    
    @RequestMapping(value = ApiEndpoints.APPLICATIONS_DOWNLOAD, method = RequestMethod.GET)
    public JSONApiDocument downloadApplication(@PathVariable long envId,@RequestParam(value = "version", required = false) String version)
    {
        // TO DO
        return new JSONApiDocument();
    }      
}
