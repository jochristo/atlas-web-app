package org.atlas.modules.aalhouse.repositories;


/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Core ( Customer Repository )
 * @since 2016-07-27
 **/

import java.util.List;
import org.atlas.modules.aalhouse.domain.Sensor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SensorsRepository extends JpaRepository<Sensor, Long> {	
	
	public Sensor findByName(String name);
	public Sensor findByAddress(String address);        
        
        @Query("SELECT s FROM Sensor s LEFT OUTER JOIN s.gatewaySensor gs WHERE gs.sensor.id IS NULL AND (s.status = 1 OR s.status = 2)") 
        public List<Sensor> findDetached();
        
        @Query("SELECT s FROM Sensor s LEFT OUTER JOIN s.gatewaySensor gs WHERE gs.sensor.id IS NOT NULL AND (s.status = 1 OR s.status = 2)") 
        public List<Sensor> findAttached();
 
        @Query("SELECT DISTINCT s FROM Sensor s LEFT OUTER JOIN s.gatewaySensor gs WHERE (s.status = 1 OR s.status = 2) AND ((gs.status = 2 "
                + "AND NOT EXISTS(SELECT gss.status FROM GatewaySensor gss WHERE gss.sensor = s AND gss.status = 1)) OR gs.status IS NULL)")
        public List<Sensor> findUnallocated();        
        
        
}