package org.atlas.modules.aalhouse.repositories;


/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Core ( Customer Repository )
 * @since 2016-07-27
 **/

import org.atlas.modules.aalhouse.domain.Datatype;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DatatypeRepository extends JpaRepository<Datatype, Long> {	
	
}