package org.atlas.modules.aalhouse.repositories;


/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Core ( Customer Repository )
 * @since 2016-07-27
 **/

import java.util.List;

import org.atlas.modules.aalhouse.domain.Gateway;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GatewaysRepository extends JpaRepository<Gateway, Long> {	
	
	public Gateway findByGateway(String name);
	public List<Gateway> findByAalhouse(long houseId);
	public Gateway findByIdAndAalhouse(long Id, long houseId);        
        public Gateway findByIdentity(String identity);
}