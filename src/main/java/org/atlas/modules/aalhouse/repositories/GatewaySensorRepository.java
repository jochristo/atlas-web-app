
package org.atlas.modules.aalhouse.repositories;

import java.util.List;
import org.atlas.modules.aalhouse.domain.GatewaySensor;
import org.atlas.modules.aalhouse.domain.GatewaySensorPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface GatewaySensorRepository extends JpaRepository<GatewaySensor, GatewaySensorPK>
{
    public List<GatewaySensor> findByGatewayId(long gatewayId);
    public List<GatewaySensor> findBySensorId(long sensorId);    
}