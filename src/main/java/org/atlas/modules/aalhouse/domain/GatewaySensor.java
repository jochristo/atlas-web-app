package org.atlas.modules.aalhouse.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;
import org.atlas.modules.aalhouse.domain.converters.GatewaySensorStatusConverter;

/**
 * Represents a Gateway-Sensor relationship.
 * @author ic
 */
@Entity
@Table(name="gateway_sensors")
@IdClass(GatewaySensorPK.class)
public class GatewaySensor implements Serializable
{   
    @JAPIIdentifier
    @JsonIgnore
    @Transient
    private String id;    
    
    @Id
    @ManyToOne
    @JoinColumn(name = "gateway_id")    
    @JAPIAttribute  
    private Gateway gateway;
    
    @Id
    @ManyToOne
    @JoinColumn(name = "sensor_id")   
    @JAPIAttribute      
    private Sensor sensor;    
    
    @Column(name = "created_at",insertable=true, updatable=false)
    @NotNull
    private Date createdAt;
    
    @JAPIAttribute
    @Column(name = "updated_at", insertable=false, updatable=true)
    private Date updatedAt;     
     
    @JAPIAttribute
    @Convert(converter = GatewaySensorStatusConverter.class)
    private GatewaySensorStatus status;  

    public GatewaySensor() {
        id = UUID.randomUUID().toString();
    }
        
    public String getId() {        
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }    
    
    public Gateway getGateway() {
        return gateway;
    }

    public void setGateway(Gateway gateway) {
        this.gateway = gateway;
    }

    public Sensor getSensor() {
        return sensor;
    }

    public void setSensor(Sensor sensor) {
        this.sensor = sensor;
    }
	
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
    
    public GatewaySensorStatus getStatus() {
        return status;
    }

    public void setStatus(GatewaySensorStatus status) {
        this.status = status;
    }
    
    
    
}
