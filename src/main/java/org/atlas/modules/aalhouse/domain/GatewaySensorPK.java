/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.modules.aalhouse.domain;

import java.io.Serializable;

/**
 * Represents a composite primary key of Gateway-Sensor relationship.
 * @author ic
 */
public class GatewaySensorPK implements Serializable
{    
    private long gateway;  
    private long sensor;    

    public long getGateway() {
        return gateway;
    }

    public void setGateway(long gateway) {
        this.gateway = gateway;
    }

    public long getSensor() {
        return sensor;
    }

    public void setSensor(long sensor) {
        this.sensor = sensor;
    }

    public GatewaySensorPK(long gatewayId, long sensorId) {
        this.gateway = gatewayId;
        this.sensor = sensorId;
    }

    public GatewaySensorPK() {
    }    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + (int) (this.gateway ^ (this.gateway >>> 32));
        hash = 61 * hash + (int) (this.sensor ^ (this.sensor >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GatewaySensorPK other = (GatewaySensorPK) obj;
        return this.gateway != other.gateway && this.sensor != other.sensor;
    }
    
    
}