package org.atlas.modules.aalhouse.domain.converters;


/** 
 * Wireless Connection converter 
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System modules ( Core )
 * @since 2016-08-01
 **/

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.atlas.modules.aalhouse.domain.WirelessConnection;

@Converter
public class WirelessConnectionConverter implements AttributeConverter<WirelessConnection, Integer>{

	@Override
	public Integer convertToDatabaseColumn(WirelessConnection attribute) {
		switch (attribute) {
	        case CC2420:
	            return 1;
	        case ZIGBEE:
	        	return 2;
	        case BLUETOOTH:
	        	return 3;
	        case BLE:
	        	return 4;
	        case ZWAVE:
	        	return 5;
	        case LORAWAN:
	        	return 6;
	        case WIFI:
	        	return 7;
	        default:
	            throw new IllegalArgumentException("Unknown" + attribute);
	    }
	}

	@Override
	public WirelessConnection convertToEntityAttribute(Integer dbData) {
		switch (dbData) {
		 	case 1:
	            return WirelessConnection.CC2420;
		 	case 2:
	            return WirelessConnection.ZIGBEE;
		 	case 3:
	            return WirelessConnection.BLUETOOTH;
		 	case 4:
	            return WirelessConnection.BLE;
		 	case 5:
	            return WirelessConnection.ZWAVE;
		 	case 6:
	            return WirelessConnection.LORAWAN;
		 	case 7:
		 		return WirelessConnection.WIFI;
	        default:
	            throw new IllegalArgumentException("Unknown" + dbData);
	    }
	}

}
