package org.atlas.modules.aalhouse.domain.converters;


/** 
 * Wireless Connection converter 
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System modules ( Core )
 * @since 2016-08-01
 **/

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.atlas.modules.aalhouse.domain.ManufacturerStatus;

@Converter
public class ManufacturerStatusConverter implements AttributeConverter<ManufacturerStatus, Integer>{

	@Override
	public Integer convertToDatabaseColumn(ManufacturerStatus attribute) {
		switch (attribute) {
	        case ENABLED:
	            return 1;
	        case DISABLED:
	        	return 2;
	        case DELETED:
	        	return 3;
	        default:
	            throw new IllegalArgumentException("Unknown" + attribute);
	    }
	}

	@Override
	public ManufacturerStatus convertToEntityAttribute(Integer dbData) {
		switch (dbData) {
		 	case 1:
	            return ManufacturerStatus.ENABLED;
		 	case 2:
	            return ManufacturerStatus.DISABLED;
		 	case 3:
	            return ManufacturerStatus.DELETED;
	        default:
	            throw new IllegalArgumentException("Unknown" + dbData);
	    }
	}

}
