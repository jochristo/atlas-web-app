package org.atlas.modules.aalhouse.domain;

/**
 * Gateway Status
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System modules ( Gateway Status )
 * @since 2016-07-31
 **/
//TODO http://stackoverflow.com/questions/1414755/can-enums-be-subclassed-to-add-new-elements
public enum GatewayStatus{
	
	/**
	 * Gateway is enabled and can be used in a AALHouse
	 */
	ENABLED,
	
	INSTALLED,	
	
	DISABLED,
	
	DELETED;

}
