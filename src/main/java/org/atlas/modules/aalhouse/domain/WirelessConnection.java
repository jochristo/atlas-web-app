package org.atlas.modules.aalhouse.domain;

public enum WirelessConnection {
	
	CC2420,ZIGBEE,BLUETOOTH,BLE,ZWAVE,LORAWAN,WIFI

}
