package org.atlas.modules.aalhouse.domain;

/**
 * Manufacturer Status
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System modules ( Manufacturer Status )
 * @since 2016-07-31
 **/
public enum ManufacturerStatus{
	
	ENABLED,	
	
	DISABLED,
	
	DELETED;

}
