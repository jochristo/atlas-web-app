package org.atlas.modules.aalhouse.domain.converters;


/** 
 * Status converter 
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System modules ( Core )
 * @since 2016-08-01
 **/

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.atlas.modules.aalhouse.domain.AALHouseStatus;

@Converter
public class AALHouseStatusConverter implements AttributeConverter<AALHouseStatus, Integer>{

	@Override
	public Integer convertToDatabaseColumn(AALHouseStatus attribute) {
		switch (attribute) {
	        case ENABLED:
	            return 1;
	        case DISABLED:
	        	return 2;
	        case DELETED:
	        	return 3;
	        default:
	            throw new IllegalArgumentException("Unknown" + attribute);
	    }
	}

	@Override
	public AALHouseStatus convertToEntityAttribute(Integer dbData) {
		switch (dbData) {
		 	case 1:
	            return AALHouseStatus.ENABLED;
		 	case 2:
	            return AALHouseStatus.DISABLED;
		 	case 3:
	            return AALHouseStatus.DELETED;
	        default:
	            throw new IllegalArgumentException("Unknown" + dbData);
	    }
	}

}
