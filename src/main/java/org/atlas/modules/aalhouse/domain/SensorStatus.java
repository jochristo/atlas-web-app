package org.atlas.modules.aalhouse.domain;

/**
 * Sensor Status
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System modules ( Sensor Status )
 * @since 2016-07-31
 **/
//TODO http://stackoverflow.com/questions/1414755/can-enums-be-subclassed-to-add-new-elements
public enum SensorStatus{
	
	ENABLED,
	
	INSTALLED,	
	
	DISABLED,
	
	DELETED;

}
