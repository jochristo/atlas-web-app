/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.modules.aalhouse.domain;

import java.io.Serializable;

/**
 *
 * @author ic
 */
//@Embeddable
public class GatewaySensorsPK implements Serializable
{    
    private long gatewayId;  
    private long sensorId;    

    public long getGatewayId() {
        return gatewayId;
    }

    public void setGatewayId(long gateway) {
        this.gatewayId = gateway;
    }

    public long getSensorId() {
        return sensorId;
    }

    public void setSensorId(long sensor) {
        this.sensorId = sensor;
    }

    public GatewaySensorsPK(long gatewayId, long sensorId) {
        this.gatewayId = gatewayId;
        this.sensorId = sensorId;
    }

    public GatewaySensorsPK() {
    }    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + (int) (this.gatewayId ^ (this.gatewayId >>> 32));
        hash = 61 * hash + (int) (this.sensorId ^ (this.sensorId >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GatewaySensorsPK other = (GatewaySensorsPK) obj;
        return this.gatewayId != other.gatewayId && this.sensorId != other.sensorId;
    }
    
    
}
