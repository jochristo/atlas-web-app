package org.atlas.modules.aalhouse.domain;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Module ( Customer Entity )
 * @since 2016-07-27
 **/

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;
import org.atlas.modules.aalhouse.domain.converters.AALHouseStatusConverter;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="aalhouses")
public class AALHouse{

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JAPIIdentifier
    @JsonIgnore
    private long id;
    
    @JAPIAttribute
    @NotNull
    private String name;

    @JAPIAttribute
    @NotNull
	private String address;
    
    @Column(name = "created_at",insertable=true, updatable=false)
	private Date createdAt;
    
    @JAPIAttribute
    @Column(name = "updated_at", insertable=false, updatable=true)
   	private Date updatedAt;    
    
    @JAPIAttribute
    @NotNull
    private String location;
    
    @JAPIAttribute
    @NotNull
    private int square_meters;
    
    @JAPIAttribute
    @NotNull
    private int total_rooms;
    
    @JAPIAttribute
    @Convert(converter = AALHouseStatusConverter.class)
    private AALHouseStatus status;       

	public AALHouse() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getSquare_meters() {
		return square_meters;
	}

	public void setSquare_meters(int square_meters) {
		this.square_meters = square_meters;
	}

	public int getTotal_rooms() {
		return total_rooms;
	}

	public void setTotal_rooms(int total_rooms) {
		this.total_rooms = total_rooms;
	}

	public AALHouseStatus getStatus() {
		return status;
	}

	public void setStatus(AALHouseStatus status) {
		this.status = status;
	}
}