
package org.atlas.modules.aalhouse.domain.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import org.atlas.modules.aalhouse.domain.GatewaySensorStatus;

/**
 * Converter class for gateway-sensor pair status: ATTACHED/DETACHED.
 * @author ic
 */
@Converter
public class GatewaySensorStatusConverter implements AttributeConverter<GatewaySensorStatus, Integer>{

    @Override
    public Integer convertToDatabaseColumn(GatewaySensorStatus attribute) {
		switch (attribute) {
	        case ATTACHED:   // ATTACHED
	            return 1;
	        case DETACHED:  // DETACHED
	        	return 2;
	        default:
	            throw new IllegalArgumentException("Unknown" + attribute);
	    }
    }

    @Override
    public GatewaySensorStatus convertToEntityAttribute(Integer dbData) {
		switch (dbData) {
		 	case 1:
	            return GatewaySensorStatus.ATTACHED;  // ATTACHED
		 	case 2:
	            return GatewaySensorStatus.DETACHED; // DETACHED
	        default:
	            throw new IllegalArgumentException("Unknown" + dbData);
	    }
    }




}