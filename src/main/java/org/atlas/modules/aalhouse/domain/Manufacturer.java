package org.atlas.modules.aalhouse.domain;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Module ( Customer Entity )
 * @since 2016-07-27
 **/

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;
import org.atlas.modules.aalhouse.domain.converters.ManufacturerStatusConverter;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="manufacturers")
public class Manufacturer{

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JAPIIdentifier
    @JsonIgnore
    private long id;
    
    @JAPIAttribute
    @NotNull
    private String name;

    @JAPIAttribute
    @Column(name = "created_at",insertable=true, updatable=false)
	private Date createdAt;
    
    @JAPIAttribute
    @Column(name = "updated_at", insertable=false, updatable=true)
   	private Date updatedAt;    
    
    @JAPIAttribute
    @Convert(converter = ManufacturerStatusConverter.class)
    private ManufacturerStatus status;  

	public Manufacturer() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public ManufacturerStatus getStatus() {
		return status;
	}

	public void setStatus(ManufacturerStatus status) {
		this.status = status;
	}	
}