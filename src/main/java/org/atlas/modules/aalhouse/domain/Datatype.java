package org.atlas.modules.aalhouse.domain;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Module ( Customer Entity )
 * @since 2016-07-27
 **/

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="datatypes")
public class Datatype{

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JAPIIdentifier
    @JsonIgnore
    private long id;
    
    @JAPIAttribute
    @NotNull
    @Column(unique=true)
    private String datatype;    
    
    @JAPIAttribute
    private String identifier;
    
    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Datatype(){}
    
    public Datatype(long id, String datatype){
    	this.id = id;
    	this.datatype = datatype;
    }
    
    @Transient
    private boolean enabled;
    
    public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

    @ManyToMany(mappedBy = "datatypes")
    private Set<Sensor> sensors;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDatatype() {
		return datatype;
	}

	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}
    
	public Set<Sensor> getSensors() {
		return sensors;
	}

	public void setSensors(Set<Sensor> sensors) {
		this.sensors = sensors;
	}
    
}