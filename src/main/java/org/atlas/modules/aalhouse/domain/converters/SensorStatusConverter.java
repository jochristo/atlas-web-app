package org.atlas.modules.aalhouse.domain.converters;


/** 
 * Gateway converter 
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System modules ( Core )
 * @since 2016-08-01
 **/

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.atlas.modules.aalhouse.domain.SensorStatus;

@Converter
public class SensorStatusConverter implements AttributeConverter<SensorStatus, Integer>{

	@Override
	public Integer convertToDatabaseColumn(SensorStatus attribute) {
		switch (attribute) {
	        case ENABLED:
	            return 1;
	        case INSTALLED:
	        	return 2;
	        case DISABLED:
	        	return 3;
	        case DELETED:
	        	return 4;
	        default:
	            throw new IllegalArgumentException("Unknown" + attribute);
	    }
	}

	@Override
	public SensorStatus convertToEntityAttribute(Integer dbData) {
		switch (dbData) {
		 	case 1:
	            return SensorStatus.ENABLED;
		 	case 2:
	            return SensorStatus.INSTALLED;
		 	case 3:
	            return SensorStatus.DISABLED;
		 	case 4:
	            return SensorStatus.DELETED;
	        default:
	            throw new IllegalArgumentException("Unknown" + dbData);
	    }
	}

}
