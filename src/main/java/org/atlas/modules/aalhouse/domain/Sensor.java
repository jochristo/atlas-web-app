package org.atlas.modules.aalhouse.domain;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Module ( Customer Entity )
 * @since 2016-07-27
 **/

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;
import org.atlas.modules.aalhouse.domain.converters.SensorStatusConverter;
import org.atlas.modules.aalhouse.domain.converters.WirelessConnectionConverter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Objects;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
@Table(name="sensors", uniqueConstraints= @UniqueConstraint(columnNames={"address"}))
public class Sensor{

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JAPIIdentifier
    @JsonIgnore
    private long id;
    
    @JAPIAttribute
    @NotNull
    private String name;
    
    @JAPIAttribute
    @NotNull
    @Column(name = "manufacturer_id")
    private long manufacturer;

    @JAPIAttribute
    @NotNull
    @Column(unique=true)
    private String address;
    
    @Column(name = "created_at",insertable=true, updatable=false)
    private Date createdAt;
    
    @JAPIAttribute
    @Column(name = "updated_at", insertable=false, updatable=true)
    private Date updatedAt;    
    
    @JAPIAttribute
    @Convert(converter = SensorStatusConverter.class)
    private SensorStatus status;  
    
    @JAPIAttribute
    @Convert(converter = WirelessConnectionConverter.class)
    @Column(name = "wireless_connection")
    private WirelessConnection connection;
    
    @JAPIAttribute
    private boolean connectable;
    
    @ManyToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinTable(name = "sensor_datatypes", 
    	joinColumns = @JoinColumn(name = "sensor_id", referencedColumnName = "id"), 
    	inverseJoinColumns = @JoinColumn(name = "datatype_id", referencedColumnName = "id")
    )
    private Set<Datatype> datatypes;
    
    @JAPIAttribute    
    @NotNull
    private int battery;     
    
    @OneToMany(mappedBy = "sensor")        
    private Set<GatewaySensor> gatewaySensor;    

    public Set<GatewaySensor> getGatewaySensor() {
        return gatewaySensor;
    }

    public void setGatewaySensor(Set<GatewaySensor> gatewaySensor) {
        this.gatewaySensor = gatewaySensor;
    }
    
    
    @JAPIAttribute
    @Column(name = "last_ping", insertable=false, updatable=true)
    private Date lastPing; 
        
    @Transient
    private boolean enabled;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }    
    
    public Date getLastPing() {
        return lastPing;
    }

    public void setLastPing(Date lastPing) {
        this.lastPing = lastPing;
    }      
    
    public Sensor() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public SensorStatus getStatus() {
        return status;
    }

    public void setStatus(SensorStatus status) {
        this.status = status;
    }

    public boolean isConnectable() {
        return connectable;
    }

    public void setConnectable(boolean connectable) {
        this.connectable = connectable;
    }

    public Set<Datatype> getDatatypes() {
        return datatypes;
    }

    public void setDatatypes(Set<Datatype> datatypes) {
        this.datatypes = datatypes;
    }

    public WirelessConnection getConnection() {
        return connection;
    }

    public void setConnection(WirelessConnection connection) {
        this.connection = connection;
    }

    public long getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(long manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getBattery() {
        return battery;
    }

    public void setBattery(int battery) {
        this.battery = battery;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 67 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sensor other = (Sensor) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
    
        
}