/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.modules.aalhouse.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;

/**
 *
 * @author ic
 */
@Entity
@Table(name="gateway_sensors")
@IdClass(GatewaySensorsPK.class)
public class GatewaySensors
{ 
    @Id   
    @Column(name="gateway_id")
    private long gatewayId;
    
    @Id 
    @Column(name="sensor_id")
    @JAPIIdentifier
    private long sensorId;    
    
    @ManyToOne    
    @JoinColumn(name = "gateway_id", updatable = false, insertable = false, referencedColumnName = "id", foreignKey = @ForeignKey(name = "GatewayFK"))   
    @JAPIAttribute    
    //@JsonManagedReference
    private Gateway gateway;    
    
    @ManyToOne
    @JoinColumn(name = "sensor_id", updatable = false, insertable = false, referencedColumnName = "id", foreignKey = @ForeignKey(name = "SensorFK"))     
    @JAPIAttribute    
    //@JsonManagedReference
    private Sensor sensor;       
   
    @JAPIAttribute
    @NotNull
    private String location;
    
    @Column(name = "created_at",insertable=true, updatable=false)
    @NotNull
    private Date createdAt;
    
    @JAPIAttribute
    @Column(name = "updated_at", insertable=false, updatable=true)
    @NotNull
    private Date updatedAt; 
    
    @JAPIAttribute
    @Column(name = "last_ping", insertable=false, updatable=true)
    @NotNull
    private Date lastPing;    

    public Gateway getGateway() {
        return gateway;
    }

    public void setGateway(Gateway gateway) {
        this.gateway = gateway;
    }

    public Sensor getSensor() {
        return sensor;
    }

    public void setSensor(Sensor sensor) {
        this.sensor = sensor;
    }    
    
    public long getGatewayId() {
        return gatewayId;
    }

    public void setGatewayId(long gatewayId) {
        this.gatewayId = gatewayId;
    }

    public long getSensorId() {
        return sensorId;
    }

    public void setSensorId(long sensorId) {
        this.sensorId = sensorId;
    }       

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getLastPing() {
        return lastPing;
    }

    public void setLastPing(Date lastPing) {
        this.lastPing = lastPing;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + (int) (this.gatewayId ^ (this.gatewayId >>> 32));
        hash = 17 * hash + (int) (this.sensorId ^ (this.sensorId >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GatewaySensors other = (GatewaySensors) obj;
        return this.sensorId == other.sensorId && this.gatewayId == other.gatewayId;
    }
    
    
    
}
