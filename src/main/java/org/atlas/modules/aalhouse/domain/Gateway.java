package org.atlas.modules.aalhouse.domain;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Module ( Customer Entity )
 * @since 2016-07-27
 **/

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;
import org.atlas.modules.aalhouse.domain.converters.GatewayStatusConverter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Set;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name="gateways", uniqueConstraints= @UniqueConstraint(columnNames={"gateway"}))
public class Gateway{

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JAPIIdentifier
    @JsonIgnore
    private long id;
    
    @JAPIAttribute
    @NotNull
    @Column(unique=true)
    private String gateway;

    @JAPIAttribute
    @Column(name = "aalhouse_id")
    private long aalhouse;
    
    @Column(name = "created_at",insertable=true, updatable=false)
    private Date createdAt;
    
    @JAPIAttribute
    @Column(name = "updated_at", insertable=false, updatable=true)
    private Date updatedAt;    
    
    @JAPIAttribute
    private String token;
    
    @JAPIAttribute
    @Convert(converter = GatewayStatusConverter.class)
    private GatewayStatus status;       
    
    @OneToMany(mappedBy = "gateway", fetch = FetchType.EAGER)      
    private Set<GatewaySensor> gatewaySensor;    

    public Set<GatewaySensor> getGatewaySensor() {
        return gatewaySensor;
    }

    public void setGatewaySensor(Set<GatewaySensor> gatewaySensor) {
        this.gatewaySensor = gatewaySensor;
    }       

    @JAPIAttribute
    @NotNull
    @Column(unique=true)    
    @Size(min=5,max=20)
    @Pattern(regexp="^[A-Za-z0-9-_]+$")
    private String identity;    

    @Transient
    private Set<Sensor> sensors;
    public Set<Sensor> getSensors() {
        return sensors;
    }

    public void setSensors(Set<Sensor> sensors) {
        this.sensors = sensors;
    }
     
    public Gateway() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public long getAalhouse() {
        return aalhouse;
    }

    public void setAalhouse(long aalhouse) {
        this.aalhouse = aalhouse;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public GatewayStatus getStatus() {
        return status;
    }

    public void setStatus(GatewayStatus status) {
        this.status = status;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }
        
        
}