package org.atlas.modules.applications;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.atlas.api.services.ThingsObservationsService;
import org.atlas.modules.applications.domain.Observation;
import org.atlas.modules.applications.domain.Thing;
import org.atlas.modules.applications.repository.ObservationsRepository;
import org.atlas.modules.applications.repository.ThingsRepository;
import org.springframework.stereotype.Service;

@Service
public class ThingsObservarvationsServiceImpl implements ThingsObservationsService{

	@Resource
	private ThingsRepository thingsRepo;
	
	@Resource
	private ObservationsRepository observsRepo;
	
	@Override
	public Thing getThing(String name) {
		return this.thingsRepo.findByName(name);
	}

	@Override
	public List<Observation> getObservations() {
		return this.observsRepo.getLastObservations();
	}

	@Override
	public Thing addThing(Thing thing) {
		Thing thng = new Thing(thing.getName());
		thng.setCreatedAt(new Date());
		thng.setSensorId(thing.getSensorId());
		System.out.println(thng);
		return this.thingsRepo.save(thng);
	}

	@Override
	public List<Thing> getThings() {
		return this.thingsRepo.findAll();
	}

	@Override
	public Observation addObservation(Observation observation) {
		return this.observsRepo.save(observation);
	}

	@Override
	public Thing getThing(long id) {
		return this.thingsRepo.findOne(id);
	}

	@Override
	public Observation getObservation(long id) {
		return this.observsRepo.findOne(id);
	}

}
