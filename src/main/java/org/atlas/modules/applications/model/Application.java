package org.atlas.modules.applications.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Represents a gateway application DTO instance.
 * @author ic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"envelope",
"appname",
"mime_type",
"main",
"version",
"checksum"
})
public class Application {

@JsonProperty("envelope")
private String envelope;
@JsonProperty("appname")
private String appname;
@JsonProperty("mime_type")
private String mimeType;
@JsonProperty("main")
private String main;
@JsonProperty("version")
private String version;
@JsonProperty("checksum")
private String checksum;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("envelope")
public String getEnvelope() {
return envelope;
}

@JsonProperty("envelope")
public void setEnvelope(String envelope) {
this.envelope = envelope;
}

@JsonProperty("appname")
public String getAppname() {
return appname;
}

@JsonProperty("appname")
public void setAppname(String appname) {
this.appname = appname;
}

@JsonProperty("mime_type")
public String getMimeType() {
return mimeType;
}

@JsonProperty("mime_type")
public void setMimeType(String mimeType) {
this.mimeType = mimeType;
}

@JsonProperty("main")
public String getMain() {
return main;
}

@JsonProperty("main")
public void setMain(String main) {
this.main = main;
}

@JsonProperty("version")
public String getVersion() {
return version;
}

@JsonProperty("version")
public void setVersion(String version) {
this.version = version;
}

@JsonProperty("checksum")
public String getChecksum() {
return checksum;
}

@JsonProperty("checksum")
public void setChecksum(String checksum) {
this.checksum = checksum;
}


@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}