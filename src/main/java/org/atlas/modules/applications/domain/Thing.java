package org.atlas.modules.applications.domain;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Module ( Things Entity )
 * @since 2017-03-07
 **/

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name="room_things", uniqueConstraints= @UniqueConstraint(columnNames={"name"}))
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Thing{

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JAPIIdentifier
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private long id;
    
    @JAPIAttribute
    @NotNull
    @Size(min=2,max=255)
    @Column(unique=true)
    private String name;

    @Column(name = "created_at",insertable=true, updatable=false)
	private Date createdAt;
    
    @Column(name = "updated_at", insertable=false, updatable=true)
   	private Date updatedAt; 
    
    @JAPIAttribute
    @Column(name = "sensor_id")
    private long sensorId;
    
    @OneToMany(mappedBy="thing")
    @JsonIgnore
    private List<Observation> observations;
    
	public Thing() {}
	
	public Thing(String name){
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public List<Observation> getObservations() {
		return observations;
	}

	public void setObservations(List<Observation> observations) {
		this.observations = observations;
	}

	public long getSensorId() {
		return sensorId;
	}

	public void setSensorId(long sensorId) {
		this.sensorId = sensorId;
	}

	@Override
	public String toString() {
		return "Thing [id=" + id + ", name=" + name + ", createdAt="
				+ createdAt + ", updatedAt=" + updatedAt + ", sensorId="
				+ sensorId + ", observations=" + observations + "]";
	}


}