package org.atlas.modules.applications.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;

/**
 *
 * @author ic
 */
@Entity
@Table(name="mime_types")
public class MimeType
{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JAPIIdentifier
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private long id;

    @Column(name = "mime", columnDefinition="varchar(255)")
    @NotNull    
    @JAPIAttribute
    private String mime; 
    
    @Column(name = "extension", columnDefinition="varchar(255)")
    @NotNull    
    @JAPIAttribute
    private String extension; 

    @Column(name = "encoding", columnDefinition="varchar(255)")
    @NotNull    
    @JAPIAttribute
    private String encoding; 
    
    @OneToMany(mappedBy="mimeType")
    @JsonIgnore
    private List<EnvelopeContent> envelopeContents;    

    public MimeType() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public List<EnvelopeContent> getEnvelopeContents() {
        return envelopeContents;
    }

    public void setEnvelopeContents(List<EnvelopeContent> envelopeContents) {
        this.envelopeContents = envelopeContents;
    }    
    
}
