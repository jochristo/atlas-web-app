package org.atlas.modules.applications.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;
import org.atlas.modules.applications.domain.converters.EnvelopeStatusConverter;

/**
 * Represents an application's envelope entity.
 * @author ic
 */
@Entity
@Table(name="envelopes")
public class Envelope
{    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JAPIIdentifier
    @JsonIgnore
    private long id;

    @Column(name = "createdBy")
    @NotNull
    @Size(max=11)
    @JAPIAttribute
    private long createdBy;    
    
    @Column(name = "name", columnDefinition="varchar(255)")
    @NotNull    
    @JAPIAttribute
    private String name;    
    
    @Column(name = "checksum",columnDefinition="char(64)")
    @NotNull    
    @JAPIAttribute
    private String checksum;  
    
    @Column(name = "createdAt",insertable=true, updatable=false)
    private Date createdAt;
    
    @Column(name = "updatedAt", insertable=false, updatable=true)
    @JAPIAttribute
    private Date updatedAt; 

    @Column(name = "isCompressed", columnDefinition="tinyint(1)")    
    @Size(max=1)
    @JAPIAttribute
    private int isCompressed;    
    
    @Column(name = "hash",columnDefinition="char(64)")
    @NotNull
    @Size(max=64)
    @JAPIAttribute
    private String hash;  
    
    @JAPIAttribute
    @Convert(converter = EnvelopeStatusConverter.class)
    private EnvelopeStatus status;  
    
    @OneToMany(mappedBy="envelope")
    @JsonIgnore
    private List<EnvelopeContent> envelopeContents;     
    
    @OneToMany(mappedBy="envelope")
    @JsonIgnore
    private List<EnvelopeTransaction> envelopeTransactions;    

    public Envelope() {
    }
        
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(long createdBy) {
        this.createdBy = createdBy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getIsCompressed() {
        return isCompressed;
    }

    public void setIsCompressed(int isCompressed) {
        this.isCompressed = isCompressed;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public EnvelopeStatus getStatus() {
        return status;
    }

    public void setStatus(EnvelopeStatus status) {
        this.status = status;
    }

    public List<EnvelopeContent> getEnvelopeContents() {
        return envelopeContents;
    }

    public void setEnvelopeContents(List<EnvelopeContent> envelopeContents) {
        this.envelopeContents = envelopeContents;
    }

    public List<EnvelopeTransaction> getEnvelopeTransactions() {
        return envelopeTransactions;
    }

    public void setEnvelopeTransactions(List<EnvelopeTransaction> envelopeTransactions) {
        this.envelopeTransactions = envelopeTransactions;
    }
    
    
    
}
