package org.atlas.modules.applications.domain;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Module ( Things Entity )
 * @since 2017-03-07
 **/

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="room_things_observations")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Observation{

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JAPIIdentifier
    @JsonIgnore
    private long id;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="room_thing_id")
    @JAPIAttribute
    private Thing thing;

    @Column(name = "created_at",insertable=true, updatable=false)
    @JAPIAttribute
	private Date createdAt;
    
    @Column(name = "updated_at", insertable=false, updatable=true)
   	private Date updatedAt;    
    
    @Column(name="vicinity_things")
    private String vicinityThings;
   
	public Observation() {}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Thing getThing() {
		return thing;
	}

	public void setThing(Thing thing) {
		this.thing = thing;
	}

	public String getVicinityThings() {
		return vicinityThings;
	}

	public void setVicinityThings(String vicinityThings) {
		this.vicinityThings = vicinityThings;
	}


}