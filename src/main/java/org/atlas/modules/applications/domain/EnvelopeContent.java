package org.atlas.modules.applications.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;
import org.atlas.modules.applications.domain.converters.EnvelopeStatusConverter;

/**
 * Represents an application's envelope contents entity.
 * @author ic
 */
@Entity
@Table(name="envelope_contents")
public class EnvelopeContent
{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JAPIIdentifier
    @JsonIgnore
    private long id;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="envelopeId",columnDefinition="int(11)")
    @JAPIAttribute 
    private Envelope envelope;  
        
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="mimeTypeId",columnDefinition="int(11)")
    @JAPIAttribute    
    private MimeType mimeType;      
    
    @Column(name = "filename", columnDefinition="varchar(255)")
    @NotNull    
    @JAPIAttribute
    private String filename;    
    
    @Column(name = "storedname", columnDefinition="varchar(255)")
    @NotNull    
    @JAPIAttribute
    private String storedname;     
    
    @Column(name = "version", columnDefinition="varchar(2)")
    @NotNull    
    @JAPIAttribute
    private String version;      

    @Column(name = "isExecutable", columnDefinition="tinyint(1)")    
    @Size(max=1)
    @JAPIAttribute
    private int isExecutable;    
    
    @Column(name = "size", columnDefinition="bigint(20)")    
    @Size(max=20)
    @JAPIAttribute
    private long size;
    
    @JAPIAttribute
    @Convert(converter = EnvelopeStatusConverter.class)
    private EnvelopeStatus status;  

    public EnvelopeContent() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Envelope getEnvelope() {
        return envelope;
    }

    public void setEnvelope(Envelope envelope) {
        this.envelope = envelope;
    }    

    public MimeType getMimeType() {
        return mimeType;
    }

    public void setMimeType(MimeType mimeType) {
        this.mimeType = mimeType;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getStoredname() {
        return storedname;
    }

    public void setStoredname(String storedname) {
        this.storedname = storedname;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getIsExecutable() {
        return isExecutable;
    }

    public void setIsExecutable(int isExecutable) {
        this.isExecutable = isExecutable;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public EnvelopeStatus getStatus() {
        return status;
    }

    public void setStatus(EnvelopeStatus status) {
        this.status = status;
    }
    
    
    
}
