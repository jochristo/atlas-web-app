package org.atlas.modules.applications.domain.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import org.atlas.modules.applications.domain.EnvelopeStatus;

/**
 * Converted for envelope-related entity status in db.
 * @author ic
 */
@Converter
public class EnvelopeStatusConverter implements AttributeConverter<EnvelopeStatus, Integer>
{

    @Override
    public Integer convertToDatabaseColumn(EnvelopeStatus x) {
		switch (x) {
	        case ENABLED:
	            return 1;
	        case DISABLED:
	            return 2;
	        case DELETED:
	            return 3;
	        default:
	            throw new IllegalArgumentException("Unknown" + x);
	    }
    }

    @Override
    public EnvelopeStatus convertToEntityAttribute(Integer y) {
		switch (y) {
	        case 1:
	            return EnvelopeStatus.ENABLED;
	        case 2:
	            return EnvelopeStatus.DISABLED;
	        case 3:
	            return EnvelopeStatus.DELETED;
	        default:
	            throw new IllegalArgumentException("Unknown" + y);
	    }
    }
    
}
