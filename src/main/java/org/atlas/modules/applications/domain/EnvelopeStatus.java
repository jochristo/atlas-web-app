package org.atlas.modules.applications.domain;

/**
 * 
 * @author ic
 */
public enum EnvelopeStatus
{
	/**
         * value: 1
         */
	ENABLED,		
	
	/**
         * value: 2
         */        
	DISABLED,
	
	/**
         * value: 3
         */        
	DELETED;    
    
}
