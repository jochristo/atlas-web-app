package org.atlas.modules.applications.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;

/**
 * Represents an application's envelope transaction entity.
 * @author ic
 */
@Entity
@Table(name="envelope_transactions")
public class EnvelopeTransaction
{    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JAPIIdentifier
    @JsonIgnore
    private long id;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="envelopeId",columnDefinition="int(11)")
    @JAPIAttribute 
    private Envelope envelope; 
    
    @Column(name = "transactionBy")
    @NotNull
    @Size(max=11)
    @JAPIAttribute
    private long transactionBy;     
    
    @Column(name = "createdAt",insertable=true, updatable=false)
    private Date createdAt;   

    @Column(name = "type", columnDefinition="tinyint(2)")    
    @Size(max=1)
    @JAPIAttribute
    private int type;    
    
    @Column(name = "info",columnDefinition="varchar(2500)")
    @NotNull
    @Size(max=2500)
    @JAPIAttribute
    private String info;     

    public EnvelopeTransaction() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Envelope getEnvelope() {
        return envelope;
    }

    public void setEnvelope(Envelope envelope) {
        this.envelope = envelope;
    }    

    public long getTransactionBy() {
        return transactionBy;
    }

    public void setTransactionBy(long transactionBy) {
        this.transactionBy = transactionBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
    
    
}
