package org.atlas.modules.applications;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import javax.annotation.Resource;
import org.atlas.api.services.ApplicationRepositoryService;
import org.atlas.modules.applications.domain.Envelope;
import org.atlas.modules.applications.domain.EnvelopeContent;
import org.atlas.modules.applications.domain.EnvelopeStatus;
import org.atlas.modules.applications.domain.EnvelopeTransaction;
import org.atlas.modules.applications.domain.MimeType;
import org.atlas.modules.applications.repository.EnvelopeContentRepository;
import org.atlas.modules.applications.repository.EnvelopeRepository;
import org.atlas.modules.applications.repository.EnvelopeTransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Application repository service class to handle application entities: Envelopes, Transactions, and Contents.
 * @author ic
 */
@Service
public class ApplicationRepositoryServiceImpl implements ApplicationRepositoryService
{    
    private ObjectMapper jsonMapper = new ObjectMapper();
    private static final Logger logger = LoggerFactory.getLogger(ApplicationRepositoryServiceImpl.class);

    @Resource
    private EnvelopeRepository envelopeRepository;    
    
    @Resource
    private EnvelopeContentRepository envelopeContentRepository;    
    
    @Resource
    private EnvelopeTransactionRepository envelopeTransactionRepository;    

    @Override
    public Envelope createEnvelope(Envelope envelope) {
        Envelope env = new Envelope();
        env.setChecksum(envelope.getChecksum());        
        env.setHash(envelope.getHash());
        env.setName(envelope.getName());
        env.setIsCompressed(envelope.getIsCompressed());
        env.setStatus(envelope.getStatus());
        env.setUpdatedAt(new java.util.Date());        
        return envelopeRepository.save(env);
    }

    @Override
    public Envelope updateEnvelope(Envelope envelope) {
        return envelopeRepository.save(envelope);
    }

    @Override
    public Envelope updateEnvelope(long id, Envelope envelope) {
        Envelope env = envelopeRepository.findOne(id);
        env.setChecksum(envelope.getChecksum());
        env.setEnvelopeContents(envelope.getEnvelopeContents());
        env.setEnvelopeTransactions(envelope.getEnvelopeTransactions());
        env.setHash(envelope.getHash());
        env.setName(envelope.getName());
        env.setIsCompressed(envelope.getIsCompressed());
        env.setStatus(envelope.getStatus());
        env.setUpdatedAt(new java.util.Date());
        return envelopeRepository.save(env);        
    }

    @Override
    public Envelope deleteEnvelope(long id) {
        Envelope env = envelopeRepository.findOne(id);
        env.setStatus(EnvelopeStatus.DELETED);
        env.setUpdatedAt(new java.util.Date());
        return envelopeRepository.save(env);
    }

    @Override
    public List<Envelope> getEnvelopes() {
        return envelopeRepository.findAll();
    }

    @Override
    public Envelope getEnvelope(String name) {
        return envelopeRepository.findByName(name);
    }

    @Override
    public Envelope getEnvelope(long id) {
        return envelopeRepository.findOne(id);
    }

    @Override
    public List<Envelope> getEnvelopes(long createdBy) {
        return envelopeRepository.findByCreatedBy(createdBy);
    }

    @Override
    public List<Envelope> getEnvelopes(EnvelopeStatus status) {
        return envelopeRepository.findByStatus(status);
    }
    
    @Override
    public Envelope addContent(long envelopeId, long envelopeContentId)
    {
        Envelope env = envelopeRepository.findOne(envelopeId);
        EnvelopeContent content = envelopeContentRepository.findOne(envelopeContentId);  
        if(content != null){
            if(!env.getEnvelopeContents().contains(content)){
                env.getEnvelopeContents().add(content);
                env.setEnvelopeContents(env.getEnvelopeContents());
                env.setUpdatedAt(new java.util.Date());
                envelopeRepository.save(env);
            }
        }        
        return  env;
    }

    @Override
    public Envelope addTransaction(long envelopeId, long envelopeTransactionId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    

    @Override
    public MimeType createMimeType(MimeType mimeType) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MimeType updateMimeType(MimeType mimeType) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MimeType updateMimeType(long id, MimeType mimeType) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MimeType deleteMimeType(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<MimeType> getMimeTypes() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EnvelopeContent createEnvelopeContent(EnvelopeContent envelopeContent) {
        EnvelopeContent content = new EnvelopeContent();
        content.setEnvelope(envelopeContent.getEnvelope());
        content.setFilename(envelopeContent.getFilename());
        content.setIsExecutable(envelopeContent.getIsExecutable());
        content.setMimeType(envelopeContent.getMimeType());
        content.setSize(envelopeContent.getSize());
        content.setStatus(envelopeContent.getStatus());
        content.setStoredname(envelopeContent.getStoredname());
        content.setVersion(envelopeContent.getVersion());
        return envelopeContentRepository.save(content);
    }

    @Override
    public EnvelopeContent updateEnvelopeContent(long id, EnvelopeContent envelopeContent) {
        EnvelopeContent content = envelopeContentRepository.findOne(id);
        content.setEnvelope(envelopeContent.getEnvelope());
        content.setFilename(envelopeContent.getFilename());
        content.setIsExecutable(envelopeContent.getIsExecutable());
        content.setMimeType(envelopeContent.getMimeType());
        content.setSize(envelopeContent.getSize());
        content.setStatus(envelopeContent.getStatus());
        content.setStoredname(envelopeContent.getStoredname());
        content.setVersion(envelopeContent.getVersion());
        return envelopeContentRepository.save(content);
    }

    @Override
    public EnvelopeContent deleteEnvelopeContent(long id) {
        EnvelopeContent content = envelopeContentRepository.findOne(id);
        content.setStatus(EnvelopeStatus.DELETED);
        return envelopeContentRepository.save(content);
    }

    @Override
    public List<EnvelopeContent> getEnvelopeContents(long envelopeId) {
        Envelope env = getEnvelope(envelopeId);
        return envelopeContentRepository.findByEnvelope(env);
    }

    @Override
    public List<EnvelopeContent> getEnvelopeContents(long envelopeId, EnvelopeStatus status) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EnvelopeTransaction createEnvelopeTransaction(EnvelopeTransaction envelopeTransaction) {
        EnvelopeTransaction transaction = new EnvelopeTransaction();
        transaction.setCreatedAt(new java.util.Date());
        transaction.setEnvelope(envelopeTransaction.getEnvelope());
        transaction.setInfo(envelopeTransaction.getInfo());
        transaction.setTransactionBy(envelopeTransaction.getTransactionBy());
        transaction.setType(envelopeTransaction.getType());        
        return envelopeTransactionRepository.save(transaction);
    }

    @Override
    public EnvelopeTransaction updateEnvelopeTransaction(long id, EnvelopeTransaction envelopeTransaction) {
        EnvelopeTransaction transaction = envelopeTransactionRepository.findOne(id);
        transaction.setCreatedAt(new java.util.Date());
        transaction.setEnvelope(envelopeTransaction.getEnvelope());
        transaction.setInfo(envelopeTransaction.getInfo());
        transaction.setTransactionBy(envelopeTransaction.getTransactionBy());
        transaction.setType(envelopeTransaction.getType());        
        return envelopeTransactionRepository.save(transaction);        
    }
 
    @Override
    public EnvelopeTransaction getEnvelopeTransaction(long id) {
        return envelopeTransactionRepository.findOne(id);
    }

    @Override
    public List<EnvelopeTransaction> getEnvelopeTransactions(long envelopeId) {
        Envelope env = getEnvelope(envelopeId);
        return envelopeTransactionRepository.findByEnvelope(env);
    }

    @Override
    public List<EnvelopeTransaction> getEnvelopeTransactions(long envelopeId, long type) {
        Envelope env = getEnvelope(envelopeId);
        return envelopeTransactionRepository.findByEnvelopeIdAndType(env.getId(), type);
    }

    @Override
    public List<EnvelopeTransaction> getEnvelopeTransactionsByType(long type) {
        return envelopeTransactionRepository.findByType(type);
    }


    
    
}
