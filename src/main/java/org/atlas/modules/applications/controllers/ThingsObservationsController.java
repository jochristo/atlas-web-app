package org.atlas.modules.applications.controllers;

/**
 * 
 * Customer profiles Rest Controller
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System modules ( Applications - Things Observations )
 * @since 2017-03-07
 * @endpoints
 * 	1.	ApiEndpoints.VINICITY_OBSERVATIONS [GET] [POST]
 *  2.	ApiEndpoints.VINICITY_OBSERVATION_THINGS [GET]
 **/

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.atlas.api.services.ThingsObservationsService;
import org.atlas.core.jsonapi.JSONApiDocument;
import org.atlas.modules.applications.domain.Observation;
import org.atlas.modules.applications.domain.Thing;
import org.atlas.modules.applications.legacy.ObservationReq;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class ThingsObservationsController {
	
	private ObjectMapper jsonMapper = new ObjectMapper();
	private static final Logger logger = LoggerFactory.getLogger(ThingsObservationsController.class);
	
	@Autowired
	private ThingsObservationsService thingsObserServ;
	
	//@RequestMapping(value = ApiEndpoints.VINICITY_THINGS , method = RequestMethod.GET, produces = "application/json")
	public JSONApiDocument getVinicityThings() {
		return new JSONApiDocument(this.thingsObserServ.getThings());	
	}
	
	//@RequestMapping(value = ApiEndpoints.VINICITY_THINGS , method = RequestMethod.POST, produces = "application/json")
	public JSONApiDocument storeVinicityThings(@RequestBody Thing thing) {
		return new JSONApiDocument(this.thingsObserServ.addThing(thing));	
	}
	
	//@RequestMapping(value = ApiEndpoints.VINICITY_OBSERVATIONS , method = RequestMethod.GET, produces = "application/json")
	public JSONApiDocument getVinicityObservations() {
		return new JSONApiDocument(this.thingsObserServ.getObservations());	
	}
	
	//@RequestMapping(value = ApiEndpoints.VINICITY_OBSERVATIONS , method = RequestMethod.POST, produces = "application/json")
	public JSONApiDocument storeVinicityObservation(@RequestBody ObservationReq observation) {
		Observation obserObj = new Observation();
		obserObj.setThing(this.thingsObserServ.getThing(observation.getId()));
		obserObj.setCreatedAt(new Date(observation.getTimestamp()*1000L));
		try {
			ArrayList<String> thingsIds = new ArrayList<String>();
			for( Thing thing : observation.getThings() ){
				thingsIds.add(String.valueOf(thing.getId()));
			}
			obserObj.setVicinityThings(this.jsonMapper.writeValueAsString(thingsIds));
		} catch (JsonProcessingException e) {
			logger.error("Unable to write observation vicinity things...",e);
		}
		return new JSONApiDocument(this.thingsObserServ.addObservation(obserObj));	
	}
	
	//@RequestMapping(value = ApiEndpoints.VINICITY_OBSERVATION_THINGS , method = RequestMethod.GET, produces = "application/json")
	public JSONApiDocument getVinicityObservationThings(@PathVariable long observationId) {
		Observation observation = this.thingsObserServ.getObservation(observationId);
		if( observation != null ){
			try {
				String[] things = this.jsonMapper.readValue(observation.getVicinityThings(), String[].class);
				ArrayList<Thing> thingsList = new ArrayList<Thing>();
				for( String thingStr : things ){
					Thing nthg = this.thingsObserServ.getThing(Long.parseLong(thingStr));
					if( nthg != null ){
						thingsList.add(nthg);
					}
				}
				return new JSONApiDocument(thingsList);
			} catch (IOException e) {
				logger.error("Unable to parse observation vicinity things...",e);
			}
		}
		return new JSONApiDocument();	
	}
	
}
