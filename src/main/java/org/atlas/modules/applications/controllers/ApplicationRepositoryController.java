package org.atlas.modules.applications.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.atlas.api.services.ApplicationRepositoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * Restful web services controller to handle application-related requests.
 * @author ic
 */
@RestController
public class ApplicationRepositoryController
{
    private ObjectMapper jsonMapper = new ObjectMapper();
    private static final Logger logger = LoggerFactory.getLogger(ApplicationRepositoryController.class);
	
    @Autowired
    private ApplicationRepositoryService applicationRepositoryService;
    
}
