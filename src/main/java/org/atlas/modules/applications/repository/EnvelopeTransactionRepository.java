package org.atlas.modules.applications.repository;

import java.util.List;
import org.atlas.modules.applications.domain.Envelope;
import org.atlas.modules.applications.domain.EnvelopeTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ic
 */
@Repository
public interface EnvelopeTransactionRepository extends JpaRepository<EnvelopeTransaction, Long> 
{    
    public List<EnvelopeTransaction> findByEnvelope(Envelope envelope);
    public List<EnvelopeTransaction> findByType(long type); 
    public List<EnvelopeTransaction> findByTransactionBy(long transactionBy);
    @Query("SELECT tr FROM EnvelopeTransaction tr INNER JOIN tr.envelope e WHERE tr.type =:type AND e.id =:envelopeId") 
    public List<EnvelopeTransaction> findByEnvelopeIdAndType(@Param("envelopeId") long envelopeId, @Param("type") long type);
}
