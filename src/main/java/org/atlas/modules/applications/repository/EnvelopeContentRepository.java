package org.atlas.modules.applications.repository;

import java.util.List;
import org.atlas.modules.applications.domain.Envelope;
import org.atlas.modules.applications.domain.EnvelopeContent;
import org.atlas.modules.applications.domain.EnvelopeStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ic
 */
@Repository
public interface EnvelopeContentRepository extends JpaRepository<EnvelopeContent, Long>
{
    public List<EnvelopeContent> findByEnvelope(Envelope envelope);    
    public List<EnvelopeContent> findByStatus(EnvelopeStatus status); 
}
