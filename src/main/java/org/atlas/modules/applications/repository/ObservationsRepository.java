package org.atlas.modules.applications.repository;


/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Core (Things Repository )
 * @since 2016-07-27
 **/

import java.util.List;

import org.atlas.modules.applications.domain.Observation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ObservationsRepository extends JpaRepository<Observation, Long> {
	
	@Query(value = "SELECT * FROM room_things_observations ORDER BY id DESC LIMIT 25", nativeQuery = true)
	List<Observation> getLastObservations();
}