package org.atlas.modules.applications.repository;

import java.util.List;
import org.atlas.modules.applications.domain.Envelope;
import org.atlas.modules.applications.domain.EnvelopeStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ic
 */
@Repository
public interface EnvelopeRepository extends JpaRepository<Envelope, Long>
{
    public List<Envelope> findByCreatedBy(long createdBy);
    public Envelope findByName(String name);
    public List<Envelope> findByStatus(EnvelopeStatus status);
}
