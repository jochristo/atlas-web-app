package org.atlas.modules.applications.repository;


/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Core (Things Repository )
 * @since 2016-07-27
 **/

import org.atlas.modules.applications.domain.Thing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ThingsRepository extends JpaRepository<Thing, Long> {	
	
	public Thing findByName(String name);
	
}