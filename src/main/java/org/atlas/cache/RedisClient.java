/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.cache;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.stereotype.Service;

/**
 * Implements the cache service operations: value and hash methods.
 * @author ic
 */
@Service("cacheService")
public class RedisClient implements CacheService
{     
    
    @Autowired
    private RedisOperations<String, Object> operations;
    private final int timeout = 10; // use 10 seconds

    @Autowired
    public RedisClient(final RedisOperations<String,Object> operations) {
        this.operations = operations;
        //this.redisTemplate = template;
    }   

    @PostConstruct
    public void RedisClientRedisOpsInit() {
        
    }

    public RedisClient() {
    }
    
    @Override
    public void add(String key, Object value)
    {
        operations.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
    }
    
    @Override
    public void add(String key, Object value, long expirationInSeconds) {
        operations.opsForValue().set(key, value, expirationInSeconds, TimeUnit.SECONDS);
    }
    
    @Override
    public void update(String key, Object value) {
        operations.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
    }
    
    public Object get(String key)
    {
        return operations.opsForValue().get(key);
    }

    @Override
    public Object added(String key, Object value) {
        operations.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
        return get(key);
    }

    @Override
    public Object updated(String key, Object value) {
        operations.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
        return get(key);
    }
    
    @Override
    public boolean hasKey(String key)
    {
        return operations.hasKey(key);
    }
    
    @Override
    public String randomKey()
    {
        return operations.randomKey();
    }
    
    @Override
    public long getExpire(String key)
    {
        return operations.getExpire(key);
    }

    @Override
    public void hAdd(String key, String field, Object value)
    {   
        if(hasKey(key))
        {        
            if(this.hExistsHashKey(key, field))
            {                
                operations.opsForHash().delete(field, value);
                hPutIfAbsent(key, field, value);
            }
            else
            {
                hPutIfAbsent(key, field, value);
            }
        }
        else
        {
            this.hPutIfAbsent(key, field, value);
        }
        
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        Date date = Date.from(zonedDateTime.plus(timeout, ChronoUnit.SECONDS).toInstant());        
        operations.expireAt(key, date);        
        
    }

    @Override
    public void hAdd(String key, String field, Object value, long expirationInSeconds) {
        if(this.hExistsHashKey(key, field))
        {
            operations.opsForHash().delete(field, value);
        }
        operations.opsForHash().put(key, field, value);        
        operations.expire(field, expirationInSeconds, TimeUnit.SECONDS);
    }

    @Override
    public Object hAdded(String key, String field, Object value) {
        operations.opsForHash().put(key, field, value);
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        Date date = Date.from(zonedDateTime.plus(timeout, ChronoUnit.SECONDS).toInstant());        
        operations.expireAt(field, date); 
        return (Object) operations.opsForHash().get(key, field);
    }    
    
    @Override
    public boolean hExistsHashKey(String key, String hashKey) {
        return operations.opsForHash().hasKey(key, hashKey);
    }

    @Override
    public void hPutIfAbsent(String type, String key, Object value)
    {
        operations.opsForHash().putIfAbsent(type, key, value);
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        Date date = Date.from(zonedDateTime.plus(timeout, ChronoUnit.SECONDS).toInstant());        
        operations.expireAt(key, date);            
    }

    @Override
    public void hPutIfAbsent(String type, String key, Object value, long expirationInSeconds)
    {
        operations.opsForHash().putIfAbsent(type, key, value);
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        Date date = Date.from(zonedDateTime.plus(expirationInSeconds, ChronoUnit.SECONDS).toInstant());        
        operations.expireAt(key, date);            
    }

    @Override
    public Object hGet(String h, String hashKey)
    {
        return (Object) operations.opsForHash().get(h, hashKey);
        
    }
}
