/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.atlas.cache;

/**
 * Defines the caching operations on implementing redis service class.
 * Value and hash methods.
 * @author ic
 * @param <T> The type of cached object
 */
public interface CacheService<T extends Object>
{
    public void add(String key, T value);
    public void add(String key, T value, long expiration);
    public T added(String key, T value);    
    public void update(String key, T value);
    public T updated(String key, T value);  
    boolean hasKey(String key);
    String randomKey();
    long getExpire(String key);
    
    //hasops
    public void hAdd(String type, String key, T value);
    public void hAdd(String type, String key, T value, long expiration);
    public T hAdded(String type, String key, T value);   
    boolean hExistsHashKey(String type, String key);
    void hPutIfAbsent(String type, String key, T value);  
    public void hPutIfAbsent(String type, String key, T value, long expirationInSeconds);
    public T hGet(String h, String hashKey);
}
