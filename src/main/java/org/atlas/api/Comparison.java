package org.atlas.api;

/**
 * Comparison
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Core
 * @since 2016-08-02
 **/
public enum Comparison {
	
	/**
	 * Not Equal
	 */
	NE,
	
	/**
	 * Equal to
	 */
	EQ,
	
	/**
	 * Greater than
	 */
	GT,
	
	/**
	 * Greater or equal to
	 */
	GE,
	
	/**
	 * Lest than
	 */
	LT,
	
	/**
	 * Les or equal to
	 */
	LE

}
