package org.atlas.api;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System core ( Scheduler )
 * @since 2016-06-29
 **/
public interface Scheduler {
	
	/**
	 * Submit a new Job to the Scheduler.
	 * @param job Runnable's job
	 */
	public void submit(Runnable job);
	
}
