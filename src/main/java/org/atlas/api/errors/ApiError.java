package org.atlas.api.errors;

/**
 * Define system global errors
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Core ( API Errors )
 * @since 2016-07-27
 **/
public enum ApiError {
	
	/**
	 * General Purpose Error
	 */
	INTERNAL_SERVER_ERROR("500","INTERNAL_SERVER_ERROR"),//Something goes wrong please retry.
	
	/**
	 * Invalid Token
	 */
	USER_NOT_FOUND("401","USER_NOT_FOUND"),//Customer not found
	
	/**
	 * Wrong Customer credentials
	 */
	USER_WRONG_CREDENTIALS("401", "USER_WRONG_CREDENTIALS"),//Invalid Login Credentials
	
	/**
	 * Constrain Violation
	 */
	CONSTRAIN_VIOLATION("1001", "CONSTRAIN_VIOLATION"),//Entity already exists
	
	/**
	 * Process already processed, No further processing needed
	 */
	PROCESS_ALREADY_PROCESSED("1002","PROCESS_ALREADY_PROCESSED"),//Already processed
	
	/**
	 * General Resource not found exception
	 */
	RESOURCE_NOT_FOUND("404", "RESOURCE_NOT_FOUND"),//Resource not found
	
	/**
	 * Invalid Argument type on URL
	 */
	ILLEGAL_ARGUMENT_TYPE("400", "ILLEGAL_ARGUMENT_TYPE"),//Invalid Arguments
	
	/**
	 * Unable to start session.
	 */
	FORBIDDEN_EXCEPTION("403", "FORBIDDEN_EXCEPTION"),//Unable to process request
	
	/**
	 * Data violation
	 */
	DATA_VIOLATION_EXCEPTION("422", "DATA_VIOLATION_EXCEPTION"),//Data violation
	
	/**
	 * Unable to start session.
	 */
	UNABLE_TO_START_SESSION("403", "UNABLE_TO_START_SESSION"),//Unable to start session
	
	/**
	 * Exception on the Media Type of the Request
	 */
	UNSUPPORTED_MEDIA_TYPE("415", "UNSUPPORTED_MEDIA_TYPE"),
	
	EMULATION_INVALID_QUEUE("422", "Unable to send message to queue (Queue not exist)"),
	EMULATION_JSON_REQUIRED("422", "JSON data required."),
	
        // Invalid response format during gateway commands interactions
	INVALID_GATEWAY_RESPONSE_FORMAT("422", "Invalid json format response from gateway");
	
	private final String appCode;
	private final String details;
	
	private ApiError(String appCode, String details) {
        this.appCode = appCode;
        this.details = details;
    }
	
	public String getAppCode() {
		return appCode;
	}

	public String getDetails() {
		return details;
	}

}
