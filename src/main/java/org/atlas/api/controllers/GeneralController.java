package org.atlas.api.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.atlas.api.ApiEndpoints;
import org.atlas.api.domain.Country;
import org.atlas.api.exceptions.ResourceNotFoundException;
import org.atlas.api.services.CountriesService;
import org.atlas.core.jsonapi.JSONApiDocument;


/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System core ( General Rest Controller )
 * @since 2016-07-25
 * endpoints
 * 	1.	ApiEndpoints.COUNTRIES
 * 	2.	ApiEndpoints.COUNTRY_BYID
 **/

@RestController
@CacheConfig(cacheNames = "generalapi",keyGenerator = "simpleKeyGenerator")
public class GeneralController {

	private final ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	private CountriesService countriesService;
	
	/**
	 * List Countries Endpoint (List)
	 * @return JSONApiDocument
	 */
	@Cacheable(value = "countries")
	@RequestMapping(value = ApiEndpoints.COUNTRIES , method = RequestMethod.GET, produces = "application/json")
	public JSONApiDocument loadCountries() {
		return new JSONApiDocument(countriesService.getCountries());	
	}
	
	/**
	 * Get specific country Endpoint(Country)
         * @param countryId the id of the country
	 * @return JSONApiDocument
	 */
	@RequestMapping(value = ApiEndpoints.COUNTRY_BYID , method = RequestMethod.GET, produces = "application/json")
	public JSONApiDocument getCountry(@PathVariable long countryId){
		Country country = countriesService.getCountry((int)countryId);
		if(country==null) {throw new ResourceNotFoundException();}
		return new JSONApiDocument(country);
	}
	
}
