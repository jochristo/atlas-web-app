package org.atlas.api.domain;

/**
 * @author IC <jocompass@gmail.com>
 * @version 0.1 
 * @category System core ( Core Entities )
 * @since 2016-07-25
 **/
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.atlas.core.jsonapi.annotations.JAPIAttribute;
import org.atlas.core.jsonapi.annotations.JAPIIdentifier;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="country")
public class Country {

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@JAPIIdentifier
	@JsonIgnore
    private long id;
	
	@JAPIAttribute
	private String country;
	
	@JAPIAttribute
	private String countrycode;
	
	@JAPIAttribute
	private String tld;
	
	@JAPIAttribute
	private String currency;
	
	@JAPIAttribute
	private String currencycode;
	private String timezone;
	
	@JAPIAttribute
	private String acc3;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCountrycode() {
		return countrycode;
	}
	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}
	public String getTld() {
		return tld;
	}
	public void setTld(String tld) {
		this.tld = tld;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getCurrencycode() {
		return currencycode;
	}
	public void setCurrencycode(String currencycode) {
		this.currencycode = currencycode;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	
	public String getAcc3() {
		return acc3;
	}
	public void setAcc3(String acc3) {
		this.acc3 = acc3;
	}
	
}
