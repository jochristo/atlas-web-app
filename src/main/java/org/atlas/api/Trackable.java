package org.atlas.api;

/**
 * 
 * Classes that will implement this Interface will provide a functionality for system tracking.
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Interface ( Trackable )
 * @since 2016-08-01
 **/
public interface Trackable {

	/**
	 * Make the track of the actions that made to the system.
	 * @return	String
	 */
	public Object track();
	
	
	
}
