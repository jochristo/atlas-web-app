package org.atlas.api.tasks;

import org.atlas.api.SysTask;
import org.atlas.modules.notifications.Recipient;
import org.atlas.modules.notifications.Sender;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.internet.MimeMessage;

public class EmailTask implements Runnable, SysTask{
	
	private static final Logger logger = LoggerFactory.getLogger(EmailTask.class);
	
	private Recipient to;
	private Sender from;
	private String subject;
	private String message;
	private JavaMailSender mailSender;
	
	public EmailTask(JavaMailSender mailSender){
		this.mailSender = mailSender;
	}
	
	@Override
	public void run() {
		final String emailTo = this.to.getEmail();
		final String emailFrom = this.from.getEmail();
		final String emailSubject = this.subject;
		final String emailMessage = this.message;
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
                message.setTo(emailTo);
                message.setFrom(emailFrom);
                message.setSubject(emailSubject);
                message.setText(emailMessage, true);
            }
        };
        this.mailSender.send(preparator);
		logger.debug("Executing Email Task: Receipient("+this.to+")");
		
	}

	public Recipient getTo() {
		return to;
	}

	public void setTo(Recipient to) {
		this.to = to;
	}

	public Sender getFrom() {
		return from;
	}

	public void setFrom(Sender from) {
		this.from = from;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String getTaskType() {
		return "email";
	}

	@Override
	public void setObject(Runnable obj) {}
	
	
	
}
