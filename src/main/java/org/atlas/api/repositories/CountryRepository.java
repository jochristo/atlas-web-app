package org.atlas.api.repositories;

import java.util.List;

import org.atlas.api.domain.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository  extends JpaRepository<Country, Long> {
	
	/**
	 * Retrieve all available Countries.
	 * @return All the countries ordered by country name.
	 */
	List<Country> findAllByOrderByCountryAsc();
	
	/**
	 * Get a country by the country name
	 * @param country	Country name
	 * @return			Country
	 */
	Country findByCountry(String country);
	
	/**
	 * Retrieve country by their currency.
        * @param currency the currency code
	 * @return All the countries with the given currency ordered by country name. 
	 */
	List<Country> findByCurrencycodeOrderByCountryAsc(String currency);
	
	/**
	 * Get a country by the Tld ( Top-level-domain )
	 * @param	tld	Country top level domain
	 * @return		Country
	 */
	Country findByTld(String tld);
	
	/**
	 * Get a country by the Country code
	 * @param	countrycode	Country code
	 * @return				Country
	 */
	Country findByCountrycode(String countrycode);
	
}
