package org.atlas.api;

/**
 * 
 * Classes that will implement this Interface will provide a functionality for exporting
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Interface ( Exportable )
 * @since 2016-07-22
 **/
public interface Exportable {

	/**
	 * Return the headers for the exportable file
	 * @return	String
	 */
	public String getHeaders();
	
	/**
	 * Return the values in appropriate format
        * @param separator Body separator
	 * @return String
	 */
	public String getBody(String separator);
	
}
