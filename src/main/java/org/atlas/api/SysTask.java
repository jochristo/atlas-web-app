package org.atlas.api;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category @category System core ( System Tasks )
 * @since 2016-06-29
 **/
public interface SysTask {
	
	/**
	 * Returns an identifier for the specific task.
	 * @return	The task type.
	 */
	public String getTaskType();
	
	/**
	 * Sets a Runnable object 
	 * @param obj Runnable object.
	 */
	public void setObject(Runnable obj);
}
