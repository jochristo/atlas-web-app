package org.atlas.api.exceptions;

import org.atlas.api.errors.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.UNPROCESSABLE_ENTITY, reason="Emulation error")
public class EmulationException extends ApplicationException {
	private static final long serialVersionUID = -3332292346834265371L;	
	
	public EmulationException(){
		super(ApiError.USER_NOT_FOUND);
	}
	
	public EmulationException(ApiError apiError){
		super(apiError);
	}
	
}