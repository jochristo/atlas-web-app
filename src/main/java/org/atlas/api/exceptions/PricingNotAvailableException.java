package org.atlas.api.exceptions;

import org.atlas.api.errors.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Pricing not found")
public class PricingNotAvailableException extends ApplicationException{

	private static final long serialVersionUID = 1L;
	
	public PricingNotAvailableException(){
		super(ApiError.RESOURCE_NOT_FOUND);
	}
	
	public PricingNotAvailableException(ApiError apiError){
		super(apiError);
	}
	
	public PricingNotAvailableException(String details){
		super(ApiError.RESOURCE_NOT_FOUND.getAppCode(), details);
	}
	
	public PricingNotAvailableException(String appCode, String details){
		super(appCode, details);
	}

}
