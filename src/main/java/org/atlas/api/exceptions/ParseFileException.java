package org.atlas.api.exceptions;

import org.atlas.api.errors.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="An error occurred, please try again.")
public class ParseFileException extends ApplicationException{

	private static final long serialVersionUID = 1L;
	
	public ParseFileException(){
		super(ApiError.INTERNAL_SERVER_ERROR);
	}
	
	public ParseFileException(ApiError apiError){
		super(apiError);
	}
	
	public ParseFileException(String details){
		super(ApiError.INTERNAL_SERVER_ERROR.getAppCode(), details);
	}
	
	public ParseFileException(String appCode, String details){
		super(appCode, details);
	}

}
