package org.atlas.api.exceptions;

/**
 * Custome: Interval Server Exception Handler
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Core ( System Exception )
 * @since 2016-07-27
 **/

import org.atlas.api.errors.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="An error occurred.")
public class CustomServerErrorException extends ApplicationException {
	private static final long serialVersionUID = -3332292346834265371L;	
	
	public CustomServerErrorException(){
		super(ApiError.INTERNAL_SERVER_ERROR);
	}
	
	public CustomServerErrorException(ApiError apiError){
		super(apiError);
	}
	
}