package org.atlas.api.exceptions;

import org.atlas.api.errors.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.UNPROCESSABLE_ENTITY, reason="Unprocessable Entity")
public class UnprocessableEntityException extends ApplicationException{

	private static final long serialVersionUID = 1L;
	
	public UnprocessableEntityException(){
		super(ApiError.DATA_VIOLATION_EXCEPTION);
	}
	
	public UnprocessableEntityException(ApiError apiError){
		super(apiError);
	}
	
	public UnprocessableEntityException(String details){
		super(ApiError.DATA_VIOLATION_EXCEPTION.getAppCode(), details);
	}
	
	public UnprocessableEntityException(String appCode, String details){
		super(appCode, details);
	}

}
