package org.atlas.api.exceptions;

import org.atlas.api.errors.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.UNPROCESSABLE_ENTITY, reason="Unprocessable entity")
public class NotValidCaptchaException extends ApplicationException{

	private static final long serialVersionUID = 1L;
	
	public NotValidCaptchaException(){
		super(ApiError.CONSTRAIN_VIOLATION);
	}
	
	public NotValidCaptchaException(ApiError apiError){
		super(apiError);
	}
	
	public NotValidCaptchaException(String details){
		super(ApiError.CONSTRAIN_VIOLATION.getAppCode(), details);
	}
	
	public NotValidCaptchaException(String appCode, String details){
		super(appCode, details);
	}

}
