package org.atlas.api.exceptions;

import org.atlas.api.errors.ApiError;

public class ApplicationException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7498621923421702089L;
	
	protected String appCode;
	protected String details;
	
	public ApplicationException(ApiError apiError) {
		this.appCode = apiError.getAppCode();
		this.details = apiError.getDetails();
	}
	
	public ApplicationException(String appCode, String details){
		this.appCode = appCode;
		this.details = details;
	}

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

}
