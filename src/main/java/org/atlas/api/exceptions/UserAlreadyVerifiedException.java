package org.atlas.api.exceptions;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Core ( Customer Exception )
 * @since 2016-07-25
 **/

import org.atlas.api.errors.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.CONFLICT, reason="CUSTOMER_ALREADY_VERIFIED")
public class UserAlreadyVerifiedException extends ApplicationException{

	private static final long serialVersionUID = 1L;
	
	public UserAlreadyVerifiedException(){
		super(ApiError.PROCESS_ALREADY_PROCESSED);
	}
	
	public UserAlreadyVerifiedException(ApiError apiError){
		super(apiError);
	}
	
	public UserAlreadyVerifiedException(String details){
		super(ApiError.PROCESS_ALREADY_PROCESSED.getAppCode(), details);
	}
	
	public UserAlreadyVerifiedException(String appCode, String details){
		super(appCode, details);
	}

}
