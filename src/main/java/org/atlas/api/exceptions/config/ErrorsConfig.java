package org.atlas.api.exceptions.config;

import java.util.Map;

import org.atlas.api.exceptions.ApplicationException;
import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestAttributes;

@Configuration
public class ErrorsConfig {

	@Bean
	public ErrorAttributes errorAttributes() { 
		return new DefaultErrorAttributes() {

	        @Override
	        public Map<String, Object> getErrorAttributes(RequestAttributes requestAttributes,boolean includeStackTrace) {
	            Map<String, Object> errorAttributes = super.getErrorAttributes(requestAttributes, includeStackTrace);
	            Throwable error = getError(requestAttributes);
	            if (error instanceof ApplicationException) {
	               errorAttributes.put("appCode", ((ApplicationException)error).getAppCode());
	               errorAttributes.put("details", ((ApplicationException)error).getDetails());
	            }
	            return errorAttributes;
	        }

	    };
	}
	
}
