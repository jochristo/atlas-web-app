package org.atlas.api.exceptions;

import org.atlas.api.errors.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Resource not found")
public class ResourceNotFoundException extends ApplicationException{

	private static final long serialVersionUID = 1L;
	
	public ResourceNotFoundException(){
		super(ApiError.RESOURCE_NOT_FOUND);
	}
	
	public ResourceNotFoundException(ApiError apiError){
		super(apiError);
	}
	
	public ResourceNotFoundException(String details){
		super(ApiError.RESOURCE_NOT_FOUND.getAppCode(), details);
	}
	
	public ResourceNotFoundException(String appCode, String details){
		super(appCode, details);
	}

}
