package org.atlas.api.exceptions;

import org.atlas.api.errors.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.UNPROCESSABLE_ENTITY, reason="Unprocessable entity")
public class EmailVerificationException extends ApplicationException{

	private static final long serialVersionUID = 1L;
	
	public EmailVerificationException(){
		super(ApiError.CONSTRAIN_VIOLATION);
	}
	
	public EmailVerificationException(ApiError apiError){
		super(apiError);
	}
	
	public EmailVerificationException(String details){
		super(ApiError.CONSTRAIN_VIOLATION.getAppCode(), details);
	}
	
	public EmailVerificationException(String appCode, String details){
		super(appCode, details);
	}

}
