package org.atlas.api.exceptions;

import org.atlas.api.errors.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.UNPROCESSABLE_ENTITY, reason="Unprocessable entity")
public class EmailAlreadyExistException extends ApplicationException{

	private static final long serialVersionUID = 1L;
	
	public EmailAlreadyExistException(){
		super(ApiError.CONSTRAIN_VIOLATION);
	}
	
	public EmailAlreadyExistException(ApiError apiError){
		super(apiError);
	}
	
	public EmailAlreadyExistException(String details){
		super(ApiError.CONSTRAIN_VIOLATION.getAppCode(), details);
	}
	
	public EmailAlreadyExistException(String appCode, String details){
		super(appCode, details);
	}

}
