package org.atlas.api.exceptions;

import org.atlas.api.errors.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="USER_NOT_FOUND")
public class UserNotFoundException extends ApplicationException {
	private static final long serialVersionUID = -3332292346834265371L;	
	
	public UserNotFoundException(){
		super(ApiError.USER_NOT_FOUND);
	}
	
	public UserNotFoundException(ApiError apiError){
		super(apiError);
	}
	
}