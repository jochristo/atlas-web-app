package org.atlas.api.exceptions;


/**
 * Custom: Forbidden Exception Handler
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * @category System Core ( System Exception )
 * @since 2016-07-27
 **/

import org.atlas.api.errors.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN, reason="Unable to process request")
public class ForbiddenException extends ApplicationException{

	private static final long serialVersionUID = 1L;
	
	public ForbiddenException(){
		super(ApiError.FORBIDDEN_EXCEPTION);
	}
	
	public ForbiddenException(ApiError apiError){
		super(apiError);
	}
	
	public ForbiddenException(String details){
		super(ApiError.FORBIDDEN_EXCEPTION.getAppCode(), details);
	}
	
	public ForbiddenException(String appCode, String details){
		super(appCode, details);
	}

}
