package org.atlas.api;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System core ( Rest API Endpoints )
 * @since 2016-07-25
 **/

public abstract class ApiEndpoints {
	
	/**
	 * Rest API prefix
	 */
	public static final String API_PATH = "/rest/v1";
	
	/**
	 * Customer login endpoint
	 * access All
	 * method POST - Authentication
	 * @see org.atlas.core.security.controllers.SecurityController (doLogin)
	 */
	public static final String AUTHENTICATION_LOGIN = API_PATH + "/auth/login";
	
	/**
	 * Customer logout endpoint
	 * access All
	 * method GET - Authentication
	 * @see org.atlas.core.security.controllers.SecurityController (doLogout)
	 */
	public static final String AUTHENTICATION_LOGOUT = API_PATH + "/auth/logout";
	
	/**
	 * Customer signup
	 * access All
	 * method POST - Customer Registration
	 * @see org.atlas.core.security.controllers.SecurityController (doSignup)
	 */
	public static final String AUTHENTICATION_SIGNUP = API_PATH + "/auth/signup";
	
	/**
	 * Retrieve customer profiles
	 * access Logged Users
	 * method - GET - Fetch customer profile (getProfile)
	 * method - PUT - Update Customer profile - Only profile, No Billing Information (updateProfile)
	 * @see org.atlas.modules.users.controllers.UserProfileController 
	 */
	public static final String USER_PROFILE = API_PATH + "/profile";
	
	/**
	 * Retrieve all countries
	 * access	Only by logged users.
	 * method GET - List Countries
	 * @see org.atlas.api.controllers.GeneralController (loadCountries)
	 */
	public static final String COUNTRIES = API_PATH + "/countries";
	
	/**
	 * Retrieve a specific Country by Id
	 * access	Only by logged users.
	 * method GET - Get specific Country
	 * params countryId - Integer - Country ID
	 * @see org.atlas.api.controllers.GeneralController (getCountry)
	 */
	public static final String COUNTRY_BYID = API_PATH + "/countries/{countryId}";
	
	/**
	 * Profile Server Services, Resources.
	 * access Super User
	 * method - GET - Profiling Server 
	 * see - Spring Boot Documentation
	 */
	public static final String SPRING_BOOT_PROFILE = "/profile";
	
	/**
	 * Profile specific server resource
	 * access Super User
	 * method - GET - Profiling Server 
	 * see - Spring Boot Documnetation
	 */
	public static final String SPRING_BOOT_PROFILE_REPOSITORY = "/profile/{repository}";
	
	/**
	 * Errors Controller, Spring will redirect all errors to this Endpoint ( DO NOT CHANGE )
	 * access All
	 * method - All
	 * @see org.atlas.core.handlers.errors.CustomErrorController (error)
	 */
	public static final String SPRING_BOOT_ERRORS = "/error";	
	
	/***
	 * AAL Houses Enpoints
	 */
	public static final String AALHOUSES = API_PATH + "/aalhouses";//Get all [GET][POST]
	public static final String AALHOUSES_BYID = API_PATH + "/aalhouses/{houseId}";//Get Specific [GET][PUT][DELETE]
	public static final String AALHOUSES_BYID_ENABLE = API_PATH + "/aalhouses/{houseId}/enable";//Get Specific [GET]
	public static final String AALHOUSES_GATEWAYS_BYID = API_PATH + "/aalhouses/{houseId}/gateways/{gatewayId}"; //Get Specific [GET][PUT][DELETE]    
        public static final String AALHOUSES_GATEWAYS = API_PATH + "/aalhouses/{houseId}/gateways"; //Get Specific [GET]  
        public static final String AALHOUSES_SENSORS = API_PATH + "/aalhouses/{houseId}/sensors"; //Get Specific [GET]  
        
	public static final String GATEWAYS = API_PATH + "/gateways";//Get all [GET][POST]
	public static final String GATEWAYS_BYID = API_PATH + "/gateways/{gatewayId}";//Get Specific [GET][PUT][DELETE]
	public static final String GATEWAYS_SENSORS = API_PATH + "/gateways/{gatewayId}/sensors";//Get All [GET][POST] 
	public static final String GATEWAYS_SENSORS_BYID = API_PATH + "/gateways/{gatewayId}/sensors/{sensorId}";//Get Specific [GET][PUT][DELETE]
	public static final String GATEWAYS_BYID_ENABLE = API_PATH + "/gateways/{gatewayId}/enable";//Get Specific [GET]
	
	public static final String SENSORS = API_PATH + "/sensors";//Get all [GET][POST]
	public static final String SENSORS_BYID = API_PATH + "/sensors/{sensorId}";//Get Specific [GET][PUT][DELETE]
	public static final String SENSORS_BYID_ENABLE = API_PATH + "/sensors/{sensorId}/enable";//Get Specific [GET]
	public static final String SENSORS_BYID_DATATYPE = API_PATH + "/sensors/{sensorId}/datatypes";//Get Specific [GET][PUT]
	public static final String DATATYPES = API_PATH + "/datatypes";//Get all [GET]
	public static final String MANUFACTURES = API_PATH + "/manufactures";//Get all [GET]
	
        public static final String DETACHED_SENSORS = API_PATH + "/sensors/detached"; //Get all [GET]
        public static final String ATTACHED_SENSORS = API_PATH + "/sensors/attached"; //Get all [GET]
        public static final String UNALLOCATED_SENSORS = API_PATH + "/sensors/unallocated"; //Get all [GET]
        
	/**
	 * Emulator Endpoints
	 */
	public static final String EMULATION = API_PATH + "/emulation";//Send an emulation [POST]
	public static final String EMULATION_QUEUES = API_PATH + "/emulation/queues";//Send an emulation [POST]
        
        /**
         * COMMANDS Endpoints        
         */
	public static final String GATEWAYS_PING_COMMAND = API_PATH + "/gateways/{gatewayId}/commands/ping"; //Get Specific [GET]        
        public static final String GATEWAYS_RESOURCES_COMMAND = API_PATH + "/gateways/{gatewayId}/commands/resources"; //Get Specific [GET]
        public static final String GATEWAYS_RESOURCES_RESULTS_COMMAND = API_PATH + "/gateways/{gatewayId}/results"; //Get Specific [GET]
        
        public static final String GATEWAYS_DEVICE_COMMAND = API_PATH + "/gateways/{gatewayId}/commands/device"; //Get Specific [GET]
        public static final String GATEWAYS_BATTERY_INDICATION_COMMAND = API_PATH + "/gateways/{gatewayId}/commands/battery"; //Get Specific [GET]
        public static final String DEVICE_BATTERY_INDICATION_COMMAND = API_PATH + "/gateways/{gatewayId}/commands/battery/{identity}"; //Get Specific [GET]
        public static final String GATEWAYS_OS_COMMAND = API_PATH + "/gateways/{gatewayId}/commands/os"; //Get Specific [GET]
        public static final String GATEWAYS_APPLICATION_COMMAND = API_PATH + "/gateways/{gatewayId}/commands/application"; //Get Specific [GET]
        public static final String GATEWAYS_COMMAND_RESPONSE = API_PATH + "/gateways/{gatewayId}/commands/{commandType}/response"; //Get Specific [GET]
        
        public static final String GATEWAYS_DASHBOARD = API_PATH + "/gateways/{gatewayId}/dashboard"; //Get Specific [GET] 
        
	/***
	 * Localization (Relative) Endpoints
	 */
	public static final String RELATIVE_OBSERVATIONS = API_PATH + "/relative/{aalhouseId}/{object}";
	
        /**
         * MOCK endpoints
         */
        public static final String GATEWAYS_MOCK_COMMAND = API_PATH + "/gateways/{gatewayId}/commands/mock"; //Get Specific [GET]         
        
	/***
	 * Applications Endpoints
	 */        
        public static final String APPLICATIONS = API_PATH + "/apps/gateways"; //Get Specific [GET]  
        public static final String APPLICATIONS_UPLOAD = API_PATH + "/apps/gateways/upload"; // [POST]  
        public static final String APPLICATIONS_DOWNLOAD = API_PATH + "/apps/gateways/download/{envId}"; // [GET]
}
