package org.atlas.api.services;

import java.util.List;

import org.atlas.api.domain.Country;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Core Services ( Country )
 **/
public interface CountriesService {

	/**
	 * Returns a list with all Countries in Database@return		A list of Countries
         * @return List
	 */
	List<Country> getCountries();
	
	/**
	 * Returns a country by Country Id
	 * @param	id	Country Id
	 * @return		Country
	 */
	Country getCountry(int id);
	
	/**
	 * Return a country by Country name ( E.g Greece )
	 * @param	country	Country name
	 * @return			Country
	 */
	Country getCountry(String country);
	
	/**
	 * Return a country by Country code ( E.g +30 )
	 * @param	countryCode	Country code
	 * @return				Country
	 */
	Country getCountryByCode(String countryCode);
	
	/**
	 * Return a list of Countries by currency code (e.g USD)
	 * @param	currencyCode	Country currency code
	 * @return					Country
	 */
	List<Country> getCountriesByCurrency(String currencyCode);
	
	/**
	 * Return a country by tld (Top-Level-Domain)
	 * @param	tld	Country Top level domain
	 * @return		Country
	 */
	Country getCountryByTld(String tld);
	
}
