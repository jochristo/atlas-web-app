package org.atlas.api.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;

import org.atlas.modules.aalhouse.domain.AALHouse;
import org.atlas.modules.aalhouse.domain.Datatype;
import org.atlas.modules.aalhouse.domain.Gateway;
import org.atlas.modules.aalhouse.domain.GatewaySensor;
import org.atlas.modules.aalhouse.domain.Manufacturer;
import org.atlas.modules.aalhouse.domain.Sensor;
import org.atlas.modules.mqtt.model.Command;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Modules ( Aalhouse )
 * Description: Main Service for aalhouse functionality.
 **/
public interface AALHouseService {
	
	public List<AALHouse> getHouses();
	public AALHouse getHouse(long houseId);
	public AALHouse enableHouse(long houseId);
	public AALHouse getHouse(String name);
	public List<Gateway> getHouseGateways(long houseId);
	
	public AALHouse addHouse(AALHouse house);
	public AALHouse updateHouse(long houseId, AALHouse house);
	public AALHouse deleteHouse(long houseId);
	public Gateway removeGatewayFromHouse(long gwId, long houseId);
	public Gateway attachGatewayToHouse(long gwId, long houseId);
        	
	public Gateway addGateway(Gateway gw);
	public Gateway updateGateway(long gwId, Gateway gw);
	public Gateway deleteGateway(long gwId);
	public Gateway enableGateway(long gwId); 
        public Gateway detachSensorFromGateway(long sensorId, long gwId);
        public Gateway attachSensorToGateway(long sensorId, long gwId); 
        public List<Gateway> getGatewaysOfHouse(long houseId);
	
	public List<Gateway> getGateways();
	public Gateway getGateway(long gwId);
	public Gateway getGateway(String gateway);
        
        public Gateway getGatewayByIdentity(String identity);
	
	public List<Sensor> getSensors();
	public List<Sensor> getHouseSensors(long houseId);
	public List<Sensor> getGatewayAllSensors(long gwId);
        public List<Sensor> getGatewayAttachedSensors(long gwId);
	
	public Sensor addSensor(Sensor sensor, HashSet<Datatype> datatypes);
	public Sensor updateSensor(long sensorId, Sensor sensor, HashSet<Datatype> datatypes);
	public Sensor enableSensor(long sensorId);
	public Sensor deleteSensor(long sensorId);
	public Sensor getSensor(long sensorId);
	public Sensor getSensor(String address);        
        public Sensor updateSensor(long sensorId, Sensor sensor);
	
	public List<Datatype> getSensorData(long sensorId);	
	public List<Datatype> getDatatypes();
	public List<Manufacturer> getManufactures();
	public Datatype getDatatype(long typeId);	
        
        // GatewaySensor
        public GatewaySensor addGatewaySensor(GatewaySensor gs);
        public GatewaySensor deleteGatewaySensor(GatewaySensor gs);
        public GatewaySensor updateGatewaySensor(long gatewayId, long sensorId, GatewaySensor gs);
        public GatewaySensor getGatewaySensor(long gatewayId, long sensorId);   
        
        public List<Sensor> getAttachedSensors();
        public List<Sensor> getDetachedSensors();
        public List<Sensor> getUnallocatedSensors();
        public List<Sensor> getSensorsByGatewayIdAndDetached(long gatewayId);
        public Gateway addGateway(Gateway gw, HashSet<Sensor> sensors);
        public Gateway updateGateway(long gwId, Gateway gw, HashSet<Sensor> sensors);
        
        // Gateway commands methods
        
        /**
         * Sends PING command to all sensors of given gateway.
         * @param gatewayId - The id of the gateway
         * @return Command
         */
        public Command sendPingCommand(long gatewayId);
        
        /**
         * Sends PING command to given sensor identity of the gateway.
         * @param gatewayId - The id of the gateway
         * @param identity - The identity of the sensor
         * @return Command
         */
        public Command sendPingCommand(long gatewayId, String identity);
        
        /**
         * Sends RESOURCES command to given gateway.
         * @param gatewayId - The id of the gateway
         * @return Command
         */
        public Command sendResourcesCommand(long gatewayId);               
        
        /**
         * Sends DEVICE command to given gateway with specified operation.
         * @param gatewayId - The id of the gateway
         * @param operation - The operation attribute of the command
         * @return Command
         */
        public Command sendDeviceCommand(long gatewayId, String operation);
        
        /**
         * Sends DEVICE command to given sensor identity of gateway with specified operation.
         * @param gatewayId - The id of the gateway
         * @param operation - The operation attribute of the command
         * @param identity - The sensor identity to send command to
         * @return Command
         */
        public Command sendDeviceCommand(long gatewayId, String operation, String identity);        
        
        /**
         * Sends BATTERY_INDICATION command to given gateway.
         * @param gatewayId - The id of the gateway
         * @return Command
         */
        public Command sendBatteryCommand(long gatewayId);
        
        /**
         * Sends BATTERY_INDICATION command to given sensor identity of the gateway.
         * @param gatewayId - the id of the gateway
         * @param identity - the identity of the sensor
         * @return Command
         */
        public Command sendBatteryCommand(long gatewayId, String identity);              
                
        /**
         * Sends OPERATING_SYSTEM command to given gateway with specified operation.
         * @param gatewayId - the id of the gateway
         * @param operation - the operation attribute of the command
         * @return Command
         */
        public Command sendOperatingSystemCommand(long gatewayId, String operation);
        
        /**
         * Sends APPLICATION command to given gateway with specified operation.
         * @param gatewayId - the id of the gateway
         * @param operation - the operation attribute of the command
         * @return Command
         */
        public Command sendApplicationCommand(long gatewayId, String operation);        
        
        /**
         * Gets the command response object stored in caching system upon receipt.
         * @param gatewayId - the id of the gateway
         * @param commandtype - the type of the command sent to the gateway
         * @return Object
         * @throws IOException  - Thrown during ObjectMapper read operations.
         */
        public Object getCommandResponse(long gatewayId, String commandtype) throws IOException;        
        
        /**
         * Gets the dashboard information (resources, battery level, etc.) of given id of gateway
         * @param gatewayId - the id of the gateway
         * @return Gateway
         */
        public Gateway getDashboard(long gatewayId);
        
        // command requests mockups
        public Object sendPingCommandMockup(long gatewayId, Command command) throws JsonProcessingException,IOException;
        public Object sendPingCommandMockup(long gatewayId) throws JsonProcessingException,IOException;
        public Object sendResourcesCommandMockup(long gatewayId, Command command) throws JsonProcessingException,IOException;
        public Object sendDeviceCommandMockup(long gatewayId, Command command) throws JsonProcessingException,IOException;
        public Object sendDeviceCommandMockup(long gatewayId) throws JsonProcessingException,IOException;
        public Object sendBatteryCommandMockup(long gatewayId, Command command) throws JsonProcessingException,IOException;        
        public Object sendBatteryCommandMockup(long gatewayId) throws JsonProcessingException,IOException; 
        public Object sendOperatingSystemCommandMockup(long gatewayId, Command command) throws JsonProcessingException,IOException;         
}
