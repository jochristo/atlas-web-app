package org.atlas.api.services;

import org.atlas.api.Trackable;

/**
 * 
 * System tracking service
 * Use a generic model in order to track  
 * 
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Core Services ( Tracking )
 * @since 2016-08-01
 **/
public interface TrackingService {
	
	/**
	 * Track a specific object
	 * @param trackable	The trackable object to track
	 * @return	Trackable
	 */
	Object track(Trackable trackable);
	
}
