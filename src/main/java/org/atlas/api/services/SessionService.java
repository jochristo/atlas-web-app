package org.atlas.api.services;

import org.atlas.core.security.session.Session;
import org.atlas.modules.users.domain.User;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Core Services ( Session )
 **/
public interface SessionService {
	
	
	/**
	 * Starts a new Session for an Authenticated User
         * @param customer The user instance
	 * @return  Session.
	 */
	public Session startSession(User customer);
	
	/**
	 * Checking System sessions, if Expired sessions occured flag them as expired.
	 */
	public void cleanSessions();
	
	/**
	 * Update an already existing session.
	 * @param session	Session to update
	 * @return			Session
	 */
	public Session updateSession(Session session);
	
	/**
	 * Close a session for an Authenticated User
	 * @param	token	Token expire.
	 * @return  Return the status of the session closing.
	 */
	public Session closeSession(String token);
}
