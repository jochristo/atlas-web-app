package org.atlas.api.services;

import org.atlas.modules.users.domain.User;
import org.atlas.modules.users.domain.UserStatus;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Modules ( Users )
 **/
public interface UserService {

	/**
	 * User Sign in
	 * @param user	User to sign in
	 * @return			User
	 */
	public User signin(User user);
	
	/**
	 * Get Signed in User
	 * @return	User
	 */
	public User getUser();
	
	/**
	 * Get User for Admins
         * @param userId The id of the user
	 * @return	User
	 */
	public User getUser(long userId);
	
	/**
	 * Update User profile
	 * @param user	User
	 * @param status	Indicate the status to be performed
	 * @return			User
	 */
	public User updateUserProfile(User user, UserStatus status);
	
}
