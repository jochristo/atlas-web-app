package org.atlas.api.services.impl;

import java.util.List;

import javax.annotation.Resource;

import org.atlas.api.domain.Country;
import org.atlas.api.repositories.CountryRepository;
import org.atlas.api.services.CountriesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CountriesServiceImpl implements CountriesService {
	
	private static final Logger logger = LoggerFactory.getLogger(CountriesService.class);
	
	@Resource
	private CountryRepository countryRepository;
	

	@Override
	public List<Country> getCountries() {
		logger.debug("Loading countries");
		return countryRepository.findAllByOrderByCountryAsc();
	}

	@Override
	public Country getCountry(int id) {
		logger.debug("Loading country with Id: " + id);
		return countryRepository.findOne((long)id);
	}

	@Override
	public Country getCountry(String country) {
		logger.debug("Loading country with Country name: " + country);
		return countryRepository.findByCountry(country);
	}

	@Override
	public List<Country> getCountriesByCurrency(String currencyCode) {
		logger.debug("Loading country with Currency code: " + currencyCode);
		return countryRepository.findByCurrencycodeOrderByCountryAsc(currencyCode);
	}

	@Override
	public Country getCountryByTld(String tld) {
		logger.debug("Loading country with TLD: " + tld);
		return countryRepository.findByTld(tld);
	}

	@Override
	public Country getCountryByCode(String countryCode) {
		logger.debug("Loading country with country code: " + countryCode);
		return countryRepository.findByCountrycode(countryCode);
	}

}
