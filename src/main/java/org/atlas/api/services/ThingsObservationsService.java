package org.atlas.api.services;

import java.util.List;

import org.atlas.modules.applications.domain.Observation;
import org.atlas.modules.applications.domain.Thing;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category Applications Modules ( Room Things Observations )
 **/
public interface ThingsObservationsService {
	
	public List<Thing> getThings();
	public Thing addThing(Thing thing);
	public Thing getThing(long id);
	public Thing getThing(String name);
	public Observation addObservation(Observation observation);
	public Observation getObservation(long id);
	public List<Observation> getObservations();
	
}
