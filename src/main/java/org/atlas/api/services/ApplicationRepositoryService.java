package org.atlas.api.services;

import java.util.List;
import org.atlas.modules.applications.domain.Envelope;
import org.atlas.modules.applications.domain.EnvelopeContent;
import org.atlas.modules.applications.domain.EnvelopeStatus;
import org.atlas.modules.applications.domain.EnvelopeTransaction;
import org.atlas.modules.applications.domain.MimeType;

/**
 * Defines methods to handle application-related entities.
 * @author ic
 */
public interface ApplicationRepositoryService
{
    // Envelopes
    public Envelope createEnvelope(Envelope envelope);
    public Envelope updateEnvelope(Envelope envelope);
    public Envelope updateEnvelope(long id, Envelope envelope);
    public Envelope deleteEnvelope(long id);
    public List<Envelope> getEnvelopes();    
    public Envelope getEnvelope(String name);
    public Envelope getEnvelope(long id);
    public List<Envelope> getEnvelopes(long createdBy); 
    public List<Envelope> getEnvelopes(EnvelopeStatus status);
    public Envelope addContent(long envelopeId, long envelopeContentId);
    public Envelope addTransaction(long envelopeId, long envelopeTransactionId);
    
    // Mime types
    public MimeType createMimeType(MimeType mimeType);
    public MimeType updateMimeType(MimeType mimeType);
    public MimeType updateMimeType(long id, MimeType mimeType);    
    public MimeType deleteMimeType(long id);
    public List<MimeType> getMimeTypes();  
    
    // Envelope Contents
    public EnvelopeContent createEnvelopeContent(EnvelopeContent envelopeContent);    
    public EnvelopeContent updateEnvelopeContent(long id, EnvelopeContent envelopeContent);     
    public EnvelopeContent deleteEnvelopeContent(long id);    
    public List<EnvelopeContent> getEnvelopeContents(long envelopeId);   
    public List<EnvelopeContent> getEnvelopeContents(long envelopeId, EnvelopeStatus status); 
        
    // Envelope Transactions
    public EnvelopeTransaction createEnvelopeTransaction(EnvelopeTransaction envelopeTransaction);
    public EnvelopeTransaction updateEnvelopeTransaction(long id, EnvelopeTransaction envelopeTransaction);        
    public EnvelopeTransaction getEnvelopeTransaction(long id);
    public List<EnvelopeTransaction> getEnvelopeTransactions(long envelopeId); 
    public List<EnvelopeTransaction> getEnvelopeTransactions(long envelopeId, long type);
    public List<EnvelopeTransaction> getEnvelopeTransactionsByType(long type);
    
}
