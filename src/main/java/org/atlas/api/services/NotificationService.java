package org.atlas.api.services;

import org.atlas.modules.notifications.Notifier;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Core Services ( Notifications )
 **/
public interface NotificationService {
	
	/**
	 * Submit a new Notification to the system to be executed
	 * @param	notifier	Notifier
	 */
	void submitNotification(Notifier notifier);
	
	
	
}
