package org.atlas.api.services;

import java.util.List;

import org.atlas.modules.emulator.EmulationData;
import org.atlas.modules.emulator.EmulatorQueue;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Core Services ( Notifications )
 **/
public interface EmulationService {
		
	EmulationData sendEmulation(long queueId, String message);
	public List<EmulatorQueue> getQueues();
	
}
