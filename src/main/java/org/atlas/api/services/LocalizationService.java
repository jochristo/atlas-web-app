package org.atlas.api.services;

import java.util.List;

import org.atlas.modules.localization.Location;

/**
 * @author IC <a href="mailto:jocompass@gmail.com">IC</a>
 * @version 0.1 
 * category System Modules ( Localization )
 * Description: Main Service for localization.
 **/
public interface LocalizationService {
	
	public List<Location> getObservedLocationsByHouse(long aalhouse, String keys);
	
}
