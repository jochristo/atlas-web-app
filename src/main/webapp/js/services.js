/**
 * Install Angular Services
 */

function AbstractService($translate){
	return{
		getApiPrefix: function(){
			return '/rest/v1';
		},

		getUUID: function(){
			return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		        var r = Math.random()*16|0, v = c === 'x' ? r : (r&0x3|0x8);
		        return v.toString(16);
		    });
		}
	};
}

function JSONApiService(AbstractService, $translate){

	return{
		getDocument: function(data, options){
			var document = {};

			if( options && options.multi ){
				document.data = [];
				angular.forEach(data, function(value, key) {
					this.push({ id: AbstractService.getUUID(), type: key, attributes: value });
				}, document.data);
			}else{
				var datatype = '';
				var isDataArray = false;

				if( options ){
					isDataArray = ( options.data_enclosure && options.data_enclosure == 'array' );
					if( options.type ) datatype = options.type;
				}

				var document_data = { id: AbstractService.getUUID(), type: datatype, attributes: data };
				if( options && options.id != undefined ){
					document_data.id = options.id;
					document_data.attributes = {};
				}


				if( isDataArray ){
					document.data = [];
					document.data.push(document_data);
				}else{
					document.data = document_data;
				}
			}

			if( options && options.include ){
				var includedData = {};
				if (options.include instanceof Array) {
					includedData = [];
					angular.forEach(options.include, function(value, key) {
						var loopData = { id: AbstractService.getUUID(), type: "", attributes: {} };
						angular.forEach(value, function(subvalue, subkey) {
							if( subkey == 'id' ){
								loopData.id = value.id;
							}else if( subkey == 'type' ){
								loopData.type = value.type;
							}else{
								loopData.attributes[subkey] = value[subkey];
							}
						});
						includedData.push(loopData);
					});
				}
				document.included = includedData;
			}

			document.jsonapi = { version: "1.0" };
			return document;
		},

		showErrors: function(document){
			var errors = '';
			angular.forEach(document.errors, function(value, key) {
				console.log(value);
				errors += $translate.instant(value.detail);
			});
			return errors;
		}
	};

}

function AuthenticationService($http, AbstractService, JSONApiService){

	return {
		login : function(loginCredentials){
			return $http.post(AbstractService.getApiPrefix()+'/auth/login',JSONApiService.getDocument(loginCredentials)).then(function(result) {
				return result.data;
			});
		},

		logout: function(){
			return $http.get(AbstractService.getApiPrefix()+'/auth/logout').then(function(result) {
				return result.data;
			});
		}

	};

}

function UserService($http, AbstractService, JSONApiService){

	return {
		updateProfile: function(type, data){
			return $http.put(AbstractService.getApiPrefix()+'/profile',JSONApiService.getDocument(data, {type: type})).then(function(result) {
				return result.data;
			});
		},

		getProfile: function(){
			return $http.get(AbstractService.getApiPrefix()+'/profile').then(function(result) {
				return result.data;
			});
		}
	};

}

function AALHouseService($http, AbstractService, JSONApiService){

	return {
		getHouses: function(){
			return $http.get(AbstractService.getApiPrefix()+'/aalhouses').then(function(result) {
				return result.data;
			});
		},

		addHouse: function(aalhouse){
			return $http.post(AbstractService.getApiPrefix()+'/aalhouses',JSONApiService.getDocument(aalhouse)).then(function(result) {
				return result.data;
			});
		},

		updateHouse: function(aalhouseId, aalhouse){
			return $http.put(AbstractService.getApiPrefix()+'/aalhouses/'+aalhouseId,JSONApiService.getDocument(aalhouse)).then(function(result) {
				return result.data;
			});
		},

		getHouseById: function(aalhouseId){
			return $http.get(AbstractService.getApiPrefix()+'/aalhouses/'+aalhouseId).then(function(result) {
				return result.data;
			});
		},

		enableHouseById: function(aalhouseId){
			return $http.get(AbstractService.getApiPrefix()+'/aalhouses/'+aalhouseId+'/enable').then(function(result) {
				return result.data;
			});
		},

		deleteHouseById: function(aalhouseId){
			return $http.delete(AbstractService.getApiPrefix()+'/aalhouses/'+aalhouseId).then(function(result) {
				return result.data;
			});
		}
	};

}

function GatewaysService($http, AbstractService, JSONApiService){

    return {
        getGateways: function () {
            return $http.get(AbstractService.getApiPrefix() + '/gateways').then(function (result) {
                return result.data;
            });
        },

        addGateway: function (gateway) {
            return $http.post(AbstractService.getApiPrefix() + '/gateways', JSONApiService.getDocument(gateway)).then(function (result) {
                return result.data;
            });
        },

        updateGateway: function (gatewayId, gateway) {
            return $http.put(AbstractService.getApiPrefix() + '/gateways/' + gatewayId, JSONApiService.getDocument(gateway)).then(function (result) {
                return result.data;
            });
        },

        getGatewayById: function (gatewayId) {
            return $http.get(AbstractService.getApiPrefix() + '/gateways/' + gatewayId).then(function (result) {
                return result.data;
            });
        },

        enableGatewayById: function (gatewayId) {
            return $http.get(AbstractService.getApiPrefix() + '/gateways/' + gatewayId + '/enable').then(function (result) {
                return result.data;
            });
        },

        deleteGatewayById: function (gatewayId) {
            return $http.delete(AbstractService.getApiPrefix() + '/gateways/' + gatewayId).then(function (result) {
                return result.data;
            });
        },

        /* Add sensor to gateway func */
        addToGateway: function (gatewayId, sensorId) {
            return $http.post(AbstractService.getApiPrefix() + '/gateways/', +gatewayId + '/sensors/' + sensorId).then(function (result) {
                return result.data;
            });
        },

        getGatewaySensors: function (gatewayId)
        {
            return $http.get(AbstractService.getApiPrefix() + '/gateways/' + gatewayId + '/sensors').then(function (result) {
                return result.data;
            });
        },
        updateGatewayWithSensors: function (gatewayId, gateway, sensors) {
            var options = {include: sensors};
            return $http.put(AbstractService.getApiPrefix() + '/gateways/' + gatewayId, JSONApiService.getDocument(gateway, options)).then(function (result) {
                return result.data;
            });

        },
                
        // get command response by command type
        getCommandResponse: function (gatewayId, commandType)
        {
            return $http.get(AbstractService.getApiPrefix() + '/gateways/' + gatewayId + '/commands/' + commandType + '/response').then(function (result) {
                return result.data;
            });
        },

        // request commands
        requestPing: function (gatewayId)
        {
            return $http.get(AbstractService.getApiPrefix() + '/gateways/' + gatewayId + '/commands/ping').then(function (result) {
                return result.data;
            });
        },
        requestResources: function (gatewayId)
        {
            return $http.get(AbstractService.getApiPrefix() + '/gateways/' + gatewayId + '/commands/resources').then(function (result) {
                return result.data;
            });
        },
        requestBattery: function (gatewayId)
        {
            return $http.get(AbstractService.getApiPrefix() + '/gateways/' + gatewayId + '/commands/battery').then(function (result) {
                return result.data;
            });
        },  
        requestDeviceBattery: function (gatewayId, identity)
        {
            return $http.get(AbstractService.getApiPrefix() + '/gateways/' + gatewayId + '/commands/battery?identity=' + identity).then(function (result) {
                return result.data;
            });
        },         
        requestDevice: function (gatewayId, operation)
        {
            return $http.get(AbstractService.getApiPrefix() + '/gateways/' + gatewayId + '/commands/device?operation=' + operation).then(function (result) {
                return result.data;
            });
        },
        requestOperatingSystem: function (gatewayId, operation)
        {
            return $http.get(AbstractService.getApiPrefix() + '/gateways/' + gatewayId + '/commands/os?operation=' + operation).then(function (result) {
                return result.data;
            });
        },
        requestApplication: function (gatewayId, operation)
        {
            return $http.get(AbstractService.getApiPrefix() + '/gateways/' + gatewayId + '/commands/application?operation=' + operation).then(function (result) {
                return result.data;
            });
        }                

	};

}

function SensorsService($http, AbstractService, JSONApiService){

	return {
		getSensors: function(){
			return $http.get(AbstractService.getApiPrefix()+'/sensors').then(function(result) {
				return result.data;
			});
		},

		addSensor: function(sensor, datatypes){
			var options = {include: datatypes};
			return $http.post(AbstractService.getApiPrefix()+'/sensors',JSONApiService.getDocument(sensor, options)).then(function(result) {
				return result.data;
			});
		},

		updateSensor: function(sensorId, sensor, datatypes){
			var options = {include: datatypes};
			return $http.put(AbstractService.getApiPrefix()+'/sensors/'+sensorId,JSONApiService.getDocument(sensor, options)).then(function(result) {
				return result.data;
			});
		},

		getSensorById: function(sensorId){
			return $http.get(AbstractService.getApiPrefix()+'/sensors/'+sensorId).then(function(result) {
				return result.data;
			});
		},

		enableSensorById: function(sensorId){
			return $http.get(AbstractService.getApiPrefix()+'/sensors/'+sensorId+'/enable').then(function(result) {
				return result.data;
			});
		},

		deleteSensorById: function(sensorId){
			return $http.delete(AbstractService.getApiPrefix()+'/sensors/'+sensorId).then(function(result) {
				return result.data;
			});
		},

		getDatatypes: function(){
			return $http.get(AbstractService.getApiPrefix()+'/datatypes').then(function(result) {
				return result.data;
			});
		},

		getManufacturers: function(){
			return $http.get(AbstractService.getApiPrefix()+'/manufactures').then(function(result) {
				return result.data;
			});
		},
		getUnallocatedSensors: function(){
			return $http.get(AbstractService.getApiPrefix()+'/sensors'+'/unallocated').then(function(result) {
				return result.data;
			});
		}
	};

}

function EmulationService($http, AbstractService, JSONApiService){

	return {
		sendMessage: function(emulation){
			return $http.post(AbstractService.getApiPrefix()+'/emulation',JSONApiService.getDocument(emulation, {type: 'emulation'})).then(function(result) {
				return result.data;
			});
		},

		getQueues: function(){
			return $http.get(AbstractService.getApiPrefix()+'/emulation/queues').then(function(result) {
				return result.data;
			});
		}
	};

}

function LocalizationService($http, AbstractService, JSONApiService){
	return {
		getLocationsByHouse: function(houseId, object){
			return $http.get(AbstractService.getApiPrefix()+'/relative/'+houseId+'/'+object).then(function(result) {
				return result.data;
			});
		}
	};
}

angular
    .module('atlasweb')
    .factory('AbstractService', AbstractService)
    .factory('JSONApiService', JSONApiService)
    .factory('AuthenticationService', AuthenticationService)
    .factory('UserService', UserService)
    .factory('AALHouseService', AALHouseService)
    .factory('GatewaysService', GatewaysService)
    .factory('SensorsService', SensorsService)
    .factory('EmulationService', EmulationService)
    .factory('LocalizationService', LocalizationService)
    ;
