function capitalize() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
}

/**
 * Pass all filters into module
 */
angular
    .module('atlasweb')
    .filter('capitalize', capitalize)