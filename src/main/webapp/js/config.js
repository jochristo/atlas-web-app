/**
 * ATLAS Web Application
 *
 */
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {

	// Configure Idle settings
	//IdleProvider.idle(5); // in seconds
    //IdleProvider.timeout(120); // in seconds

    $urlRouterProvider.otherwise("/auth/login");

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: true
    });

    $stateProvider

	    .state('auth', {
	    	abstract: true,
            url: "/auth",
            templateUrl: "",
            authenticate: false
	    })
	    .state('auth.login', {
	        url: "/login",
	        templateUrl: "views/auth/login.html",
	        data: { pageTitle: 'Login', specialClass: 'gray-bg' },
	        controller: 'LoginCtrl',
	    	authenticate: false
	    })
        .state('index', {
            abstract: true,
            url: "/index",
            templateUrl: "views/common/content.html",
            authenticate: true
        })
        .state('index.dashboard', {
            url: "/main",
            templateUrl: "views/main.html",
            data: { pageTitle: 'Example view' },
            controller: 'DashboardCtrl as dashCtrl',
        	authenticate: true
        })
        .state('system', {
            abstract: true,
            url: "/system",
            templateUrl: "views/common/content.html",
            authenticate: true
        })
        .state('system.network', {
            url: "/network",
            templateUrl: "views/main.html",
            data: { pageTitle: 'Network' },
        	authenticate: true
        })
        .state('system.emulator', {
            url: "/emulator",
            templateUrl: "views/system/emulator.html",
            data: { pageTitle: 'Emulator' },
        	authenticate: true,
        	controller: 'EmulatorCtrl as emulatorCtrl',
        	resolve: {
                emulationQueues: function(EmulationService) {
      		      return EmulationService.getQueues();
    		    }
            }
        })
        /*AALHouse (START)*/
        .state('aalhouses', {
            abstract: true,
            url: "/aalhouses",
            templateUrl: "views/common/content.html",
            authenticate: true
        })
        .state('aalhouses.list', {
        	url: "",
            templateUrl: "views/aalhouses/aalhouses-list.html",
            controller: 'AALHousesCtrl as aalCtrl',
            data: { pageTitle: 'AAL Houses' },
        	authenticate: true,
        	resolve: {
                aalhouses: function(AALHouseService) {
      		      return AALHouseService.getHouses();
    		    }
            }
        })
        .state('aalhouses.add', {
            url: "/add",
            templateUrl: "views/aalhouses/aalhouses-add.html",
            controller: 'AddHousesCtrl as addAalCtrl',
            data: { pageTitle: 'Add House' },
        	authenticate: true
        })
        .state('aalhouses.edit', {
            url: "/:aalhousesId/edit",
            templateUrl: "views/aalhouses/aalhouses-edit.html",
            controller: 'EditHousesCtrl as editAalCtrl',
            data: { pageTitle: 'Edit House' },
        	authenticate: true,
        	resolve: {
        		aalhouseData: ['$http','$stateParams', 'AALHouseService', function($http, $stateParams, AALHouseService) {
        			return AALHouseService.getHouseById($stateParams.aalhousesId);
		         }]
        	}
        })
        /*Gateways (START)*/
        .state('gateways', {
            abstract: true,
            url: "/gateways",
            templateUrl: "views/common/content.html",
            authenticate: true
        })
        .state('gateways.list', {
        	url: "",
            templateUrl: "views/gateways/gateways-list.html",
            controller: 'GatewaysCtrl as gwCtrl',
            data: { pageTitle: 'Gateways' },
        	authenticate: true,
        	resolve: {
                gateways: function(GatewaysService) {
      		      return GatewaysService.getGateways();
    		}
            }
        })
        .state('gateways.add', {
            url: "/add",
            templateUrl: "views/gateways/gateways-add.html",
            controller: 'AddGatewayCtrl as addGwCtrl',
            data: { pageTitle: 'Add Gateway' },
        	authenticate: true
        })
        .state('gateways.edit', {
            url: "/:gatewayId/edit",
            templateUrl: "views/gateways/gateways-edit.html",
            controller: 'EditGatewayCtrl as editGwCtrl',
            data: { pageTitle: 'Edit Gateway' },
        	authenticate: true,
        	resolve: {
                    gatewayData: ['$http','$stateParams', 'GatewaysService', function($http, $stateParams, GatewaysService) {
                        return GatewaysService.getGatewayById($stateParams.gatewayId);
		    }],
                    unallocatedSensors: function(SensorsService)
                    {
                      return SensorsService.getUnallocatedSensors();
                    },
                    sensorsData: function($http, $stateParams, GatewaysService)
                    {
                      return GatewaysService.getGatewaySensors($stateParams.gatewayId);
                    },
                    aalhouses: function(AALHouseService) {
                            return AALHouseService.getHouses();
                    }                    
        	}
        })

        .state('gateways.dashboard', {
            url: "/:gatewayId/dashboard",
            templateUrl: "views/gateways/gateways-dashboard.html",
            controller: 'GatewayDashboardCtrl',
            data: { pageTitle: 'Gateway Dashboard' },
            authenticate: true,
            resolve: {
                gatewayData: ['$http','$stateParams', 'GatewaysService', function($http, $stateParams, GatewaysService) {
                    return GatewaysService.getGatewayById($stateParams.gatewayId);
		}],
                resourcesData: function($http, $stateParams, GatewaysService) {
                     return GatewaysService.getCommandResponse($stateParams.gatewayId, 'resources');
                },
                pingData: function($http, $stateParams, GatewaysService) {
                     return GatewaysService.getCommandResponse($stateParams.gatewayId, 'ping');
                }
            }
        })


        /*Gateways (END)*/
        .state('sensors', {
            abstract: true,
            url: "/sensors",
            templateUrl: "views/common/content.html",
            authenticate: true
        })
        .state('sensors.list', {
            url: "",
            templateUrl: "views/sensors/sensors-list.html",
            controller: 'SensorsCtrl as snCtrl',
            data: { pageTitle: 'Sensors' },
        	authenticate: true,
        	resolve: {
                sensors: function(SensorsService) {
      		      return SensorsService.getSensors();                      
    		    }
            }
        })
        .state('sensors.add', {
            url: "/add",
            templateUrl: "views/sensors/sensors-add.html",
            controller: 'AddSensorCtrl as addSnCtrl',
            data: { pageTitle: 'Add Sensor' },
        	authenticate: true,
        	resolve: {
        		manufacturers: function(SensorsService) {
        			return SensorsService.getManufacturers();
        		},
                datatypes: function(SensorsService) {
      		      return SensorsService.getDatatypes();
    		    }
            }
        })
        .state('sensors.edit', {
            url: "/:sensorId/edit",
            templateUrl: "views/sensors/sensors-edit.html",
            controller: 'EditSensorCtrl as editSnCtrl',
            data: { pageTitle: 'Edit Sensor' },
        	authenticate: true,
        	resolve: {
        		manufacturers: function(SensorsService) {
        			return SensorsService.getManufacturers();
        		},
        		datatypes: function(SensorsService) {
        		      return SensorsService.getDatatypes();
                        },
        		sensorData: ['$http','$stateParams', 'SensorsService', function($http, $stateParams, SensorsService) {
        			return SensorsService.getSensorById($stateParams.sensorId);
		        }]                        
        	}
        })
        .state('applications', {
            abstract: true,
            url: "/applications",
            templateUrl: "views/common/content.html",
            authenticate: true
        })
        .state('applications.indoor', {
            url: "/indoor",
            templateUrl: "views/localization/indoor.html",
            data: { pageTitle: 'Indoor' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: [ 'js/unity/UnityProgress.js' ]
                        }
                    ]);
                }
            },
        	authenticate: true
        })
        .state('applications.localization', {
            url: "/localization",
            templateUrl: "views/localization/relative.html",
            data: { pageTitle: 'Localization' },
            controller: 'LocalizationCtrl as locCtrl',
            resolve: {
            	aalhouses: function(AALHouseService) {
            		return AALHouseService.getHouses();
            	}
            },
        	authenticate: true
        })
}
angular
    .module('atlasweb')
    .config(config)
    .run(function($rootScope, $state, $http, $location, $window) {
        $rootScope.$state = $state;
        // redirect to login page if not logged in and trying to access a restricted page
        $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){

            var token = $window.localStorage['jwtToken'];
            if (toState.authenticate && !token){
            	// User isn’t authenticated
                $state.transitionTo("auth.login");
                event.preventDefault();
            }

        });
    });
