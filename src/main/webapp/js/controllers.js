/**
 * ATLAS Web Application
 *
 */

//Authentication Interceptor
//https://thinkster.io/angularjs-jwt-auth (Checking)
function authInterceptor($window,$q, $injector) {
    return {
        // automatically attach Authorization header
        request: function(config) {
        	$("button[type=submit]").attr('disabled','disabled');
            var token = $window.localStorage['jwtToken'];
            if( token ){
                config.headers.Authorization = 'Bearer ' + token;
            }
            return config;
        },

        // If a token was sent back, save it
        response: function(res) {
        	$("button[type=submit]").removeAttr('disabled');
            return res;
        },

        // If a token was sent back, save it
        responseError: function(response) {
        	$("button[type=submit]").removeAttr('disabled');
            if( response.status == 401 ){
                $window.localStorage.removeItem('jwtToken');
                var $state = $injector.get('$state');
                $state.go('auth.login');
                event.preventDefault();
            }
            return $q.reject(response);
        }
    }
}


/**
 * MainCtrl - controller
 */
function MainCtrl($scope, AuthenticationService, $window, $state, toaster, $translate, $rootScope, UserService) {

    this.helloText = 'Welcome to ATLAS IoT Platform';
    this.descriptionText = '';

    $rootScope.$on('loadprofile', function (event) {
    	UserService.getProfile().then(function(data){
    		$scope.user = data.data.attributes;
        },function errorCallback(response) {});
    });

    $rootScope.$emit('loadprofile');

    /**
     * Logout Control
     */
    $scope.logout = function(){
    	AuthenticationService.logout().then(function(data){
        	$window.localStorage.removeItem('jwtToken');
        	$state.go('auth.login');
        	toaster.success({ body:$translate.instant('LOGOUT_SUCCESS')});
        },function errorCallback(response) {
        	toaster.error({ body:$translate.instant('UNABLE_TO_LOGOUT')});
        });
    }

};

/**
 * Dashboard controller
 */
function DashboardCtrl($rootScope, $scope, $location, $state, $window, $translate){
	$rootScope.$emit('loadprofile');
}

/**
 *  Login Controller
 */
function LoginCtrl($scope, $location, $state, $window, $translate, AuthenticationService, toaster){

	$scope.doLogin = function(){
		if ($scope.login_form.$valid) {
            AuthenticationService.login($scope.login).then(function(data){
            	$window.localStorage['jwtToken'] = data.data.attributes.token;
            	$state.go('index.dashboard');
            },function errorCallback(response) {
            	toaster.error({ body:$translate.instant('INVALID_LOGIN_CREDENTIALS')});
	        });
		}
    }
 }

/***Aalhouses Controllers (START) ***/
function AALHousesCtrl($scope, $location, $state, $window, $translate, toaster, aalhouses, AALHouseService, SweetAlert){
	this.houses = aalhouses.data;

	$scope.$on('reload-aalhouses', function(event, aalhouseId, status) {
		var updatedKey = null;
		angular.forEach(aalhouses.data, function(value, key) {
    		if( value.id == aalhouseId ){
    			updatedKey = key;
    			return;
    		}
    	});
		if( updatedKey != null ){
			aalhouses.data[updatedKey].attributes.status = status;
		}
	});

	$scope.disableHouse = function(aalhouseId){

		SweetAlert.swal({
            title: $translate.instant('ARE_YOU_SURE'),
            text: $translate.instant('AFTER_DISABLE_AALHOUSE_TEXT_MESSAGE'),
            type: "warning",
            showCancelButton: true,
            animation: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: $translate.instant('YES'),
            cancelButtonText: $translate.instant('NO'),
            closeOnConfirm: true,
            closeOnCancel: true
         },
            function (isConfirm) {
	            if (isConfirm) {
	            	AALHouseService.deleteHouseById(aalhouseId).then(function(data){
	        			toaster.success({ body: $translate.instant('AALHOUSE_DISABLED_SUCCESSFULLY')});
	        			$scope.$emit('reload-aalhouses', aalhouseId, 'DISABLED');
	                },function errorCallback(response) {
	                	var responseErrors = '';
	                	angular.forEach(response.data.errors, function(value, key) {
	                		responseErrors += value.detail + '<br/>';
	                	});
	                	toaster.error({ body: responseErrors});
	                });
	            }
        });
	}

	$scope.enableHouse = function(aalhouseId){
		AALHouseService.enableHouseById(aalhouseId).then(function(data){
			toaster.success({ body: $translate.instant('AALHOUSE_ENABLED_SUCCESSFULLY')});
			$scope.$emit('reload-aalhouses', aalhouseId, 'ENABLED');
        },function errorCallback(response) {
        	var responseErrors = '';
        	angular.forEach(response.data.errors, function(value, key) {
        		responseErrors += value.detail + '<br/>';
        	});
        	toaster.error({ body: responseErrors});
        });
	}

}

function AddHousesCtrl($scope, $location, $state, $window, $translate, toaster, AALHouseService){
	$scope.submitHouse = function(){
		if ($scope.aalhouses_form.$valid) {
			AALHouseService.addHouse(this.aalhouse).then(function(data){
				toaster.success({ body: $translate.instant('AALHOUSE_ADDED_SUCCESSFULLY')});
				$state.go('aalhouses.list');
            },function errorCallback(response) {
            	var responseErrors = '';
            	angular.forEach(response.data.errors, function(value, key) {
            		responseErrors += value.detail + '<br/>';
            	});
            	toaster.error({ body: responseErrors});
	        });
		}
	}
}

function EditHousesCtrl($scope, $location, $state, $stateParams, $window, $translate, toaster, AALHouseService, aalhouseData){
	$scope.aalhouse = {};
	$scope.aalhouse.id = aalhouseData.data.id;
	$scope.aalhouse.name = aalhouseData.data.attributes.name;
	$scope.aalhouse.address = aalhouseData.data.attributes.address;
	$scope.aalhouse.square_meters = aalhouseData.data.attributes.square_meters;
	$scope.aalhouse.total_rooms = aalhouseData.data.attributes.total_rooms;

	$scope.updateHouse = function(){
		if ($scope.aalhouses_form.$valid) {
			AALHouseService.updateHouse(this.aalhouse.id, this.aalhouse).then(function(data){
				toaster.success({ body: $translate.instant('AALHOUSE_UPDATED_SUCCESSFULLY')});
				$state.go('aalhouses.list');
            },function errorCallback(response) {
            	var responseErrors = '';
            	angular.forEach(response.data.errors, function(value, key) {
            		responseErrors += value.detail + '<br/>';
            	});
            	toaster.error({ body: responseErrors});
	        });
		}
	}
}
/***Aalhouses Controllers (END) ***/
/***Gateways Controllers (START) ***/

function GatewaysCtrl($scope, $location, $state, $window, $translate, toaster, gateways, GatewaysService, SweetAlert){
	this.gateways = gateways.data;

	$scope.$on('reload-gateways', function(event, gatewayId, status) {
		var updatedKey = null;
		angular.forEach(gateways.data, function(value, key) {
    		if( value.id == gatewayId ){
    			updatedKey = key;
    			return;
    		}
    	});
		if( updatedKey != null ){
			gateways.data[updatedKey].attributes.status = status;
		}
	});

	$scope.disableGateway = function(gatewayId){

		SweetAlert.swal({
            title: $translate.instant('ARE_YOU_SURE'),
            text: $translate.instant('AFTER_DISABLE_GATEWAY_TEXT_MESSAGE'),
            type: "warning",
            showCancelButton: true,
            animation: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: $translate.instant('YES'),
            cancelButtonText: $translate.instant('NO'),
            closeOnConfirm: true,
            closeOnCancel: true
         },
            function (isConfirm) {
	            if (isConfirm) {
	            	GatewaysService.deleteGatewayById(gatewayId).then(function(data){
	        			toaster.success({ body: $translate.instant('GATEWAY_DISABLED_SUCCESSFULLY')});
	        			$scope.$emit('reload-gateways', gatewayId, 'DISABLED');
	                },function errorCallback(response) {
	                	var responseErrors = '';
	                	angular.forEach(response.data.errors, function(value, key) {
	                		responseErrors += value.detail + '<br/>';
	                	});
	                	toaster.error({ body: responseErrors});
	                });
	            }
        });
	}

	$scope.enableGateway = function(gatewayId){
		GatewaysService.enableGatewayById(gatewayId).then(function(data){
			toaster.success({ body: $translate.instant('GATEWAY_ENABLED_SUCCESSFULLY')});
			$scope.$emit('reload-gateways', gatewayId, 'ENABLED');
        },function errorCallback(response) {
        	var responseErrors = '';
        	angular.forEach(response.data.errors, function(value, key) {
        		responseErrors += value.detail + '<br/>';
        	});
        	toaster.error({ body: responseErrors});
        });
	}


    // gateway RESOURCES

	$scope.resources = function(gatewayId){
		GatewaysService.viewResources(gatewayId).then(function(data){
			toaster.success({ body: $translate.instant('GATEWAY_RESOURCES_SUCCESSFULLY')});
			$scope.$emit('reload-gateways', gatewayId, 'ENABLED');
        },function errorCallback(response) {
        	var responseErrors = '';
        	angular.forEach(response.data.errors, function(value, key) {
        		responseErrors += value.detail + '<br/>';
        	});
        	toaster.error({ body: responseErrors});
        });
	}


}

function AddGatewayCtrl($scope, $location, $state, $window, $translate, toaster, GatewaysService){
	$scope.submitGateway = function(){
		if ($scope.gateways_form.$valid) {
			GatewaysService.addGateway(this.gateway).then(function(data){
				toaster.success({ body: $translate.instant('GATEWAY_ADDED_SUCCESSFULLY')});
				$state.go('gateways.list');
            },function errorCallback(response) {
            	var responseErrors = '';
            	angular.forEach(response.data.errors, function(value, key) {
            		responseErrors += value.detail + '<br/>';
            	});
            	toaster.error({ body: responseErrors});
	        });
		}
	}
}

function EditGatewayCtrl($scope, $location, $state, $stateParams, $window, $translate, toaster, GatewaysService, gatewayData, unallocatedSensors, sensorsData, aalhouses)
{
    $scope.gateway = {};
    $scope.gateway.id = gatewayData.data.id;
    $scope.gateway.gateway = gatewayData.data.attributes.gateway;
    $scope.gateway.identity = gatewayData.data.attributes.identity; // new
    // fetch aalhouses and related aalhouse id
    $scope.aalhouses = aalhouses.data;
    $scope.gateway.aalhouse = gatewayData.data.attributes.aalhouse.toString();           

    // populate sensor data
    $scope.sensorList = [];
    var includedData = gatewayData.included;
    angular.forEach(sensorsData.data, function(value, key)
    {
    	$scope.sensorList.push({
            id: value.id,
            type: "sensor",    	
            enabled: true,
            name: value.attributes.name,
            address: value.attributes.address
    });
  	});


    angular.forEach(unallocatedSensors.data, function(value, key)
    {
        $scope.sensorList.push({
            id: value.id,
            type: "sensor",            
            enabled: false,
            name: value.attributes.name,
            address: value.attributes.address
        });
    });


    $scope.updateGateway = function () {
        if ($scope.gateways_form.$valid) {
            GatewaysService.updateGatewayWithSensors(this.gateway.id, this.gateway, $scope.sensorList).then(function (data) {
                toaster.success({body: $translate.instant('GATEWAY_UPDATED_SUCCESSFULLY')});
                $state.go('gateways.list');
            }, function errorCallback(response) {
                var responseErrors = '';
                angular.forEach(response.data.errors, function (value, key) {
                    responseErrors += value.detail + '<br/>';
                });
                toaster.error({body: responseErrors});
            });
        }
    };

    function isSensorEnabled(sensorId) {
        var isEnabled = false;
        angular.forEach(includedData, function (value, key) {
            if (value.id === sensorId) {
                isEnabled = true;
            }
        });
        return isEnabled;
    }

}

function GatewayDashboardCtrl($scope, $location, $state, $stateParams, $window, $translate, toaster, GatewaysService, gatewayData, resourcesData, pingData)
{
    console.log("Starting GatewayDashboardCtrl...");
    // store scope data
    $scope.gateway = {};
    $scope.gateway.id = gatewayData.data.id;
    $scope.gateway.gateway = gatewayData.data.attributes.gateway;
    $scope.gateway.identity = gatewayData.data.attributes.identity; // new        
    $scope.gateway.monitoring = resourcesData;
    $scope.gateway.ping = pingData;
    $scope.gateway.sensors = gatewayData.included;
    console.log('Gateway data: ');
    console.log(gatewayData);
    console.log('Attached sensors: ');
    console.log(gatewayData.included);
    console.log('Gateway monitoring data: ');
    console.log(resourcesData);
    console.log('Gateway ping data: ');
    console.log(pingData);
    
    // request PING
    // TO-DO    
    $scope.ping = function () {
        GatewaysService.requestPing(this.gateway.id).then(function (data) {
            toaster.success({body: $translate.instant('GATEWAY_PING_SUCCESSFULLY')});
            //$scope.$emit('reload-gateways', gatewayId, 'ENABLED');
        }, function errorCallback(response) {
            var responseErrors = '';
            angular.forEach(response.data.errors, function (value, key) {
                responseErrors += value.detail + '<br/>';
            });
            toaster.error({body: responseErrors});
        });
    };

    // request RESOURCES
    // TO-DO    
    $scope.resources = function () {
        GatewaysService.requestResources(this.gateway.id).then(function (data) {
            toaster.success({body: $translate.instant('GATEWAY_RESOURCES_SUCCESSFULLY')});
            $state.go('gateways.resources');
            //$scope.$emit('reload-gateways', gatewayId, 'ENABLED');
        }, function errorCallback(response) {
            var responseErrors = '';
            angular.forEach(response.data.errors, function (value, key) {
                responseErrors += value.detail + '<br/>';
            });
            toaster.error({body: responseErrors});
        });
    };

    // request BATTERY_INDICATION
    // TO-DO    
    $scope.battery = function (identity) {
        GatewaysService.requestDeviceBattery(this.gateway.id, identity).then(function (data) {
            toaster.success({body: $translate.instant('SENSOR_BATTERY_SUCCESSFULLY')});
            //$scope.$emit('reload-gateways', gatewayId, 'ENABLED');
        }, function errorCallback(response) {
            var responseErrors = '';
            angular.forEach(response.data.errors, function (value, key) {
                responseErrors += value.detail + '<br/>';
            });
            toaster.error({body: responseErrors});
        });
    };  
    
    // request OPERATING_SYSTEM
    // TO-DO    
    $scope.os = function (operation) {
        GatewaysService.requestOperatingSystem(this.gateway.id, operation).then(function (data) {
            toaster.success({body: $translate.instant('OPERATING_SYSTEM_COMMAND_SUCCESSFULLY')});
            //$scope.$emit('reload-gateways', gatewayId, 'ENABLED');
        }, function errorCallback(response) {
            var responseErrors = '';
            angular.forEach(response.data.errors, function (value, key) {
                responseErrors += value.detail + '<br/>';
            });
            toaster.error({body: responseErrors});
        });
    };  
    
    // request DEVICE
    // TO-DO    
    $scope.device = function (operation) {
        GatewaysService.requestDevice(this.gateway.id, operation).then(function (data) {
            toaster.success({body: $translate.instant('DEVICE_COMMAND_SUCCESSFULLY')});
            //$scope.$emit('reload-gateways', gatewayId, 'ENABLED');
        }, function errorCallback(response) {
            var responseErrors = '';
            angular.forEach(response.data.errors, function (value, key) {
                responseErrors += value.detail + '<br/>';
            });
            toaster.error({body: responseErrors});
        });
    };     
    
    // request APPLICATION
    // TO-DO
    $scope.application = function (operation) {
        GatewaysService.requestApplication(this.gateway.id, operation).then(function (data) {
            toaster.success({body: $translate.instant('APPLICATION_COMMAND_SUCCESSFULLY')});
            //$scope.$emit('reload-gateways', gatewayId, 'ENABLED');
        }, function errorCallback(response) {
            var responseErrors = '';
            angular.forEach(response.data.errors, function (value, key) {
                responseErrors += value.detail + '<br/>';
            });
            toaster.error({body: responseErrors});
        });
    };     
}

function SensorsCtrl($scope, $location, $state, $window, $translate, toaster, sensors, SensorsService, SweetAlert){
	this.sensors = sensors.data;

        console.log(sensors.data);

	$scope.$on('reload-sensors', function(event, sensorId, status) {
		var updatedKey = null;
		angular.forEach(sensors.data, function(value, key) {
    		if( value.id == sensorId ){
    			updatedKey = key;
    			return;
    		}
    	});
	if( updatedKey != null ){
            sensors.data[updatedKey].attributes.status = status;
            //sensors.data[updatedKey].attributes.sensor.status = status;
	}
	});

	$scope.disableSensor = function(sensorId){

		SweetAlert.swal({
            title: $translate.instant('ARE_YOU_SURE'),
            text: $translate.instant('AFTER_DISABLE_SENSOR_TEXT_MESSAGE'),
            type: "warning",
            showCancelButton: true,
            animation: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: $translate.instant('YES'),
            cancelButtonText: $translate.instant('NO'),
            closeOnConfirm: true,
            closeOnCancel: true
         },
            function (isConfirm) {
	            if (isConfirm) {
	            	SensorsService.deleteSensorById(sensorId).then(function(data){
	        			toaster.success({ body: $translate.instant('SENSOR_DISABLED_SUCCESSFULLY')});
	        			$scope.$emit('reload-sensors', sensorId, 'DISABLED');
	                },function errorCallback(response) {
	                	var responseErrors = '';
	                	angular.forEach(response.data.errors, function(value, key) {
	                		responseErrors += value.detail + '<br/>';
	                	});
	                	toaster.error({ body: responseErrors});
	                });
	            }
        });
	}

	$scope.enableSensor = function(sensorId){
		SensorsService.enableSensorById(sensorId).then(function(data){
			toaster.success({ body: $translate.instant('SENSOR_ENABLED_SUCCESSFULLY')});
			$scope.$emit('reload-sensors', sensorId, 'ENABLED');
        },function errorCallback(response) {
        	var responseErrors = '';
        	angular.forEach(response.data.errors, function(value, key) {
        		responseErrors += value.detail + '<br/>';
        	});
        	toaster.error({ body: responseErrors});
        });
	}

}

function AddSensorCtrl($scope, $location, $state, $window, $translate, toaster, SensorsService, datatypes, manufacturers){
	$scope.sensor = {id: "", name: "", address: "", connectable :false, connection: ""};
	$scope.listDatatypes = [];
	$scope.manufacturers = manufacturers.data;
	angular.forEach(datatypes.data, function(value, key){
		$scope.listDatatypes.push({
			id: value.id,
			type: "datatype",
			enabled: false,
			datatype: value.attributes.datatype
		});
	});

	$scope.submitSensor = function(){
		if ($scope.sensors_form.$valid) {
			SensorsService.addSensor($scope.sensor, $scope.listDatatypes).then(function(data){
				toaster.success({ body: $translate.instant('SENSOR_ADDED_SUCCESSFULLY')});
				$state.go('sensors.list');
            },function errorCallback(response) {
            	var responseErrors = '';
            	angular.forEach(response.data.errors, function(value, key) {
            		responseErrors += value.detail + '<br/>';
            	});
            	toaster.error({ body: responseErrors});
	        });
		}
	}
}

function EditSensorCtrl($scope, $location, $state, $stateParams, $window, $translate, toaster, SensorsService, sensorData, datatypes, manufacturers){

	$scope.sensor = {};
	$scope.sensor.id = sensorData.data.id;
	$scope.sensor.name = sensorData.data.attributes.name;
	$scope.sensor.address = sensorData.data.attributes.address;
	$scope.sensor.connectable = sensorData.data.attributes.connectable;
	$scope.sensor.connection = sensorData.data.attributes.connection;
	$scope.manufacturers = manufacturers.data;
	$scope.sensor.manufacturer = sensorData.data.attributes.manufacturer.toString();

	var includedData = sensorData.included;
	$scope.listDatatypes = [];
	angular.forEach(datatypes.data, function(value, key){
		$scope.listDatatypes.push({
			id: value.id,
			type: "datatype",
			enabled: checkIfDatatypeisEnabledToSensor(value.id),
			datatype: value.attributes.datatype
		});
	});

	$scope.updateSensor = function(){
		if ($scope.sensors_form.$valid) {
			SensorsService.updateSensor(this.sensor.id, this.sensor, $scope.listDatatypes).then(function(data){
				toaster.success({ body: $translate.instant('SENSOR_UPDATED_SUCCESSFULLY')});
				$state.go('sensors.list');
            },function errorCallback(response) {
            	var responseErrors = '';
            	angular.forEach(response.data.errors, function(value, key) {
            		responseErrors += value.detail + '<br/>';
            	});
            	toaster.error({ body: responseErrors});
	        });
		}
	}

	function checkIfDatatypeisEnabledToSensor(datatypeId){
		var exists = false;
		angular.forEach(includedData, function(value, key){
			if( value.id==datatypeId ) {
				exists = true;
			}
		});
		return exists;
	}
}

function EmulatorCtrl($scope, $location, $state, $stateParams, $window, $translate, toaster, EmulationService, emulationQueues){
	$scope.queues = emulationQueues.data;
	$scope.emulation = { topic: '', message: ''};
	$scope.sendEmulation = function(){
		if ($scope.emulation_form.$valid) {
			console.log($scope.emulation);
			EmulationService.sendMessage($scope.emulation).then(function(data){
				toaster.success({ body: $translate.instant('SEND_MESSAGE_SUCCESSFULLY')});
                $scope.emulation_form.$setUntouched();
                $scope.emulation_form.$setPristine();
            },function errorCallback(response) {
            	var responseErrors = '';
            	angular.forEach(response.data.errors, function(value, key) {
            		responseErrors += value.detail + '<br/>';
            	});
            	toaster.error({ body: responseErrors});
	        });
		}
	}
}

function LocalizationCtrl($scope, $translate, toaster, LocalizationService, aalhouses){
	$scope.houses = aalhouses.data;
	$scope.localization = {
			house: '',
			object: ''
	}
	$scope.observations = [];
	$scope.findRelativeObjects = function(){
		if( $scope.localization.house=='' || $scope.localization.object=='' ) return;
		
		LocalizationService.getLocationsByHouse($scope.localization.house, $scope.localization.object).then(function(data){
			$scope.observations = data.data;
        },function errorCallback(response) {
        	var responseErrors = '';
        	angular.forEach(response.data.errors, function(value, key) {
        		responseErrors += value.detail + '<br/>';
        	});
        	toaster.error({ body: responseErrors});
        });
	}
	
}

/***Sensors Controllers (END) ***/

angular
    .module('atlasweb')
    .factory('authInterceptor', authInterceptor)
   	.config(function($httpProvider) {
  	      $httpProvider.interceptors.push('authInterceptor');
    })
    .controller('MainCtrl', MainCtrl)
    .controller('LoginCtrl', LoginCtrl)
    .controller('AALHousesCtrl',AALHousesCtrl)
    .controller('AddHousesCtrl',AddHousesCtrl)
    .controller('EditHousesCtrl',EditHousesCtrl)
    .controller('GatewaysCtrl',GatewaysCtrl)
    .controller('AddGatewayCtrl',AddGatewayCtrl)
    .controller('EditGatewayCtrl',EditGatewayCtrl)
    .controller('SensorsCtrl',SensorsCtrl)
    .controller('AddSensorCtrl',AddSensorCtrl)
    .controller('EditSensorCtrl',EditSensorCtrl)
    .controller('EmulatorCtrl',EmulatorCtrl)
    .controller('DashboardCtrl',DashboardCtrl)
    //.controller('GetGatewayResourcesCtrl', GetGatewayResourcesCtrl)
    //.controller('VicinityCtrl',VicinityCtrl)
    .controller('LocalizationCtrl',LocalizationCtrl)
    .controller('GatewayDashboardCtrl',GatewayDashboardCtrl);
