/**
 * ATLAS Web Application
 *
 */
(function () {
    angular.module('atlasweb', [
        'ui.router',                    // Routing
        'oc.lazyLoad',                  // ocLazyLoad
        'ui.bootstrap',                 // Ui Bootstrap
        'pascalprecht.translate',       // Angular Translate
        'ngIdle',                       // Idle timer
        'ngSanitize',                    // ngSanitize
        'ngAnimate',                    // ngAnimate
        'toaster',                      // toaster
        'oitozero.ngSweetAlert'					// Alerts
    ])
})();

