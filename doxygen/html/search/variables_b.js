var searchData=
[
  ['registered',['REGISTERED',['../enumorg_1_1atlas_1_1modules_1_1users_1_1domain_1_1_user_status.html#aed1a3ae8086b9fb39d726ae1d32c4d38',1,'org::atlas::modules::users::domain::UserStatus']]],
  ['resource_5fnot_5ffound',['RESOURCE_NOT_FOUND',['../enumorg_1_1atlas_1_1api_1_1errors_1_1_api_error.html#a09f5366e0b948890b407400e4fd36a2a',1,'org::atlas::api::errors::ApiError']]],
  ['role_5fadmin',['ROLE_ADMIN',['../enumorg_1_1atlas_1_1core_1_1security_1_1domain_1_1_authority_role.html#ac604be8b5b6d9969bf85c799bc6bdeef',1,'org::atlas::core::security::domain::AuthorityRole']]],
  ['role_5fsuperadm',['ROLE_SUPERADM',['../enumorg_1_1atlas_1_1core_1_1security_1_1domain_1_1_authority_role.html#aa70a5c00b1b3b2b2f57a738754c32b1a',1,'org::atlas::core::security::domain::AuthorityRole']]],
  ['role_5fuser',['ROLE_USER',['../enumorg_1_1atlas_1_1core_1_1security_1_1domain_1_1_authority_role.html#ac6b08e9013fe5449a3fe3033cd1673d2',1,'org::atlas::core::security::domain::AuthorityRole']]]
];
