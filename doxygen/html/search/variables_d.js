var searchData=
[
  ['unable_5fto_5fstart_5fsession',['UNABLE_TO_START_SESSION',['../enumorg_1_1atlas_1_1api_1_1errors_1_1_api_error.html#ace6818ef65272df7bd6e57a76d776038',1,'org::atlas::api::errors::ApiError']]],
  ['unsupported_5fmedia_5ftype',['UNSUPPORTED_MEDIA_TYPE',['../enumorg_1_1atlas_1_1api_1_1errors_1_1_api_error.html#aedb026f8dfece167cd51256dca72d466',1,'org::atlas::api::errors::ApiError']]],
  ['user_5fnot_5ffound',['USER_NOT_FOUND',['../enumorg_1_1atlas_1_1api_1_1errors_1_1_api_error.html#aef60758905d691e9406ca0ec10247b3a',1,'org::atlas::api::errors::ApiError']]],
  ['user_5fprofile',['USER_PROFILE',['../classorg_1_1atlas_1_1api_1_1_api_endpoints.html#a7f2916aad014fdff1decb1b9e15f3660',1,'org::atlas::api::ApiEndpoints']]],
  ['user_5fwrong_5fcredentials',['USER_WRONG_CREDENTIALS',['../enumorg_1_1atlas_1_1api_1_1errors_1_1_api_error.html#a4eaafb55753c4d5bfbf34d29d9e8f975',1,'org::atlas::api::errors::ApiError']]]
];
