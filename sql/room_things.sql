/*
Navicat MySQL Data Transfer

Source Server         : Tunnelled - MariaDB
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : atlas

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-03-08 13:01:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for room_things
-- ----------------------------
DROP TABLE IF EXISTS `room_things`;
CREATE TABLE `room_things` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `sensor_id` int(11) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ThingUnique` (`name`) USING BTREE,
  KEY `SensorThingFK` (`sensor_id`),
  CONSTRAINT `SensorThingFK` FOREIGN KEY (`sensor_id`) REFERENCES `sensors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for room_things_observations
-- ----------------------------
DROP TABLE IF EXISTS `room_things_observations`;
CREATE TABLE `room_things_observations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `room_thing_id` int(11) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `vicinity_things` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `RoomThing` (`room_thing_id`),
  CONSTRAINT `RoomThing` FOREIGN KEY (`room_thing_id`) REFERENCES `room_things` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
