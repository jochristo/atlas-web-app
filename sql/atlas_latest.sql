/*
Navicat MySQL Data Transfer

Source Server         : mariadb-10.1.21
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : atlas

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-07-07 10:42:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for aalhouses
-- ----------------------------
DROP TABLE IF EXISTS `aalhouses`;
CREATE TABLE `aalhouses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `location` text NOT NULL,
  `square_meters` int(11) NOT NULL,
  `total_rooms` smallint(6) NOT NULL,
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for country
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `country` varchar(255) NOT NULL,
  `countrycode` varchar(20) NOT NULL,
  `tld` char(3) NOT NULL,
  `acc3` char(3) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `currencycode` char(3) NOT NULL,
  `timezone` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for datatypes
-- ----------------------------
DROP TABLE IF EXISTS `datatypes`;
CREATE TABLE `datatypes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `datatype` varchar(255) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for emulator_queues
-- ----------------------------
DROP TABLE IF EXISTS `emulator_queues`;
CREATE TABLE `emulator_queues` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `queue_name` varchar(255) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `json` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0 string, 1 json',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gateways
-- ----------------------------
DROP TABLE IF EXISTS `gateways`;
CREATE TABLE `gateways` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `gateway` varchar(255) NOT NULL,
  `identity` varchar(255) DEFAULT NULL,
  `aalhouse_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `token` varchar(50) NOT NULL,
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gateway_sensors
-- ----------------------------
DROP TABLE IF EXISTS `gateway_sensors`;
CREATE TABLE `gateway_sensors` (
  `gateway_id` int(11) NOT NULL,
  `sensor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`gateway_id`,`sensor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for locations
-- ----------------------------
DROP TABLE IF EXISTS `locations`;
CREATE TABLE `locations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `aalhouse_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `object` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1565 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for manufacturers
-- ----------------------------
DROP TABLE IF EXISTS `manufacturers`;
CREATE TABLE `manufacturers` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for room_things
-- ----------------------------
DROP TABLE IF EXISTS `room_things`;
CREATE TABLE `room_things` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `sensor_id` int(11) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ThingUnique` (`name`) USING BTREE,
  KEY `SensorThingFK` (`sensor_id`),
  CONSTRAINT `SensorThingFK` FOREIGN KEY (`sensor_id`) REFERENCES `sensors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for room_things_observations
-- ----------------------------
DROP TABLE IF EXISTS `room_things_observations`;
CREATE TABLE `room_things_observations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `room_thing_id` int(11) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `vicinity_things` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `RoomThing` (`room_thing_id`),
  CONSTRAINT `RoomThing` FOREIGN KEY (`room_thing_id`) REFERENCES `room_things` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for sensors
-- ----------------------------
DROP TABLE IF EXISTS `sensors`;
CREATE TABLE `sensors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `manufacturer_id` smallint(6) unsigned NOT NULL,
  `address` varchar(255) NOT NULL,
  `wireless_connection` tinyint(2) NOT NULL,
  `connectable` tinyint(2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(2) NOT NULL,
  `battery` int(11) NOT NULL,
  `last_ping` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `un_address` (`address`) USING BTREE,
  KEY `FKManufacturer` (`manufacturer_id`),
  CONSTRAINT `FKManufacturer` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for sensor_datatypes
-- ----------------------------
DROP TABLE IF EXISTS `sensor_datatypes`;
CREATE TABLE `sensor_datatypes` (
  `sensor_id` int(11) unsigned NOT NULL,
  `datatype_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`sensor_id`,`datatype_id`),
  KEY `FKDatatype` (`datatype_id`),
  CONSTRAINT `FKDatatype` FOREIGN KEY (`datatype_id`) REFERENCES `datatypes` (`id`),
  CONSTRAINT `FKSensor` FOREIGN KEY (`sensor_id`) REFERENCES `sensors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for sessions
-- ----------------------------
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `token` char(36) NOT NULL,
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=434 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for sys_users
-- ----------------------------
DROP TABLE IF EXISTS `sys_users`;
CREATE TABLE `sys_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL COMMENT 'User Flags\r\n\r\n1.  Enabled\r\n2.  Disabled\r\n3.  Banned',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1. Enabled, 2. Disabled, 3. Deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for unity_components_mappings
-- ----------------------------
DROP TABLE IF EXISTS `unity_components_mappings`;
CREATE TABLE `unity_components_mappings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `unity_identifier` varchar(255) NOT NULL,
  `real_identifier` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1. enabled, 2. disabled.',
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
